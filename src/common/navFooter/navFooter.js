angular.module( 'alcove.navFooter', [] )
    .directive( 'navFooter', function() {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                isExtended: '='
            },
            templateUrl: 'navFooter/navFooter.tpl.html',
            controller : ['$scope', '$i18next', '$cookies', '$timeout', function(
                $scope, $i18next, $cookies, $timeout ){ 

                $scope.languages = [{
                    name: 'English',
                    code: 'en'
                }, {
                    name: 'Chinese',
                    code: 'zh'
                }
                // , {
                //     name: 'French',
                //     code: 'fr'
                // }
                ];

                $i18next.options.lng = ( $cookies.get('lng') || "en");
                $scope.activeLanguage = $i18next.options.lng.toUpperCase();
                
                $scope.switchLanguage = function(lng) {
                    $i18next.options.lng = lng.code;
                    $cookies.put('lng',lng.code);
                    $scope.activeLanguage = lng.code.toUpperCase();
                };

                $scope.openSubscribePage = function() {
                    window.open('http://artstor.org/trial', '_blank');
                };

                // Delay the footer to show after other layout elements load
                $timeout(function() {
                    $scope.isLoaded = true;
                },400);
                
            }],
            link: function($scope, elem, attrs, account, $location) {  

            }
        };
    })
 ;