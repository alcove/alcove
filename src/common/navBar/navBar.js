angular.module( 'alcove.navBar', ['ngStorage'] )
    .directive( 'navBar', function() {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                searchTerm: '@',
                hideSearch: '@',
                hideNavigation: '=',
                linkText: '@',
                searchFunction: '&',
                usercurrentlocation: '=',
                assetindex: '=',
                totalassets: '='
            },
            templateUrl: 'navBar/navBar.tpl.html',
            controller : ['$scope', '$localStorage', '$location', '$timeout', '$rootScope', '$window', '$route', 'searchQuery', 'account', function(
                        $scope,
                        $localStorage,
                        $location,
                        $timeout,
                        $rootScope,
                        $window,
                        $route,
                        searchQuery,
                        account
                    ){


                $scope.searchVocab = "";
                $scope.$storage = $localStorage;
                $scope.placeholder = "Search";
                $scope.showAction = true;

                // account.fetchAccountInfo()
                //     .success( function(response) {
                //         if (response["logged_in"]) {
                //             $localStorage.loggedIn = true;
                //             $localStorage['username'] = response['username'];
                //         } else {
                //             $localStorage.loggedIn = false;
                //             $localStorage['username'] = "";
                //         }
                //     });
                $rootScope.progressbar.setParent( document.getElementById('nav') );
                $timeout(function() {
                    if ($scope.assetindex && $scope.totalassets) {
                        $rootScope.progressbar.set( $scope.assetindex / $scope.totalassets * 100 );
                    }
                });

                $scope.clickAction = function() {
                    if ( $scope.hideSearch ) {
                        $window.location.href = '#' + $localStorage.last_search_url;
                    } else {
                        triggerEvent( document.getElementById('navSearch'), 'focus');
                    }
                };

                $scope.setView = function() {
                    $localStorage.last_search_url = $location.url();
                };

                $scope.goHome = function() {
                    // Make sure to clear fullscreen
                    if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                    }
                    $localStorage.last_search_url = $location.url();
                    searchQuery.clearFilters();
                    $location.path('/?');
                    $route.reload();
                };

                $scope.logoClick = function() {
                    if ( $location.path.toString().indexOf('/view/') > -1 ) {
                        $scope.clickAction();
                    } else {
                        $scope.goHome();
                    }
                };

                $scope.prevAsset = function(){
                    if($scope.assetindex){
                        var current_assetindex = $scope.assetindex - 1;
                        if(current_assetindex > 0){
                            $window.location.href = '#/item/' + $localStorage.search_results.ids_array[current_assetindex - 1];
                        }
                    }

                };
                $scope.nextAsset = function(){
                    if($scope.assetindex){
                        var current_assetindex = $scope.assetindex - 1;
                        if($scope.assetindex < $localStorage.search_results.ids_array.length){
                            $window.location.href = '#/item/' + $localStorage.search_results.ids_array[current_assetindex + 1];
                        }
                    }
                };

                $scope.showRegistration = false;
                $scope.toggleRegistration = function() {
                    $scope.showRegistration = !$scope.showRegistration;
                    $scope.showSignIn = false;

                    if ($scope.showRegistration === false) {
                        // Reset Registration form
                    }
                };

                $scope.hideSlidePane = function() {
                    $scope.showRegistration = false;
                    $scope.showSignIn = false;
                    $scope.showEmailFields = true;
                    $scope.showInstitutionalFields = false;
                };

                $scope.showSignIn = false;
                $scope.toggleSignIn = function() {
                    $scope.showSignIn = !$scope.showSignIn;
                    $scope.showRegistration = false;

                    if ($scope.showSignIn === false) {
                        $scope.showEmailFields = true;
                        $scope.showInstitutionalFields = false;
                    }
                };

                $scope.showEmailFields = true;
                $scope.openEmailFields = function() {
                    console.log("Show email fields");
                    $scope.showEmailFields = true;
                    $scope.showInstitutionalFields = false;
                };

                $scope.showInstitutionalFields = false;
                $scope.openInstitutionalFields = function() {
                    console.log("Show email fields");
                    $scope.showInstitutionalFields = true;
                    $scope.showEmailFields = false;
                };

                $scope.signInButton = "Sign In";
                $scope.emailSignIn = function(email, password) {
                    console.log("Sign In");
                    $scope.signInButton = "Signing In";
                    account.signin(email, password)
                        .success(function(response) {
                            if(response['username']){
                                $localStorage['user'] = response;
                                //$localStorage['username'] = response['firstName'] + " " + response['lastName'];
                                $localStorage.loggedIn = true;
                                $localStorage['username'] = response['username'];
                                $scope.toggleSignIn();
                                __insp.push(['identify', response.username]);
                                __insp.push(['tagSession', {username: response.username}]);
                            } else {
                                $localStorage.loggedIn = false;
                            }
                        });
                };

                $scope.signOut = function() {
                    $scope.signOutLoading = true;
                    account.signout().success(function() {
                        $scope.signOutLoading = false;
                        $localStorage.$reset();
                    });
                };

                $scope.$on('refreshNav', function() {
                    $localStorage.last_search_url = $location.url();
                    // console.log("Saved: " + $location.url() );
                });

                $scope.toggleFilterMenu = function() {
                    if ($rootScope.filterMenuShowing) {
                        $rootScope.filterMenuShowing = false;
                        angular.element(document.getElementById('filters')).removeClass('slideOut');
                    } else {
                        $rootScope.filterMenuShowing = true;
                        angular.element(document.getElementById('filters')).addClass('slideOut');
                    }
                };

            }],
            link: function($scope, elem, attrs, account, $location) {
                $scope.runSearchFunction = function() {
                    $scope.searchFunction()($scope.searchTerm);
                };
            }
        };
    })
 ;
