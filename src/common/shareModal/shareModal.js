angular.module( 'alcove.shareModal', [] )
    .directive( 'shareModal', function() {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                description: '=',
                url: '=',
                mediaUrl: '='
            },
            templateUrl: 'shareModal/shareModal.tpl.html',
            controller : ['$scope', '$timeout', function($scope, $timeout){



              $scope.hideShareOptions = function(){
                  angular.element( document.getElementById('shareAssetModal') ).removeClass('fade-in');
              };

              $scope.getCurrentPageUrl = function(){
                  return window.location.href;
              };

              $scope.emailLinkClicked = function(){
                  ga('send', {
                    hitType: 'event',
                    eventCategory: 'Share',
                    eventAction: 'Email Link',
                    eventLabel: $scope.url
                  });
              };

              $scope.pinterestClicked = function(){
                  ga('send', {
                    hitType: 'event',
                    eventCategory: 'Share',
                    eventAction: 'Save to Pinterest',
                    eventLabel: $scope.url
                  });
              };

              $scope.evernoteClicked = function(){
                  ga('send', {
                    hitType: 'event',
                    eventCategory: 'Share',
                    eventAction: 'Save to Evernote',
                    eventLabel: $scope.url
                  });
              };

              $scope.copyLink = function(){
                  ga('send', {
                    hitType: 'event',
                    eventCategory: 'Share',
                    eventAction: 'Copy Link',
                    eventLabel: $scope.url
                  });


                  var currentUrl = $scope.url; // $scope.getCurrentPageUrl();

                  var textArea = document.createElement("textarea");

                  textArea.style.position = 'fixed';
                  textArea.style.bottom = 0;
                  textArea.style.left = 0;

                  textArea.style.width = '5px';
                  textArea.style.height = '5px';

                  textArea.style.padding = 0;

                  textArea.style.border = 'none';
                  textArea.style.outline = 'none';
                  textArea.style.boxShadow = 'none';

                  textArea.style.background = 'transparent';

                  textArea.value = currentUrl;

                  document.body.appendChild(textArea);

                  textArea.select();

                  try {
                      $scope.successful = document.execCommand('copy');
                      var timeout = 2000;
                      if($scope.successful){
                        $scope.msg = 'Link Copied';
                      }
                      else{
                        $scope.msg = 'Copy to clipboard not supported by the browser <br/> Copy the URL manually: ' + currentUrl;
                        timeout = 8000;
                      }

                     $timeout(function() {
                       $scope.msg = "";
                     }, timeout);
                    console.log('URL Copy:- ' + $scope.msg);
                  }
                  catch (err) {
                    console.log('Unable to Copy');
                  }

                  document.body.removeChild(textArea);

              };

            }],
            link: function($scope, elem, attrs, account, $location) {

            }
        };
    })
 ;
