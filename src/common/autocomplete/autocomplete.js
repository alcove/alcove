angular.module( 'alcove.autocomplete', [] )
.directive('autocomplete', [ 'searchQuery', function(searchQuery) {
  var index = -1;

  return {
    restrict: 'E',
    templateUrl: 'autocomplete/autocomplete.tpl.html',
    transclude: true,
    scope: {
      searchParam: '=ngModel',
      onType: '=onType',
      onSelect: '=onSelect',
      autocompleteRequired: '='
    },
    controller: ['$scope', '$rootScope', '$window', '$timeout', '$localStorage', 'schema', 'assets', function($scope, $rootScope, $window, $timeout, $localStorage, schema, assets){

      // Pull our suggestions
      $scope.suggestions = [];
      $scope.specialSuggestions = [];
      
      // Institutions to suggestions
      if ($localStorage['institutionFilter']) {
        var instObj = $localStorage['institutionFilter'];
        for (var instId in instObj) {
          $scope.specialSuggestions.push( { "id" : instId, "term" : instObj[instId].name, "field" : schema.institutionId, "count" : instObj[instId].doc_count } );
        }
      }
      // Collections to suggestions
      if ($localStorage['collectionFilter']) {
        var colObj = $localStorage['collectionFilter'];
        for (var colId in colObj) {
          $scope.specialSuggestions.push( { "id" : colId, "term" : colObj[colId].name, "field" : schema.collections, "count" : colObj[colId].doc_count } );
        }
      }
      

      // the index of the suggestions that's currently selected
      $scope.selectedIndex = -1;
      
      $scope.initLock = true;

      // set new index
      $scope.setIndex = function(i){
        $scope.selectedIndex = parseInt(i);
      };

      this.setIndex = function(i){
        $scope.setIndex(i);
        $scope.$apply();
      };

      $scope.getIndex = function(i){
        return $scope.selectedIndex;
      };

      $scope.clearSearch = function() {
        $scope.searchFilter = $scope.searchParam = "";
        // Reset field width
        document.getElementById('autocomplete').width = '30px';
        // Send new empty search query (triggering wild-card search)
        searchQuery.newSearch("");
      };

      $scope.goHome = function() {
          var homeUrl = $window.location.origin + $window.location.pathname;
          $window.open(homeUrl, '_self');
      };
      
      // Additional filter to limit non-suggester (service) suggestions to matching first characters
      $scope.startsWith = function (actual, expected) {
          var lowerStr = (actual + "").toLowerCase();
          return lowerStr.indexOf(expected.toLowerCase()) === 0;
      };

      $scope.fieldFocus = function() {
        triggerEvent(document.getElementById('autocomplete'),'focus');
        // Hide filters on Mobile
        if ($rootScope.filterMenuShowing){
          $rootScope.filterMenuShowing = false;
          angular.element(document.getElementById('filters')).removeClass('slideOut');
        }
      };

       // watches if the parameter filter should be changed
      var watching = false;

      // autocompleting drop down on/off
      $scope.completing = false;

      // for hovering over suggestions
      this.preSelect = function(suggestion){

        watching = false;

        // this line determines if it is shown
        // in the input field before it's selected:
        //$scope.searchParam = suggestion;

        $scope.$apply();
        watching = true;

      };

      $scope.preSelect = this.preSelect;

      this.preSelectOff = function(){
        watching = true;
      };

      $scope.preSelectOff = this.preSelectOff;

      // Plain text search without applying a filter
      $scope.search = function() {
        if ($scope.selectedIndex < 0) {
          // Search and clear filters with "newSearch"
          searchQuery.newSearch($scope.searchParam);

          // Remove focus
          triggerEvent(document.getElementById('autocomplete'),'blur');
        }
      };

      // ngClick does not consistently access the 'suggestion' object, so we keep the selectedIndex updated
      $scope.hoverSuggestion = function(suggestion) {
        $scope.selectedIndex = $scope.suggestions.indexOf(suggestion);
      };

      // Selecting a suggestion with RIGHT ARROW or ENTER
      $scope.select = function(suggestion){
        if (!suggestion) {
          suggestion = $scope.suggestions[$scope.selectedIndex];
        }
        if(suggestion){
          $scope.searchParam = suggestion.term;
          $scope.searchFilter = suggestion.term;

          // Title field is just used as a search term
          if (suggestion.field.indexOf("Title") > -1) {
            searchQuery.updateSearchTerm(suggestion.term);
          } else if (suggestion.id) {
            // Check if suggestion has ID, which should be applied instead
            // - A suggestion with an id doesn't actually exist on a record, so we shouldn't search by Id
            // - Only the ID lives on the record
            searchQuery.searchWithFilter("", suggestion.field, suggestion.id);
          } else {
            // Perform new search, with updated search term and filter
            // (Function clears previous filters)
            searchQuery.searchWithFilter(suggestion.term, suggestion.field, suggestion.term);
          }

          if($scope.onSelect) {
            $scope.onSelect(suggestion);
          }

          // Make sure field width is correct
          $timeout(function() {
            onFieldChange();
          });

          // Remove focus
          $timeout(function() {
            triggerEvent(document.getElementById('autocomplete'),'blur');
          });

        }
        $scope.setIndex(-1);
      };
      
      // Debounce suggest queries
      var searchingSuggests = false;
      
      // Use clone to maintain field width
      function onFieldChange() {
          if (!searchingSuggests) {
            searchingSuggests = true;
            assets.getSearchSuggestions($scope.searchParam)
              .success(function(res) {
                var newSuggestions = [];
                
                angular.forEach(res, function(obj, field) {
                  if (res[field] && res[field][0] && res[field][0].options) {
                    angular.forEach( res[field][0].options, function(val, key) {
                      if (val && val.text) {
                        newSuggestions.push( { "term": val.text, "field": field } ); 
                      }
                    });
                  }
                });
                if (newSuggestions.length > 0) {
                  $scope.suggestions = newSuggestions;
                  $scope.suggestions = $scope.specialSuggestions.concat( $scope.suggestions );
                }
              });
            setTimeout(function() {
              searchingSuggests = false;
            },100);
          }
          
          
          if ( autoEl.value ) {
              document.getElementById('autocompleteClone').textContent =  autoEl.value;
              autoEl.style.width = (document.getElementById('autocompleteClone').clientWidth + 0) + 'px';
          } else {
              document.getElementById('autocompleteClone').textContent = '';
              autoEl.style.width = '30px';
          }


          // See if value has changed
          if ( $scope.searchParam === $scope.searchFilter ) {
            return;
          }

          if( watching && typeof $scope.searchParam !== 'undefined' && $scope.searchParam !== null) {
            document.getElementById('suggestions').style.display = 'block';
            $scope.completing = true;
            $scope.searchFilter = $scope.searchParam;
            $scope.selectedIndex = -1;
            $scope.$apply();
          }

          // function thats passed to on-type attribute gets executed
          if($scope.onType) {
            $scope.onType($scope.searchParam);
          }
      }
      
      var autoEl = document.getElementById('autocomplete');
      var navEl = document.getElementById('nav');
      
      angular.element(autoEl).on('focus', function() {
        document.getElementById('suggestions').style.display = 'block';
        angular.element(navEl).addClass('search-engaged');
        watching = true;
        $scope.completing = true;
        $scope.searchFilter = $scope.searchParam;
      });

      angular.element(autoEl).on('change input keyup paste', function() {
          onFieldChange();
      });

      angular.element(autoEl).on('blur', function() {
        // we do a timeout to prevent hiding it before a click event is registered
        setTimeout(function() {
          $scope.select();
          $scope.setIndex(-1);
          $scope.$apply();
        }, 150);

        angular.element(navEl).removeClass('search-engaged');

        watching = false;
        $scope.completing = false;

        // setTimeout(function() {
        //   if ( !$('#autocomplete').is(':focus') ) {
        //     $('#suggestions').hide(1000);
        //   }
        // }, 400);

      });

      $timeout(function() {
          document.getElementById('suggestions').style.display = 'none';
          onFieldChange();
      }, 500);

    }],
    link: function(scope, element, attrs){

      setTimeout(function() {
        scope.initLock = false;
        scope.$apply();
      }, 250);

      var attr = '';
      var autoEl = document.getElementById('autocomplete');

      // Default atts
      scope.attrs = {
        "placeholder" : "...",
        "class" : "",
        "id" : "",
        "inputclass" : "",
        "inputid" : ""
      };

      for (var a in attrs) {
        attr = a.replace('attr', '').toLowerCase();
        // add attribute overriding defaults
        // and preventing duplication
        if (a.indexOf('attr') === 0) {
          scope.attrs[attr] = attrs[a];
        }
      }

      if (attrs.clickActivation) {
        element[0].onclick = function(e){
          if(!scope.searchParam){
            setTimeout(function() {
              scope.completing = true;
              scope.$apply();
            }, 200);
          }
        };
      }

      var key = {left: 37, up: 38, right: 39, down: 40 , enter: 13, esc: 27, tab: 9};

      document.addEventListener("keydown", function(e){
        var keycode = e.keyCode || e.which;

        switch (keycode){
          case key.esc:
            // disable suggestions on escape
            scope.select();
            scope.setIndex(-1);
            scope.$apply();
            e.preventDefault();
            triggerEvent(autoEl, 'onblur');
        }
      }, true);

      // document.addEventListener("blur", function(e){
      //   // disable suggestions on blur
      //   // we do a timeout to prevent hiding it before a click event is registered
      //   setTimeout(function() {
      //     scope.select();
      //     scope.setIndex(-1);
      //     scope.$apply();
      //   }, 150);
      // }, true);

      var suggestionIndex;

      element[0].addEventListener("keydown",function (e){
        var keycode = e.keyCode || e.which;

        var l = angular.element(this).find('li').length - 1;
        
        if ( keycode === key.esc ) {
            triggerEvent(autoEl, 'blur');
            e.preventDefault();
        }
        
        // this allows submitting forms by pressing Enter in the autocompleted field
        if(!scope.completing || l === 0) {
          if (keycode === key.tab) {
            // Prevents tabbing to the next element/field
            e.preventDefault();
          }
          return;
        }
        // implementation of the up and down movement in the list of suggestions
        switch (keycode){
          case key.up:
            index = scope.getIndex();
            // Get index of previous suggestion in list (list is filtered so indexes are not consecutive) by traversing li's
            var prevIndex = angular.element(document.querySelector('#suggestions li[index="'+index+'"]').previousElementSibling).attr('index');
            
            if(!prevIndex || prevIndex<-1){
              // Loop to last item showing in list
              prevIndex = angular.element(document.querySelector('#suggestions').lastElementChild).attr('index');
              if (!prevIndex) {
                prevIndex = -1;
              }
            }
            scope.setIndex(prevIndex);
            scope.preSelectOff();
            scope.$apply();
            break;
          case key.down:
            index = scope.getIndex();
            // Get index of next suggestion in list (list is filtered so indexes are not consecutive) by traversing li's
            var nextIndex = angular.element(document.querySelector('#suggestions li[index="'+index+'"]')).next().attr('index');
            
            if(!nextIndex || nextIndex<-1){
              nextIndex = -1;
            }
            scope.setIndex(nextIndex);
            scope.preSelectOff();
            scope.$apply();
            break;
          case key.left:
            break;
          case key.right:
            break;
          case key.enter:
            e.preventDefault();
            index = scope.getIndex();

            // scope.preSelectOff();
            if(index !== -1) {
              if (angular.element(this).find('li')[index]) {
                suggestionIndex = angular.element( angular.element(this).find('li')[index] ).attr('index');
                scope.select(scope.suggestions[suggestionIndex + 1]);
              } else {
                scope.select(scope.suggestions[index]);
              }
            }

            break;
          case key.tab:
            e.preventDefault();
            index = scope.getIndex();

            // scope.preSelectOff();
            if(index !== -1) {
              if (angular.element(this).find('li')[index]) {
                suggestionIndex = angular.element(angular.element(this).find('li')[index]).attr('index');
                scope.select(scope.suggestions[suggestionIndex]);
              } else {
                scope.select(scope.suggestions[index]);
              }
            } else {
              if(keycode == key.enter) {
                scope.select();
              }
            }
            // scope.setIndex(-1);
            // scope.$apply();
            break;
          default:
            return;
        }

      });
    }
  };
}]);
