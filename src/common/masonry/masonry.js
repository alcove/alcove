angular.module( 'alcove.masonry', [] )
    .directive( 'masonry', function() {
        return {
            scope: {}, 
            controller: function($scope, $element, $rootScope) { 

                $element
                    .addClass('masonry')
                    .prepend("<div class='thumbnail-sizer'></div>");

                var elem = document.getElementById($element[0]['id']);

                var msnry = new Masonry( elem, {
                    transitionDuration: 0,
                    // isFitWidth: true,
                    // initLayout: false,
                    columnWidth: '.thumbnail-sizer',
                    itemSelector: '.thumbnail'
                });

                msnry.layout();
                msnry.reloadItems(); 
                
                // Fired every image load by "thumbnail.js"
                $rootScope.$on('newThumbnailData', function() {  
                    updateLayout();
                }); 
                
                var layoutDebounce = false;
                var runOnceAfter = false;
                
                function updateLayout() {
                    if (layoutDebounce === false) {
                        layoutDebounce = true;
                        msnry.layout();
                        msnry.reloadItems();
                        setTimeout(function() {
                            layoutDebounce = false;
                            
                            if (runOnceAfter) {
                                updateLayout();
                                runOnceAfter = false;
                            }
                        }, 300);
                    } else {
                        runOnceAfter = true;
                    }
                }

            }
        };
    })
 ;

