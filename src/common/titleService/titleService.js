angular.module( 'titleService', [])

.factory( 'titleService', function ( $document ) {
  var prefix, suffix, title;
  
  var titleService = {
    setPrefix: function setPrefix ( s ) {
      prefix = s;
    },

    getPrefix: function getPrefix () {
      return prefix;
    },

    setSuffix: function setSuffix ( s ) {
      suffix = s;
    },
    
    getSuffix: function getSuffix () {
      return suffix;
    },

    setTitle: function setTitle ( t ) {
      if (t) {
        if ( angular.isDefined( prefix ) ) {
          title = prefix + t;
        } else if ( angular.isDefined( suffix )  ) {
          title = t + suffix;
        } else {
          title = t;
        }
      } else {
        title = "Alcove";
      }
      

      $document.prop( 'title', title );
    }, 
    
    getTitle: function getTitle () {
      return $document.prop( 'title' );
    }
  };

  return titleService;
})

;

