angular.module( 'alcove.sliderThumb', [] )
    .directive('sliderRow', function() {
        // return {
        //     replace: false,
        //     transclude: true,
        //     controller : ['$scope', '$timeout', '$element', function($scope, $timeout, $element) {
        //         $timeout(function() {
        //             console.log( angular.element($element[0]) );
        //             $element[0].addEventListener("scroll", function(event) {
        //                 console.log('Scroll!');
        //                 console.log(event);
        //             });
        //         });
        //     }]
            
        // };
        return function($scope, $element, attrs) {
                console.log( angular.element($element[0]) );
                var waiting = false;
                $element[0].addEventListener("scroll", function(event) {
                    // Debounce
                    if (!waiting) {
                        waiting = true;
                        setTimeout(function() {
                            waiting = false;
                            var i = 0;
                            var rowLeftScroll = angular.element($element[0])[0].scrollLeft;
                            angular.forEach(angular.element($element[0]).find('thumbnail'), function(val, key) {
                                i++;
                                var labelEl = angular.element(val).find('a')[0];
                                if (labelEl) {
                                    if ( rowLeftScroll > val.offsetLeft + val.clientWidth ) {
                                        // Hide labels
                                        labelEl.style.opacity = 0;
                                    } else {
                                        labelEl.style.opacity = 1;
                                    }
                                }
                            });
                        }, 200);
                    }
                    
                });
        };
    })
    .directive( 'sliderThumb', function() {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                assetId: '@',
                thumbnailUrl: '@',
                labelOne: '@',
                labelTwo: '@',
                labelThree: '@',
                mediaType: '@',
                downloadUrl: '@',
                downloadName: '@',
                numbered: '@',
                uri: '@',
                imageWidth: '@',
                imageHeight: '@',
                externalAssetIndex: '@',
                externalApi: '@',
                firstAsset: '=',
                size: '@',
                sliderId: '@',
                index: '='
            },
            templateUrl: 'sliderThumb/sliderThumb.tpl.html',
            controller : ['$scope', '$timeout', '$rootScope', '$element', function($scope, $timeout, $rootScope, $element) {


                $scope.wide = false;
                $scope.tall = false;
                $scope.setUrl = "";
                if (!$scope.firstAsset) {
                    $scope.firstAsset = false;
                } else if ($scope.firstAsset === true) {
                    $scope.isActive = true;
                }

                // Set URL for thumbnail image
                if ($scope.size) {
                    $scope.setUrl = $scope.thumbnailUrl.replace("size2","size" + $scope.size);
                } else if ($scope.setUrl.length < 1) {
                    $scope.setUrl = $scope.thumbnailUrl;
                }

                // Fires for every image load, which might be too much?
                angular.element($element[0]).find('img')[0].onload = function() {
                    angular.element($element[0]).addClass('hide-blank');
                    angular.element($element[0]).find('img').css('display', 'block');
                    $rootScope.$broadcast('newThumbnailData');
                };
                
                $scope.getLink = function() {
                  if ($scope.uri) {
                      return $scope.uri;
                  } else {
                      return '#/item/' + $scope.assetId;
                  }
                };

                $scope.openUrl = function(url) {
                    // Prevent from firing when overlayed buttons are clicked
                    // if ($($event.target).hasClass('button') || $($event.target.parentNode).hasClass('button') ) {
                    //     return;
                    // }
                    // if($scope.externalApi === 'lacma'){
                    //     return;
                    // }
                    if($scope.uri){
                        window.open($scope.uri, '_blank');
                    }
                    else{
                        window.open(url, '_self');
                    }
                };

                $scope.trackDownload = function() {
                    ga('send', {
                      hitType: 'event',
                      eventCategory: 'asset_download_by_collection',
                      eventAction: 'SSC',
                      eventLabel: $scope.assetId
                    });
                };

                $scope.showShareOptions = function(){
                    $scope.$parent.$parent.$parent.$parent.shareUrl = window.location.origin + window.location.pathname + '#/item/' + $scope.assetId; //window.location.href;
                    $scope.$parent.$parent.$parent.$parent.shareMediaUrl = $scope.setUrl;
                    $scope.$parent.$parent.$parent.$parent.shareDesc = $scope.labelTwo;
                    $scope.$parent.$parent.$parent.$parent.showShareOptions();
                }; 
                
                $scope.makeActive = function() {
                    $rootScope.$broadcast($scope.sliderId, $scope.assetId);
                };
                
                $rootScope.$on($scope.sliderId, function(event, id) {
                    if (id == $scope.assetId) {
                        $scope.isActive = true; 
                    } else {
                        $scope.isActive = false;
                    } 
                });

            }]
        };
    })
 ;
