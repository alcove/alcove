angular.module( 'alcove.assetViewer', [] )
.directive('assetViewer', function() {
  return {
    templateUrl: 'assetViewer/assetViewer.tpl.html',
    replace: true,
    scope: {
      asset: '=',
      fixedPosition: '=',
      index: '=',
      removableAsset: '='
    },
    controller: ['$scope', '$http', '$timeout', '$log', '$sce', '$rootScope', 'findAssetField', 'hotkeys', 
      function($scope, $http, $timeout, $log, $sce, $rootScope, findAssetField, hotkeys){

      $scope.findAssetField = findAssetField;
      $scope.OSDFileTypes = ["img","image","file","pdf","ppt","doc"];
      $scope.KalturaFileTypes = ["video", "audio", "vid", "aud"];
      
      var assetLoaded = false;
      var tilesource = [];

      $timeout( function() {
        $scope.$watch($scope.asset, function() {
          if ($scope.asset && $scope.asset['Meta-Id'] && !assetLoaded) {
            assetLoaded = true;
            $http.get('http://catalog.sharedshelf.artstor.org/iiifmap/ss/' + $scope.asset['Meta-Id'])
              .success(function(res) {
                tileSource = res.info_url;
                if ($scope.KalturaFileTypes.indexOf(findAssetField.filetype($scope.asset)) > 0) {
                  $scope.loadKaltura();  
                } else {
                  $scope.loadOpenSea($scope.asset['Meta-Id']);
                }
            })
            .error(function(res) {
              // No IIIF URLs
              $log.warn("No IIIF tiles to load");
              $scope.mediaLoadingFailed = true;
            });  
          }
        });
      });

      function setColumnHeight() {
        // Column height adjustment
        if(!$scope.fullscreen && $scope.asset && $scope.asset['Meta-Id'] && document.getElementById('wrap-' + $scope.asset['Meta-Id'] + '-' + $scope.index) ){
          document.getElementById('wrap-' + $scope.asset['Meta-Id'] + '-' + $scope.index).style.height = ( window.innerHeight - 162) + 'px';
          document.getElementById('columnTwoContent').style.minHeight = ( window.innerHeight - 50) + 'px';
        }
      }
      
      $scope.loadOpenSea = function(id) {
        // OpenSeaDragon Initializer
        id = id  + '-' + $scope.index;

        var viewer = new OpenSeadragon({
          id: 'viewer-' + id,
          prefixUrl: 'assets/images/',
          tileSources:   tileSource,
          gestureSettingsMouse : {
            scrollToZoom : true,
            pinchToZoom: true
          },
          controlsFadeLength: 500,
          autoHideControls: false,
          zoomInButton: 'zoomIn-' + id,
          zoomOutButton: 'zoomOut-' + id,
          homeButton: 'zoomFit-' + id,
          sequenceMode: true,
          initialPage: 0,
          nextButton: 'nextButton'
        });


        if( $scope.index == 1) {
           hotkeys      
            .bindTo($scope)
            .add({
              combo: ['+','='],
              description: 'Image viewer: Zoom In',
              callback: function() {
                viewer.viewport.zoomBy(1.2);
              }
            })
            .add({
              combo: ['-'],
              description: 'Image viewer: Zoom Out',
              callback: function() {
                viewer.viewport.zoomBy(0.8);
              }
            });
        }
        
        // Check if Tile Source is available
        // ---- The OpenSeaDragon handler is too slow for providing a fallback
        $http.get(tileSource)
          .success(function(response) {
            $log.info("Tile source is available for asset " + $scope.index);

            // This is a temporary lie, since the 'ready' handler fails to trigger
            $scope.openSeaDragonReady = true;
          })
          .error(function(data){
            $log.warn("Loading tiles failed");
            $scope.mediaLoadingFailed = true;
            viewer.destroy();
          });
          
        // ---- Use handler in case other error crops up
        viewer.addHandler('open-failed', function() {
          $log.warn("Opening source failed");
          $scope.mediaLoadingFailed = true;
          viewer.destroy();
        });
        
        viewer.addHandler('tile-load-failed', function() {
          $log.warn("Loading tiles failed");
          $scope.mediaLoadingFailed = true;
          viewer.destroy();
        });

        viewer.addHandler('ready', function() {
          $log.info("Tiles are ready");
          $scope.openSeaDragonReady = true;
        });

        $timeout(function() {
          if(viewer && viewer.ButtonGroup){
            viewer.ButtonGroup.element.addClass('button-group');
          }
          document.getElementById('viewer-' + id).appendChild( document.getElementById('imageButtons-' + id) );
          setColumnHeight();
        });
      };

      window.addEventListener('resize', setColumnHeight);

      function requestFullScreen(el) {
        // Supports most browsers and their versions.
        var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullScreen;

        if (requestMethod) { // Native full screen.
          requestMethod.call(el);
        } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
          var wscript = new ActiveXObject("WScript.Shell");
          if (wscript !== null) {
              wscript.SendKeys("{F11}");
          }
        }
      }

      function exitFullScreen() {
        if (document.cancelFullScreen) {
          document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
          document.webkitCancelFullScreen();
        }
      }


      $scope.fullscreen = false; //false;

      var changeHandler = function(){                      
        if (document.webkitIsFullScreen || document.mozFullScreen || document.fullscreen) {
          $scope.fullscreen = true;
        } else {
          $scope.fullscreen = false;
          $timeout(function() {
            // Fix for when setColumnHeight runs during fullscreen mode
            // 3s wait to ensure it runs after exiting fullscreen animation is complete
            setColumnHeight();
          }, 3000);
        }  
        // Angular isn't applying scope after the 'ESC' to exit key event
        $scope.$apply();     
      };

      document.addEventListener('fullscreenchange', function () {
            changeHandler();
        }, false);

        document.addEventListener('mozfullscreenchange', function () {
            changeHandler();
        }, false);

        document.addEventListener('webkitfullscreenchange', function () {
            changeHandler();
        }, false);

      $scope.togglePresentationMode = function(){

        var elem = document.body; // Make the body go full screen.

        if (!$scope.fullscreen) {
          ga('send', {
            hitType: 'event',
            eventCategory: 'Access',
            eventAction: 'Enter Full Screen Mode',
            eventLabel: $scope.assetId
          });
          requestFullScreen(elem);
          $scope.fullscreen = true;
        } else {
          exitFullScreen();
          $scope.fullscreen = false;
        }
      };

      if (document.getElementById('fallbackViewer')) {
        angular.element( document.getElementById('fallbackViewer') ).find('img')[0].onerror = function() {
          $scope.fallbackFailed = true;
        };
      }
      
      $scope.removeComparedAsset = function(assetId) {
        // Verify asset can be removed
        if ($scope.removableAsset) {
          // Tell item.js to remove asset from array
          $rootScope.$broadcast('removeComparedAsset', assetId);
        }
      };

      // WORKAROUND: Get IDs and build the Kaltura url
      var kalturaId = "";
      var spId = "";

      // For media player backups
      $scope.mediaUrl = $sce.trustAsResourceUrl( $scope.asset.File.url.replace('stor//','stor/') );
      $scope.mediaType = $scope.asset.File.format;
      
      $scope.loadKaltura = function() {
        $http.get($scope.asset.File.url.replace('stor//','stor/') + '_kplayer')
          .success(function(data) {
            var htmlPage = data;
            spId = htmlPage.slice(htmlPage.indexOf('/sp/') + 4 );
            spId = spId.slice(0,spId.indexOf('/'));
            htmlPage = htmlPage.slice(htmlPage.indexOf('thumbnail/entry_id/') + 19 );
            kalturaId = htmlPage.slice(0,htmlPage.indexOf('/'));
              
            kWidget.embed({
              'targetId': 'video-' + $scope.asset['Meta-Id'] + '-' + $scope.index,
              'wid': '_101',
              'uiconf_id' : '23448189',
              'entry_id' : kalturaId,
              'flashvars': {
                'fullScreenBtn.plugin': false
              },
              'readyCallback' : function(playerId) {
                var kdp = document.getElementById( playerId );
                kdp.kBind( 'mediaError', function(){
                  if (findAssetField.filetype($scope.asset) === 'aud') {
                    document.getElementById(playerId).style.display = 'none';
                    document.getElementById(playerId.replace('video','audio')).style.display = 'block';
                  } else {
                    $scope.mediaLoadingFailed = true;
                    $scope.$apply();
                  }
                });
              }
            }); 
          })
          .error(function(data) {
            console.log('Failed to find Kaltura!');
            $scope.mediaLoadingFailed = true;
          });
      };
        
          
      $scope.isPDF = function() {
        if ($scope.asset && $scope.asset.File) {
          if ($scope.asset.File.type.indexOf('pdf') > -1) {
            return true;
          }
          if ($scope.asset.File.format.indexOf('pdf') > -1) {
            return true;
          }
        }
        return false;
      };
      
      $scope.isPPT = function() {
        if ($scope.asset && $scope.asset.File) {
          if ($scope.asset.File.type.indexOf('ppt') > -1) {
            return true;
          }
          if ($scope.asset.File.format.indexOf('ppt') > -1) {
            return true;
          }
        }
        return false;
      };

    }],
    link: function(scope, element, attrs){

    }
  };
});
