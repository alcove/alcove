angular.module( 'alcove.assetThumbnail', [] )
    .directive( 'assetThumbnail', function() {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                assetId: '@',
                thumbnailUrl: '@',
                labelOne: '@',
                labelTwo: '@',
                labelThree: '@',
                mediaType: '@',
                downloadUrl: '@',
                downloadName: '@',
                numbered: '@',
                uri: '@',
                useMasonry: '@',
                imageWidth: '@',
                imageHeight: '@',
                externalAssetIndex: '@',
                externalApi: '@',
                lastAsset: '@',
                size: '@',
                noLink: '='
            },
            templateUrl: 'thumbnail/thumbnail.jade',
            controller : ['$scope', '$timeout', '$rootScope', '$element', function($scope, $timeout, $rootScope, $element) {


                $scope.wide = false;
                $scope.tall = false;
                $scope.setUrl = "";

                if (!$scope.lastAsset) {
                    $scope.lastAsset = false;
                }

                // Set URL for thumbnail image
                if ($scope.size) {
                    $scope.setUrl = $scope.thumbnailUrl.replace("size2","size" + $scope.size);
                } else if ($scope.setUrl.length < 1) {
                    $scope.setUrl = $scope.thumbnailUrl;
                }

                if ($scope.useMasonry) {
                    if ( $scope.imageWidth && $scope.imageHeight) {
                        if ( ($scope.imageWidth/$scope.imageHeight) > 2.25 ) {
                            $scope.wide = true;

                            if ( $scope.thumbnailUrl.indexOf("size2") > -1 ) {
                                $scope.setUrl = $scope.thumbnailUrl.replace("size2","size3");
                            }
                        }

                        // Portrait
                        if ( ($scope.imageHeight/$scope.imageWidth) > 0.64 ) {
                            $scope.tall = 1;

                            if ( ($scope.imageHeight/$scope.imageWidth) > 1) {
                                $scope.tall = 2;

                                if ( ($scope.imageHeight/$scope.imageWidth) > 1.2) {
                                    $scope.tall = 3;
                                    $scope.setUrl = $scope.thumbnailUrl.replace("size2","size3");

                                    if ( ($scope.imageHeight/$scope.imageWidth) > 1.4) {
                                        $scope.tall = 4;
                                        
                                        if ( ($scope.imageHeight/$scope.imageWidth) > 1.6) {
                                            $scope.tall = 5;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                // Fires for every image load, which might be too much?
                angular.element($element[0]).find('img')[0].onload = function() {
                    angular.element($element[0]).addClass('hide-blank');
                    angular.element($element[0]).find('img').css('display', 'block');
                    $rootScope.$broadcast('newThumbnailData');
                };

                $scope.thumbnailClicked = function(externalApi){
                    if(externalApi){
                        ga('send', {
                            hitType: 'event',
                            eventCategory: 'Access',
                            eventAction: 'Link to Open Web Item',
                            eventLabel: externalApi
                        });
                    }
                };

                $scope.openUrl = function($event, url) {
                    // Prevent from firing when overlayed buttons are clicked
                    if ($($event.target).hasClass('button') || $($event.target.parentNode).hasClass('button') ) {
                        return;
                    }
                    if($scope.uri){
                        window.open($scope.uri, '_blank');
                    }
                    else{
                        window.open(url, '_self');
                    }
                };

                $scope.trackDownload = function() {
                    ga('send', {
                      hitType: 'event',
                      eventCategory: 'asset_download_by_collection',
                      eventAction: 'SSC',
                      eventLabel: $scope.assetId
                    });
                };

                $scope.showShareOptions = function(){
                    $rootScope.$broadcast('showShareOptions', {
                        'shareUrl' : window.location.origin + window.location.pathname + '#/item/' + $scope.assetId,
                        'shareMediaUrl' : $scope.setUrl,
                        'shareDesc' : $scope.labelTwo
                    });
                };

            }]
        };
    })
 ;
