# Styleguide options

### Head

    link(rel='stylesheet' href='../assets/main.css')
    link(rel='stylesheet' href='../assets/docs/styledown.css')
    link(rel='stylesheet' href='../assets/docs/styledown_custom.css')
    script(src='styledown.js')

### Body
	div#main
	    h1.page-title Alcove Style Guide
	    section
	    	h2.index Docs Index
			h4.sg
				a(href='../docs/') JS Docs
			h4.sg
				a(href='../styleguide/') Style Docs  
			hr
	    section(sg-content)