/**
 * @fileOverview Handles application's authentication, access to Artstor Digital Library and external api(s).
 * @author Cody & Rehan
 * @namespace Services
 * @version 1.0.1
 */

 /** @type {string}
   * @default
   * @description Base URL for the server
   * @memberof Services
   */
var baseUrl;
// Check to see if prod environment
if ( window.location.host.indexOf('artstor.org') > -1 ) {
  baseUrl = "http://alcove.artstor.org";
} else if ( window.location.host.indexOf('ec2-54-152-89-174') > -1 ) {
  baseUrl = "http://ec2-54-152-89-174.compute-1.amazonaws.com";
} else {
  // Fall back to Dev environment calls
  baseUrl = "http://ec2-54-88-159-164.compute-1.amazonaws.com";
}

angular.module('alcove.services',[])
/**
 * @type service
 * @name schema
 * @description Provides global a object for mapping labels
 * @memberof Services
 */
.constant('schema', {
  "title" : "Title",
  "dateBegin" : "DateRangeBegin",
  "dateEnd" : "DateRangeEnd",
  "collections" : "Collections",
  "genre" : "Genre",
  "institutionId" : "Meta-InstitutionId",
  "media" : "Media",
  "file" : "File",
  "id" : "Meta-Id",
  "art" : {
    "title" : "ART-Title",
    "creator" : "ART-Creator",
    "material" : "ART-Material",
    "culture" : "ART-Culture",
    "subject" : "ART-Subject",
    "location" :"ART-Location",
    "workType" : "ART-Work-Type",
    "style" : "ART-Style-Period",
    "browseCountry" : "ART-BrowseCountry",
    "browseClass" : "ART-BrowseClass"
  },
  "ART-Creator" : "ART-Creator",
  "creator" : "Name",
  "date" : "Date",
  "Location" : "Location",
  "Place" : "Place",
  "record" : "Source-Record",
  "name" : "name",
  "type" : "type",
  "oldestDate" : "OldestDate",
  "description" : "Description",
  "facets" : [
    "Genre",
    "Meta-InstitutionId",
    "Collections",
    "Media",
    "ART-Creator",
    "ART-Culture",
    "Location",
    "ART-Location",
    "ART-Material",
    "ART-Work-Type",
    "ART-Style-Period",
    "ART-Subject",
    "ART-BrowseCountry",
    "ART-BrowseClass",
    "Title"
  ]
})
.factory('findAssetField', function(schema) {

  /**
   * @function
   * @name hasGeneratedThumb
   * @description Check if the asset has a thumnail image or not.
   * @param {Object} asset - Asset object to be checked.
   * @returns {boolean} Returns true if the asset has a thumbnail image else returns false.
   * @memberof findAssetField
   */
  function hasGeneratedThumb(asset) {
        if(!asset){
          return;
        }
        // Avoids the small Artstor logo thumbnails the service returns
        if (asset['File'] && typeof asset['File']['type'] == 'undefined') {
           return false;
        } else if (asset['File']) {
           return asset['File']['type'].length > 0;
        }
        return false;
  }
  
  function arrayCheck(array) {
      if ( angular.isArray(array) && array[0] && array[0].length > 0) {
        return arrayCheck(array[0]);
      } else if (array.length > 0) {
        return array;
      }
  }

  return {
    /**
     * @function
     * @name title
     * @description Find asset title, with fallbacks
     * @returns {String} Title
     * @memberof Services
     */
    title: function(asset) {
      if(!asset){
          return;
      }
      if (asset._source) {
            asset = asset._source;
      }
      if (asset[schema.title]) {
        if ( arrayCheck(asset[schema.title]).length > 0 ) {
            return arrayCheck( asset[schema.title] );
        } 
      } 
    //   else if (asset[schema.file].name) {
    //     return asset[schema.file].name;
    //   }
      return "";
    },
    
    date: function(asset) {
        if(!asset){
            return;
        }
        if (asset._source) {
            asset = asset._source;
        }
        var genre = "";
        if (asset[schema.genre]) {
            genre = asset[schema.genre].toUpperCase() + '-';
        }
        // Currrently guessing by pulling first Agent
        if ( asset[genre + schema.date] ) {
            if ( arrayCheck(asset[genre + schema.date]).length > 0 ) {
                return arrayCheck( asset[genre + schema.date] );
            } else {
               return asset[genre + schema.date];
            }
        }
        if (asset[schema.date]) {
            return asset[schema.date];
        }
        return "";    
    },
    
    creator: function(asset) {
        if(!asset){
            return;
        }
        if (asset._source) {
            asset = asset._source;
        }
        var genre = "";
        if (asset[schema.genre]) {
            genre = asset[schema.genre].toUpperCase() + '-';
        }
        // Currrently guessing by pulling first Agent
        if ( asset[genre + schema.creator] ) {
           if ( arrayCheck(asset[genre + schema.creator]).length > 0 ) {
                return arrayCheck( asset[genre + schema.creator] );
           } 
        }
        if (asset[schema.creator]) {
             if ( arrayCheck(asset[schema.creator]).length > 0 ) {
                return arrayCheck( asset[schema.creator] );
            } else if (asset[schema.creator].length > 0) {
               return asset[schema.creator];
            }
        }
        return "";
    },
    
    description: function(asset) {
      if(!asset){
          return;
      }
      if (asset[schema.description]) {
          return asset[schema.description];
      }
      if (asset._source) {
        if (asset._source[schema.description]) {
          return asset[schema.description];
        }
        if (asset._source[schema.record]) {
          return asset._source[schema.record][schema.description];
        }
      }
      if (asset[schema.record]) {
        return asset[schema.record][schema.description];
      }
      return "";
    },
    /**
     * @function
     * @name thumbUrl
     * @description Get Asset thumbnail image URL.
     * @param {Object} asset - Asset object whose thumbnail image URL is required.
     * @param {Number} size - Thumbnail size, 0 - 4, 4 is largest.
     * @returns {string} Asset thumbnail image URL.
     * @memberof findAssetField
     */
    thumbUrl: function(asset, size) {
        if(!asset){
          return;
        }
        if (asset._source) {
          asset = asset._source;
        }
        if (hasGeneratedThumb(asset) ) {
            if(asset[schema.file]["type"] == 'video'){
              return asset[schema.file]["data"]["imageUrl"] + "_size" + size;
            } else {
              return asset[schema.file]["url"].replace('stor//','stor/') + "_size" + size;
            }
        } else {
            return "";
        }
      },
      /**
       * @function
       * @name filename
       * @description Get original asset filename
       * @param {Object} asset - Asset object whose filename is required.
       * @returns {string} Asset filename with extension
       * @memberof findAssetField
       */
      filename: function(asset) {
        if(!asset){
            return;
        }
        if (asset._source) {
            asset = asset._source;
        }
        if (asset[schema.file]) {
          return asset[schema.file][schema.name];
        }
        return "";
      },
      /**
       * @function
       * @name filetype
       * @description Get original asset filetype
       * @param {Object} asset - Asset object whose filetype is required.
       * @returns {string} Asset file media type
       * @memberof findAssetField
       */
      filetype: function(asset) {
        if(!asset){
            return;
        }
        if (asset._source) {
            asset = asset._source;
        }
        if (asset[schema.file]) {
          return asset[schema.file][schema.type];
        }
        return "";
      },
      /**
       * @function
       * @name id
       * @description Get original asset id
       * @param {Object} asset - Asset object whose id is required.
       * @returns {string} Asset id with extension
       * @memberof findAssetField
       */
      id: function(asset, size) {
        if(!asset){
            return;
        }
        if (asset._source) {
            asset = asset._source;
        }
        if (asset[schema.id]) {
          return asset[schema.id];
        }
        return "";
      },
      
      downloadUrl: function(asset) {
          if (!asset) {
              return;
          }
          if (asset._source) {
            asset = asset._source;
          }
          if (asset[schema.file] && asset[schema.file]['data']) {
            return asset[schema.file]['data']['imageUrl'];
          }
          if (asset[schema.file]) {
            return asset[schema.file]['url'];
          }
          return "";
      }
  };
})
.factory('account', function($http, $q, $log, $rootScope, $window, $localStorage) {
    /**
     * @function
     * @name formEncode
     * @description Encodes the form data
     * @param {Object} obj - Object bind with the form data
     * @returns {Object} Encoded object corresponding to form data
     * @memberof Services
     */
    function formEncode(obj) {
        var encodedString = '';
        for (var key in obj) {
            if (encodedString.length !== 0) {
                encodedString += '&';
            }

            encodedString += key + '=' + encodeURIComponent(obj[key]);
        }
        return encodedString.replace(/%20/g, '+');
    }

    var self = this;
    self.data = {};

    /** @type {string}
      * @default
      * @description Base URL for Authentication calls
      * @memberof Services
      */
    var authUrl =  baseUrl + "/service/api/user/json/authenticate?userName=qah001@artstor.org&password=artstor&expiry=1000";

    return {
        /**
         * @function
         * @name authenticate
         * @description To authenticate the user (current with dummy credentials i.e. userName=qah001@artstor.org & password=artstor)
         * @returns {Object} Authentication response
         * @memberof Services
         */
        authenticate: function(){
            return $http.get(authUrl, {});
        },

        /**
         * @function
         * @name signin
         * @description To authenticate the user with email & password
         * @param {string} email - User's Email Id
         * @param {string} password - User's Password
         * @returns {Object} Authentication response
         * @memberof Services
         */
        signin: function(email, password) {
            return $http({
                method: 'POST',
                url: baseUrl + "/session",
                data: formEncode({ "email" : email, "password" : password })
            }).success(function(response) {
                self.data = response;

                $log.debug("Logged in user:");
                $log.debug(self.data);

                return selfie.fetchAuthTokenInfo(function() {
                    $window.location.href = url;
                });
            }).error(function(response) {
                $log.debug(response);
            });
        },

        /**
         * @function
         * @name fetchAccountInfo
         * @description To get Signed In user's account info.
         * @returns {Object} Signed In user's account info.
         * @memberof Services
         */
        fetchAccountInfo: function() {
            var selfie = this;
            return $http({
                method: 'GET',
                url: baseUrl + "/session",
                withCredentials: true
            });
        },

        /**
         * @function
         * @name signout
         * @description To Sign Out current Signed In user.
         * @returns {Object} Success / Failure info.
         * @memberof Services
         */
        signout: function () {
                console.log("Signing out...");

                return $http({
                    method: 'DELETE',
                    url: baseUrl + "/account",
                    withCredentials: true
                }).success(function(response) {
                    console.log("Logged out.");
                    return true;
                }).error(function(response) {
                    console.log("Failed to log out");
                });
        },

        // Not yet functioning, but needed to get user first and last name
        /**
         * @function
         * @name fetchUserInfo
         * @description To get Signed In user's account info.
         * @returns {Object} Signed In user's account info.
         * @memberof Services
         */
        fetchUserInfo: function() {
          return $http({
                method: 'GET',
                url: baseUrl + "/userinfo",
                withCredentials: true
            });
        },

        /**
         * @function
         * @name fetchAuthTokenInfo
         * @description To get authentication token for the Signed In user.
         * @param {string} callback - Method to which the response object containing the authentication token shall be passed.
         * @returns {Object} Response object containing the authentication token.
         * @memberof Services
         */
        fetchAuthTokenInfo: function(callback) {
            /**
             * Generate Authentication & Names Token
             */
            var selfie = this;

            var profileId = $window.localStorage["profile_id"];

            var institutionId = $window.localStorage["institution_id"];

            return $q.all([
                $http.get(baseUrl + '/names/userManagement/json/authenticate?adlUserId=' + profileId + '&instituteId=' + institutionId + '&expiry=24'),
                $http.get(baseUrl + '/service/api/user/json/authenticate?adlUserId=' + profileId + '&instituteId=' + institutionId + '&expiry=24')
            ]).then(
                function(responses) {
                    console.log(responses);
                    self.data.namesAuthentication = responses[0].data;
                    self.data.authentication = responses[1].data;

                    $window.localStorage.namesAuthToken = self.data.namesAuthentication.authToken;
                    $window.localStorage.authToken = self.data.authentication.authToken;
                    $window.localStorage.instituteName = self.data.authentication.instituteName;

                    if (callback) {
                        callback();
                    }
                }
            );
        },

        // getAuthToken: function() {
        //     return $window.localStorage.authToken;
        // },

        // getNamesAuthToken: function() {
        //     return $window.localStorage.namesAuthToken;
        // },

        // getInstituteName: function() {
        //     return $window.localStorage.instituteName;
        // },

        /**
         * @function
         * @name getProjects
         * @description To get all the asset projects for the current Signed In user.
         * @returns {Object} Response object containing the asset projects for the current Signed In user.
         * @memberof Services
         */
        getProjects: function() {
            var params = {};
            params.instituteName = this.getInstituteName();
            params.authToken = this.getAuthToken();

            $http
                .get(baseUrl + "/projects", {
                    //params: params,
                    withCredentials: true
                })
                .success(function(response) {
                    console.log(response);
                    callback(response);
                });
        }
    };
})

.factory('assets', function($http, $log, account, $q, $location, schema) {
    /** @type {string}
      * @default
      * @description Base URL for asset search calls
      * @memberof Services
      */
    var searchUrl = baseUrl + "/es2/alcove/_search";

    var params = {};

    return {
        /**
         * @function
         * @name simpleSearch
         * @description Searches for assets in the elastic search index with the given query term.
         * @param {string} query - Term to be searched.
         * @param {string} size - Number of results to be returned.
         * @returns {Object} Reponse object containing the result assets.
         * @memberof Services
         */
        simpleSearch: function(query, size) {
            if (!size) {
                size = 25;
            }
            params.q = "Title:" + query;
            params.size = size;

            var getLink = searchUrl;

            return $http.get(getLink, {
                params: params
            });
        },
        /**
         * @function
         * @name getById
         * @description Fetch asset meta data
         * @param {string} id - Id of the asset whose meta data is to be fetched
         * @returns {Object} Reponse object containing the asset meta data
         * @memberof Services
         */
        getById: function(id) {
            // For fetching asset data
            var getLink = searchUrl + "?q=_id:" + id;
            return $http.get(getLink);
        },

        /**
         * @function
         * @name getBulkAssetsById
         * @description Fetch asset meta data for multiple assets
         * @param {string[]} id - Array of string id(s) of the assets whose meta data is to be fetched
         * @returns {Object} Reponse object containing the assets meta data
         * @memberof Services
         */
        getBulkAssetsById: function(assetIds){
            var requestArray = [];
            angular.forEach(assetIds, function(assetId) {
                requestArray.push($http({method: 'GET', url: searchUrl + "?q=_id:" + assetId}));
            });
            return $q.all(requestArray);
        },

        /**
         * @function
         * @name openById
         * @description Fetch asset meta data while displaying the asset in view mode. Also the view count for the asset is incremented.
         * @param {string} id - Id of the asset whose meta data is to be fetched and the view count is to be incremented.
         * @returns {Object} Reponse object containing the asset meta data
         * @memberof Services
         */
        openById: function(id) {

            // For fetching asset data
            var getLink = searchUrl + "?q=_id:" + id;
            return $http.get(getLink);
        },

        /**
         * @function
         * @name getRelatedDRs
         * @description Get DRs associated with the Work record.
         * @param {string} id - Id of the Work record whose DRs are to be fetched.
         * @param {string} callback - Method to which the response object shall be passed as a parameter.
         * @returns {Object} Reponse object containing Related DRs info.
         * @memberof Services
         */
        getRelatedDRs: function(id, callback) {
            if (!id) {
                console.log("No Work Id found!");
                return;
            }

            var params = {};

            params.authToken = '123';
            params.pageNumber = '1';
            params.pageSize = '1000';
            params.workIndexId = id;
            params.callback = 'JSON_CALLBACK';

            var getLink = 'http://catalog.sharedshelf.artstor.org' + '/service/api/work/json/getDisplayRecordsByWork';

            $http
                .jsonp(getLink, {
                    params: params
                })
                .success(function(response) {
                    callback(response);
                });

        },

        /**
         * @function
         * @name getBulkDRsInfo
         * @description Get Bulk DRs Details.
         * @param {string[]} drIDsArray - String array of DR ids.
         * @returns {Object} Reponse object containing array of DRs info.
         * @memberof Services
         */
        getBulkDRsInfo: function(drIDsArray){
            var requestArray = [];
            angular.forEach(drIDsArray, function(id) {
                requestArray.push($http({ method: 'GET', url: searchUrl + "?q=_id:" + id }));
            });
            return $q.all(requestArray);
        },

        /**
         * @function
         * @name getWikiInfo
         * @description Get information about the user entered search term from wikipedia.
         * @param {string} title - User enterd search term.
         * @returns {Object} Reponse object containing the wikipedia information about the search term.
         * @memberof Services
         */
        getWikiInfo: function(title) {
            var getLink = 'https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&explaintext=true&exintro=true&redirects=true&callback=JSON_CALLBACK&exlimit=max&titles=' + encodeURIComponent(title);
            return $http.jsonp(getLink);
        },

        /**
         * @function
         * @name getCachedData
         * @description Get cached data via page cache service.
         * @returns {Object} Reponse object containing cached data for the blog posts and recent assets.
         * @memberof Services
         */
        getCachedData: function() {
            var getLink = baseUrl + '/api/v1/cache/root';
            return $http.get(getLink);
        },

        /**
         * @function
         * @name search
         * @description Search for assets in the elastic search index along with the facted information about the result assets.
         * @param {string} query - User enterd search term.
         * @param {string} page - Page number from which the asset results should be fetched (used for implementing pagination).
         * @param {string} size - Number of asset results to be included in each page (used for implementing pagination).
         * @param {string[]} filters - String array of filters to be used for searching assets.
         * @param {boolean} titleSort - Whether to sort by title or not.
         * @param {boolean} dateSort - Whether to sort by date or not.
         * @returns {Object} Reponse object containing the asset results along with thier facets.
         * @memberof Services
         */
        search: function(query, page, size, filters, rangeQuery, titleSort, dateSort, newestSort, newRelease, filtersExclude, booleanTerm, fuzzyFallback) {
            page = page - 1;
            
            // Begin facet object with custom OldestDate

            var facets = {
                "EarliestDate" : {
                    "min" : { "field" : schema.dateBegin }
                },
                "LatestDate" : {
                    "max" : { "field" : schema.dateEnd }
                }
            };

            // Loop through set facets to show
            for (var i = 0; i < schema.facets.length; i++) {
                facets[schema.facets[i]] = {
                  "terms" : {
                      "field" : schema.facets[i],
                      "size" : 100
                  }  
                };
            }

            // Set maximum time the search query can take
            var timeout = "4s";
            
            // Forward slash escaping for Elastic
            query = query.replace(/\//g, '\\/');
            
            // Build query formatted for Bool
            var data = {
                "size" : size,
                "from" : page * size,
                "query" : {
                  "bool" : {
                        "must" : [
                                { 
                                    "query" : {
                                        "query_string" : {
                                            "query" : query,
                                            "default_operator" : booleanTerm
                                        }
                                }
                            }
                        ],
                        "must_not" : [],
                        "should" : [
                            {
                                "match": {
                                    "ART-Location": {
                                    "query": query,
                                    "boost": 10,"operator" : booleanTerm
                                    }
                                }
                            },
                            {
                                "match": { 
                                    "ART-BrowseClass": {
                                        "query": query,
                                        "boost": 10 ,"operator" : booleanTerm
                                        }
                                }
                            },
                            {
                                "match": { 
                                    "ART-Subject": {
                                        "query": query,
                                        "boost": 10 ,"operator" : booleanTerm
                                        }
                                }
                            },
                            {
                                "match": { 
                                    "Name": {
                                        "query": query,
                                        "boost": 10,"operator" : booleanTerm
                                        }
                                }
                            },
                            {
                                "match": { 
                                    "ART-Work-Type": {
                                        "query": query,
                                        "boost": 10,"operator" : booleanTerm
                                        }
                                }
                            }, {
                                "match": { 
                                    "ART-Culture": {
                                        "query": query,
                                        "boost": 10,"operator" : booleanTerm
                                        }
                                }
                            }, {
                                "match": { 
                                    "ART-Style-Period": {
                                        "query": query,
                                        "boost": 5,"operator" : booleanTerm
                                        }
                                }
                            }
                        ]
                    }  
                },
                "aggs" : facets,
                "timeout" : "4s"
                //,
                // "_source": {
                //     "include": ["Genre", "ART-Title", "Title", "ART-Creator", "File", "Date", "Source-Record", "DateRangeBegin", "DateRangeEnd"]
                // }
            };

            if (fuzzyFallback) {
                // Remove query_string
                delete data["query"]["bool"]["must"][0];
                // Add fuzzy match _all
                data["query"]["bool"]["must"].push( { "match" : {
                    "_all" : {
                        "query" : query,
                        "operator" : booleanTerm,
                        "fuzziness" : "AUTO",
                        "zero_terms_query": "all"
                    }
                } });
            }

            if (filters) {
                for (i = 0; i < filters.length; i++) {
                    var filter = filters[i];
                    var field = Object.keys(filter.term)[0];
                    if ( filter.term && filter.term[field].length > 0 ) {
	                    data.query.bool['must'].push(filter); 
                    } else {
                        // A filter without a value implies searching for assets that have anything in that field
                        var existQuery = {
                            exists : { "field" : field } 
                        };
                        data.query.bool['must'].push(existQuery);
                    }
                }
            }
            if (filtersExclude) {
                for (var j = 0; j < filtersExclude.length; j++) {
                    var xfilter = filtersExclude[j];
                    var xfield = Object.keys(xfilter.term)[0];
                    if ( xfilter.term && xfilter.term[xfield].length > 0) {
                       data.query.bool['must_not'].push(xfilter); 
                    } else {
                        // Exclude filter without a value implies searching for assets that don't have values for a field
                        var xExistQuery = {
                            exists : { "field" : xfield } 
                        };
                        data.query.bool['must_not'].push(xExistQuery);
                    }
                }
            }

            if (rangeQuery["DateRangeBegin"]) {
                if(data.query.bool && data.query.bool['must']){
                  // Check if query is already filtered
                    var range = {};
                    range.range = rangeQuery;
                    data.query.bool['must'].push(range); 
                }
            }

            if( titleSort ){
                data.sort = [
                    { "Title" : {
                        "order" : titleSort,
                        "missing" : "_last"
                        }
                    },
                    "_score"
                ];
            }
            if( dateSort ){
                var dateSortValArray = [];
                if (dateSort === 'asc') {
                    dateSortValArray.push( { "DateRangeBegin" : dateSort } );
                } else {
                    dateSortValArray.push( { "DateRangeBegin" : dateSort } );
                    // Make sure bad data showing as beyond the current year is filtered out
                    var endRange = {}; 
                    endRange.range = {
                        "DateRangeBegin" : {
                            "lte" : new Date().getFullYear() + 1
                        }    
                    };
                    data.query.bool['must'].push(endRange); 
                }
                
                data.sort = dateSortValArray;
            }
            if( newestSort ){
                var newestSortValArray = [];
                newestSortValArray.push( { "Meta-PublishedDate" : newestSort } );
                data.sort = newestSortValArray;
            }

            // if(newRelease && !titleSort && !dateSort && !newestSort){
            //     var newReleaseDateSortValArray = [];
            //     newReleaseDateSortValArray.push( { "Meta-PublishedDate" : "desc" } );
            //     data.sort = newReleaseDateSortValArray;
            // }

            var getLink = searchUrl;
            
            // Thanks to Ben Nadel for this method of aborting a call
            // http://www.bennadel.com/blog/2616-aborting-ajax-requests-using-http-and-angularjs.htm
    
            // The timeout property of the http request takes a deferred value
            // that will abort the underying AJAX request if / when the deferred
            // value is resolved.
            var deferredAbort = $q.defer();
            
            // Set variable so we can return whether user aborted or call failed
            var isAborted = false;

            var request = $http.get(getLink, {
                params : { 'source' : data },
                timeout: deferredAbort.promise
            });
            
            // Rather than returning the http-promise object, we want to pipe it
            // through another promise so that we can "unwrap" the response
            // without letting the http-transport mechansim leak out of the
            // service layer.
            var promise = request.then(
                function( res ) {
                    return( res );
                },
                function( res ) {
                    if (isAborted) {
                        return( $q.reject( { "statusText": "Aborted" }) );
                    } else {
                        return( $q.reject( { "statusText": "Failed" }) );
                    }
                }
            );
            // Now that we have the promise that we're going to return to the
            // calling context, let's augment it with the abort method. Since
            // the $http service uses a deferred value for the timeout, then
            // all we have to do here is resolve the value and AngularJS will
            // abort the underlying AJAX request.
            promise.abort = function() {
                isAborted = true;
                deferredAbort.resolve();
            };
            // Since we're creating functions and passing them out of scope,
            // we're creating object references that may be hard to garbage
            // collect. As such, we can perform some clean-up once we know
            // that the requests has finished.
            promise.finally(
                function() {
                    promise.abort = angular.noop;
                    deferredAbort = request = promise = null;
                }
            );
            return( promise );

        },

        getInstitutionCollections: function(instId) {
            var data = {
                "query" : {
                    "bool" : {"must" : [ ] }
                },
                "aggs" : {
                    "Collections" : {
                        "terms" : {
                            "field" : schema.collections,
                            "size" : 0
                        }  
                    }
                },
                "timeout" : "4s",
                "_source": {
                    "include": []
                }
            };
            var instFilter = { "term" : {} };
            instFilter.term[schema.institutionId] = instId;
            data.query.bool.must.push(instFilter);

            return $http.get(searchUrl, {
                params : { 'source' : data }
            });
        },
        
        getSearchSuggestions: function(term) { 
            var postLink = baseUrl + "/es2/alcove/_suggest";
            
            suggestFields = [
                schema.creator,
                schema.art.location,
                schema.art.browseCountry,
                schema.art.workType,
                schema.art.browseClass,
                schema.art.material,
                schema.art.culture,
                schema.title
            ];
            
            query = {
                "text" : term
            };
            
            for (var i = 0; i < suggestFields.length; i++) {
                query[suggestFields[i]] = {
                    "completion" : {
                        "field" : "Meta-Suggest." + suggestFields[i]
                    }
                };
            }
            
            return $http({
                method: 'POST',
                url: postLink,
                data: query 
            });
        },

        /**
         * @function
         * @name getInstituteNameById
         * @description Get institute name from institute id.
         * @param {string} id - Institute id.
         * @returns {Object} Reponse object containing the institute name.
         * @memberof Services
         */
        getInstituteNameById: function(id) {
            var getLink = baseUrl + "/api/v1/institutions/" + id;
            return $http.get(getLink);
        },

        getAllFilters: function(field) {
            var getLink = searchUrl;
            var data = {
                "aggs" : { }
            };
            data.aggs[field] =  {
                "terms" : {
                    "field" : field,
                    "size" : 0
                }  
            };
            return $http.get(getLink, {
                params : { 'source' : data }
            });
        },

        /**
         * @function
         * @name getAllInstitutions
         * @description Get all the Institutions information.
         * @returns {Object} Reponse object containing all the Institutions information.
         * @memberof Services
         */
        getAllInstitutions: function() {
            var getLink = baseUrl + "/api/v1/institutions?all=true";
            return $http.get(getLink);
        },

        /**
         * @function
         * @name getBackupInstitutions
         * @description Get all the Institutions information from the backup resource (.json) file incase there are any issues fetching the info over the network.
         * @returns {Object} Reponse object containing all the Institutions information.
         * @memberof Services
         */
        getBackupInstitutions: function() {
            return $http.get('assets/backup/institutions.json');
        },

        /**
         * @function
         * @name getCollection
         * @description Get a specific collection or image group metadata
         * @returns {Object} Reponse object containing all group metadata
         * @memberof Services
         */
        getCollectionById: function(ids, fieldsToPull) {
          if (typeof(ids) === 'string' || typeof(ids) === 'number'){
            ids = [ids];
          }
          var data = {};
          data["query"] = {
            "bool" : {
              "must": {
                "terms" : {
                  "id" : ids
                }
              }
            }
          };
          
          if (fieldsToPull) {
            data["_source"] = {
                "include": fieldsToPull
            };
          }

          var getLink = baseUrl + "/es2/groups/_search?size=200";

          return $http.get(getLink, {
              'params' : {  'source' : data }
          });
        },

        /**
         * @function
         * @name getAllCollections
         * @description Get all collections and groups
         * @returns {Object} Object containing all collections and groups
         * @memberof Services
         */
        getAllCollections: function(id) {
          var getLink = baseUrl + "/es2/groups/_search?size=200";
          return $http.get(getLink);
        },

        /**
         * @function
         * @name getRecentCollections
         * @description Get recent collections
         * @returns {Object} Response object containing recently published collections
         * @memberof Services
         */
        getRecentCollections: function() {
          var getLink = baseUrl + "/es2/groups/_search?size=5&sort=published:desc";
          return $http.get(getLink);
        }
    };
})

.factory('external', function($http, $log) {
    /** @type {string}
      * @default
      * @description API key for querying events from eventful.com
      * @memberof Services
      */
    var eventfulKey = "zGxBqjXxb75cBDNW";

    function formEncode(obj) {
      var encodedString = '';
      for (var key in obj) {
        if (encodedString.length !== 0) {
          encodedString += '&';
        }

        encodedString += key + '=' + encodeURIComponent(obj[key]);
      }
      return encodedString.replace(/%20/g, '+');
    }

    return {
        /**
         * @function
         * @name getEvents
         * @description Get event results from eventful api based on user's geolocation and entered searchword.
         * @param {string} latitude - User's geolocation latitude value.
         * @param {string} longitude - User's geolocation longitude value.
         * @param {string} expandedSearch - Expand the event search radius from 60 Miles to 150 Miles (with respect to user's current location).
         * @param {string} term - User entered search term.
         * @returns {Object} Reponse object containing the event results.
         * @memberof Services
         */
        getEvents: function(latitude, longitude, expandedSearch, term) {
            var location = latitude + "," + longitude;
            var getLink;
            var date;
            var miles;

            if (expandedSearch) {
                date = "";
                miles = "150";
            } else {
                date = "&date=This+Week";
                miles = "60";
            }

            if ( term ) {
                getLink = "http://api.eventful.com/json/events/search?app_key=" + eventfulKey + "&keywords=" + term + "&location=" + location + "&category=art&within=" + miles + "&sort_order=date" + date + "&callback=JSON_CALLBACK";

            } else {
                getLink = "http://api.eventful.com/json/events/search?app_key=" + eventfulKey + "&location=" + location + "&category=art&within=" + miles + "&sort_order=date" + date + "&callback=JSON_CALLBACK";
            }

            return $http.jsonp(getLink);
        },

        /**
         * @function
         * @name getUserLoc
         * @description Get user's current location info.
         * @returns {Object} Reponse object containing user's current location info.
         * @memberof Services
         */
        getUserLoc: function() {
            var getLink = 'http://ip-api.com/json';
            return $http({
                method: 'GET',
                url: getLink
            });
        },

        /**
         * @function
         * @name getNYArtBeat
         * @description Get event results from NYArtBeat api based on user's geolocation.
         * @param {string} latitude - User's geolocation latitude value.
         * @param {string} longitude - User's geolocation longitude value.
         * @returns {Object} Reponse object containing the event results.
         * @memberof Services
         */
        getNYArtBeat: function(latitude, longitude) {
            // latitude = "40.719130";
            // longitude = "-73.980000";
            var getLink = "http://www.nyartbeat.com/list/event_searchNear?latitude=" + latitude + "&longitude=" + longitude;
            return $http
                .get(getLink)
                .success(function(response) {
                    
                });
        },

        /**
         * @function
         * @name getBlogEntries
         * @description Search Artstor blog data from Wordpress.com API
         * @param {string} query - User entered search term.
         * @returns {Object} Response object containing Artstor blog data.
         * @memberof Services
         */
        getBlogEntries: function(query) {
            if (!query || query == "*") {
                // An asterisk query on the Wordpress API *LIMITS* results to those with an asterisk!
                query = "";
            } else {
                // Force exact phrase match
                query = '"'+ query +'"';
            }

            var getLink = "https://public-api.wordpress.com/rest/v1.1/sites/artstor.wordpress.com/posts/?number=24&search=" + query;
            return $http.get(getLink);
        },

        /**
         * @function
         * @name getBlogEntriesByCat
         * @description Get Artstor blog data from Wordpress.com API for a specific category
         * @param {string} category - User entered specific category (must exist on blog)
         * @param {number} count - Number of posts to return
         * @returns {Object} Response object containing Artstor blog data.
         * @memberof Services
         */
        getBlogEntriesByCat: function(category, count) {
          if (!category) {
              category = "";
          }
          var getLink = "https://public-api.wordpress.com/rest/v1.1/sites/artstor.wordpress.com/posts/?number=" + count + "&category=" + category;
          return $http
              .get(getLink)
              .success(function(response) {
                  
              });
        },

        /**
         * @function
         * @name searchBrooklynData
         * @description Get assets data from Brooklyn Museum Api.
         * @param {string} query - User entered search term.
         * @param {string} index - Start index for the searched asset results (starting from 0).
         * @returns {Object} Reponse object containing the Brooklyn asset results.
         * @memberof Services
         */
        searchBrooklynData: function(query, index){
            // query = 'french';
            var startIndex = '';

            if(index){
                startIndex = index;
            }
            else{
                startIndex = '0';
            }

            if(query === '*'){
                query = 'painting';
            }


            var getLink = 'https://www.brooklynmuseum.org/api/v2/object/?has_images=1&sort_by=relevance&keyword=' + query + '&limit=9&offset=' + startIndex;
            var config = {
                            headers:  {
                              'api_key': '3Pc2o1iAdg'
                            }
                          };

            return $http
                  .get(getLink, config)
                  .success(function(response) {
                      
                  });
        },

        /**
         * @function
         * @name searchRijksmuseumData
         * @description Get assets data from Rijksmuseum Api.
         * @param {string} query - User entered search term.
         * @param {string} index - Start index for the searched asset results (starting from 0).
         * @returns {Object} Reponse object containing the Rijksmuseum asset results.
         * @memberof Services
         */
        searchRijksmuseumData: function(query, index){
            var page;
            
            if(index){ 
              page = (index / 9);
            }
            else{
              page = 0;
            }

            var getLink = 'https://www.rijksmuseum.nl/api/en/collection?key=UNrPtT97&format=json&p=' + page + '&ps=9&q=' + query + '&imgonly=True&s=relevance';
            return $http
                  .get(getLink)
                  .success(function(response) {
                      
                  });
        },

        /**
         * @function
         * @name searchLacmaData
         * @description Get assets data from LACMA Elastic Search Index.
         * @param {string} query - User entered search term.
         * @param {string} index - Start index for the searched asset results (starting from 0).
         * @returns {Object} Reponse object containing the LACMA asset results.
         * @memberof Services
         */
        searchLacmaData: function(query, index){
            // query = 'Ynez%20Johnston';
            var startIndex = '';

            if(index){
                startIndex = index;
            }
            else{
                startIndex = '0';
            }

            var getLink = "http://ec2-54-88-159-164.compute-1.amazonaws.com/es/lacmacollection/_search?q=" + query + "&sort=PrimaryImageURL:asc&size=9&from=" + startIndex;
            return $http
                .get(getLink)
                .success(function(response) {
                    
                });
        }
    };
})

.factory('records', function($http, $log, account, $localStorage) {
    
    return {
        /**
         * @function
         * @name getWorkRecord
         * @description Get WORK record detailed info.
         * @param {string} id - Id of the WORK record whose detailed info. is to be fetched.
         * @param {string} callback - Method to which the response object shall be passed as a parameter.
         * @returns {Object} Reponse object containing WORK record detailed info.
         * @memberof Services
         */
        getWorkRecord: function(id, callback) {
            var params = {};

            if (!id) {
                console.log("No work record ID found");
                return;
            }

            params.authToken = account.getAuthToken();
            params.nameAuthToken = account.getNamesAuthToken();
            params.instituteName = account.getInstituteName();
            params.workIndexId = id;

            var getLink = baseUrl + '/service/api/work/json/getWorkByWorkIndexId';

            $http.jsonp(getLink, {
                    params: params
                })
                .success(function(response) {
                    callback(response);
                });
        },

        /**
         * @function
         * @name getNameRecord
         * @description Get NAME record detailed info.
         * @param {string} id - Id of the NAME record whose detailed info. is to be fetched.
         * @param {string} callback - Method to which the response object shall be passed as a parameter.
         * @returns {Object} Reponse object containing NAME record detailed info.
         * @memberof Services
         */
        getNameRecord: function(id, callback){
            var params = {};

            if (!id) {
                console.log("No record ID found");
                return;
            }

            params.authToken = '123';
            params.conceptId = id;
            params.callback = 'JSON_CALLBACK';

            // Temporarily accessing production directly
            var getLink = 'http://catalog.sharedshelf.artstor.org' + '/names/name/json/conceptDetail';

            $http
                .jsonp(getLink, {
                    params: params
                })
                .success(function(response) {
                    callback(response);
                });

        },

        /**
         * @function
         * @name getAatRecord
         * @description Get AAT record detailed info.
         * @param {string} id - Id of the AAT record whose detailed info. is to be fetched.
         * @param {string} callback - Method to which the response object shall be passed as a parameter.
         * @returns {Object} Reponse object containing AAT record detailed info.
         * @memberof Services
         */
        getAatRecord: function(id, callback){
            if (!id) {
                console.log("No record ID found");
                return;
            }

            var params = {};

            params.authToken = '123';
            params.subjectid = id;
            params.callback = 'JSON_CALLBACK';

            // Temporarily accessing production directly
            var getLink = 'http://catalog.sharedshelf.artstor.org' + '/aat/aat/json/subjectDetail';

            $http
                .jsonp(getLink, {
                    params: params
                })
                .success(function(response) {
                    callback(response);
                });
        },

        /**
         * @function
         * @name getTgnRecord
         * @description Get TGN record detailed info.
         * @param {string} id - Id of the TGN record whose detailed info. is to be fetched.
         * @param {string} callback - Method to which the response object shall be passed as a parameter.
         * @returns {Object} Reponse object containing TGN record detailed info.
         * @memberof Services
         */
        getTgnRecord: function(id, callback) {
            if (!id) {
                console.log("No record ID found");
                return;
            }

            var params = {};

            params.authToken = '123';
            params.subjectid = id;
            params.callback = 'JSON_CALLBACK';

            // Temporarily accessing production directly
            var getLink = 'http://catalog.sharedshelf.artstor.org' + '/tgn/tgn/json/subjectDetail';

            $http
                .jsonp(getLink, {
                    params: params
                })
                .success(function(response) {
                    callback(response);
                });

        },


        /**
         * @function
         * @name lookupAllVocabs
         * @description Search for exact matches against the search keyword in SS Vocabs
         * @param {string} term - keyword term to be searched.
         * @param {string} callback - Method to which the response object shall be passed as a parameter.
         * @returns {Object} Reponse object containing exactly matched vocabs.
         * @memberof Services
         */
        lookupAllVocabs: function(term, callback) {
            if (!term || term === '*') {
                // No search term found
                return;
            }

            var params = {};

            params.searchTerm = '"' + term + '"';

            params.authToken = '123';
            params.nameAuth = '123';

            params.instituteId = '10000'; // 10003
            params.instituteName = 'Artstor';

            params.lookupType = 'SUBJECT';
            params.mfclFlag = false;
            params.pageNumber = '1';
            params.pageSize = '25';
            // Sort applies alphabetization
            params.sort = false;

            params.tgnFlag = true;
            params.aatFlag = true;
            params.nameFlag = true;
            params.subjectFlag = false;
            params.workFlag = false;

            params.callback = 'JSON_CALLBACK';

            // Temporarily accessing production directly
            var getLink = 'http://catalog.sharedshelf.artstor.org' + '/service/api/work/json/lookupAllVocabs';

            $http
                .jsonp(getLink, {
                    params: params
                })
                .success(function(response) {
                    callback(response);
                });

        }
    };
})
;
