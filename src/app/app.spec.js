/*
 * Basic test cases for app-wide code
 */
describe('AlcoveCtrl', function() {
  var scope, $location, createController;

  beforeEach(module('alcove')); //<--- Hook module

  beforeEach(inject(function ($rootScope, $controller, _$location_) {
      $location = _$location_;
      scope = $rootScope.$new();

      createController = function() {
          return $controller('AlcoveCtrl', {
              '$scope': scope
          });
      };
  }));

  it('should have a method to check if the path is active', function() {
      var controller = createController();
      $location.path('/');
      expect($location.path()).toBe('/');
      expect(scope.isActive('/')).toBe(true);
      expect(scope.isActive('/badpath')).toBe(false);
  });
});