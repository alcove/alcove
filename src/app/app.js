angular.module('jm.i18next').config( [ '$i18nextProvider', function ($i18nextProvider) {

    //'use strict';
    $i18nextProvider.options = {
        lng: 'en',
        useCookie: true,
        useLocalStorage: false,
        preload: ['en', 'fr', 'zh'],
        resGetPath: 'assets/locales/__lng__/__ns__.json',
        defaultLoadingValue: ''
    };

}]);

var alcove = angular.module( 'alcove', [
    '720kb.socialshare',
    'templates-app',
    'templates-common',
    'ngRoute',
    'ngSanitize',
    'jm.i18next',
    'ui-rangeSlider',

    'ngProgress',
    'ngStorage',
    'ngCookies',
    'lrInfiniteScroll',

    'alcove.services',
    'jstor.services',
    'alcove.handlers',
    'alcove.simpleDirectives',
    'titleService',

    'alcove.navBar',
    'alcove.autocomplete',
    'alcove.navFooter',
    'alcove.masonry',
    'alcove.assetThumbnail',
    // Home Components
    'alcove.home',
        'alcove.homeScroll',
        'alcove.subjectGuides',
        'alcove.blogPosts',
        'alcove.contextPanel',
        'alcove.catalogColumn',
        'alcove.filterCrumbs',
    'alcove.item',
    'alcove.assetViewer',
    'alcove.pdfViewer',
    'alcove.shareModal',
    // Mobile Components
    'alcove.sliderThumb',

    // Drag and Drop components
    'lvl.services',
    'lvl.directives.dragdrop',

    // Keyboard Shortcuts
    'cfp.hotkeys'
])

.config(['$httpProvider', '$routeProvider', '$compileProvider', function ($httpProvider, $routeProvider, $compileProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.useApplyAsync(true);
        
        $routeProvider.otherwise({
            redirectTo: '/'
        });
        
        $compileProvider.debugInfoEnabled(false);
    }
])

.config(function(hotkeysProvider) {
    hotkeysProvider.templateHeader = '<h2 class="hotkeys__header">Keyboard Shortcuts</h2>';
  })

.run(function run(titleService) {
    //titleService.setPrefix('Alcove | ');
    titleService.setSuffix(' - Alcove');
})

.filter('titlecase', function() {
    return function(s) {
        s = ( s === undefined || s === null ) ? '' : s;
        return s.toString().toLowerCase().replace( /\b([a-z])/g, function(ch) {
            return ch.toUpperCase();
        });
    };
})

.filter('formatEpoch', [ '$filter', function($filter) {
    return function(input) {
        if (input) {
            input = $filter('date')(input, 'y GG') + "";
            return input.replace("-", "" ).replace("BC", "BCE" ).replace("AD", "CE" );
        } else {
            return "";
        }
    };
}])

.filter('words', function () {
    return function (input, words) {
        if (isNaN(words)) {
            return input;
        }
        if (words <= 0) {
            return '';
        }
        if (input) {
            var inputWords = input.split(/\s+/);
            if (inputWords.length > words) {
                input = inputWords.slice(0, words).join(' ') + '...';
            }
        }
        return input;
    };
})

.filter('limitEls', function () {
    return function (input, limit) {
        if (isNaN(limit)) {
            return input;
        }
        if (limit <= 0) {
            return '';
        }
        if (input) {
            var contentString = input.toString(); //
            var content = angular.element("<div>" + contentString + "</div>");
            var inputHTML = content.children();
            if ( inputHTML.length < limit) {
              limit = inputHTML.length;
            }
            input = "";
            for (var i = 0; i < limit; i++) {
              input = input + inputHTML[i].outerHTML;
            }
        }
        return input;
    };
})

.filter('elipsis', function () {
    return function (input, letters) {
        if (isNaN(letters)) {
            return input;
        }
        if (letters <= 0) {
            return '';
        }
        var result = '';
        if (input) {
            if(input.length > letters){
                result = input.substring(0, letters) + '...';
            }
            else{
                result = input;
            }

        }
        return result;

    };
})

.filter('formatIds', function ($localStorage) {
    return function (filter, filterGroup, returnEmpty) {
        var instArray = [];
        if (filterGroup === 'Meta-InstitutionId'){
          if ( $localStorage['institutionFilter'] && $localStorage['institutionFilter'][filter] ) {
           return ($localStorage['institutionFilter'][filter].name ? $localStorage['institutionFilter'][filter].name : filter);
          }
       } 
       else if (filterGroup === 'Collections') {
         if ( $localStorage['collectionFilter'] && $localStorage['collectionFilter'][filter] ) {
          return ($localStorage['collectionFilter'][filter].name ? $localStorage['collectionFilter'][filter].name : filter);
         }
       } 
       else if (filterGroup === 'Media') {
          // Object label for translations/i18next
          return "media-type." + filter;
        }
        if (returnEmpty) {
            return "";
        }
        return filter;
    };
})

.filter('highlight', ['$sce', function ($sce) {
  return function (aString, searchParam) {
    if (typeof(aString) != 'string') {
      return '';
    }
    if (searchParam) {
      var words = '(' +
            searchParam.split(/\ /).join(' |') + '|' +
            searchParam.split(/\ /).join('|') +
          ')',
          exp = new RegExp(words, 'gi');
      if (words.length) {
        aString = aString.replace(exp, "<span class=\"highlight\">$1</span>");
      }
    }
    return $sce.trustAsHtml(aString);
  };
}])

.filter('fetchFacets', ['schema', function(schema) {
  return function(items, GIC) {
    var sortedItems = {};
    var otherItems = JSON.parse(JSON.stringify(items));
    if (GIC) {
        sortedItems[schema.genre] = items[schema.genre];
        sortedItems[schema.institutionId] = items[schema.institutionId];
        sortedItems[schema.collections] = items[schema.collections];
        sortedItems[schema.media] = items[schema.media];
    }
    else {
        for (var i = 0; i < schema.facets.length; i++) {
            if (items[schema.facets[i]]) {
                sortedItems[schema.facets[i]] = items[schema.facets[i]];
            }
        }
        if (sortedItems[schema.genre]) {
            delete sortedItems[schema.genre];
        }
        if (sortedItems[schema.institutionId]) {
            delete sortedItems[schema.institutionId];
        }
        if (sortedItems[schema.collections]) {
            delete sortedItems[schema.collections];
        }
        if (sortedItems[schema.media]) {
            delete sortedItems[schema.media];
        }
    }
    return sortedItems;
  };
}])

.filter('startsWithFilter', function() {
    return function(string, startText) {
        if (string && string.term && string.term.toLowerCase().indexOf(startText.toLowerCase()) === 0) {
            return string;
        }
    };
})

.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    };
})

.controller( 'AlcoveCtrl', function AlcoveCtrl ( $scope, $rootScope, $location, account, $timeout, $localStorage, ngProgressFactory, external, $i18next) {
    
    $scope.languageCode = $i18next.options.lng.toUpperCase();
    
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        if ( angular.isDefined( toState.data.pageTitle ) ) {
          $scope.pageTitle = toState.data.pageTitle + 'Alcove | ' ;
        }
    });

    $rootScope.progressbar = ngProgressFactory.createInstance();

    // $scope.getAccountInfo = function() {
    //     if (!$localStorage.loggedIn) {
    //         console.log("Checking for account info");
    //         account.fetchAccountInfo();
    //     }
    // };
    //
    // $scope.getAccountInfo();

    $scope.isActive = function(route) {
        return route === $location.path();
    };

    $scope.usercurrentlocation = '';

    $scope.getLocation = function() {
        external.getUserLoc()
            .success(function(data) {
                if(data && data.lat){
                    $localStorage['userLatitude'] = data.lat;
                    $localStorage['userLongitude'] = data.lon;

                    $scope.usercurrentlocation = data.city;
                    $localStorage['userLocation'] = $scope.usercurrentlocation;
                    
                    // $scope.$apply();

                    $rootScope.$broadcast('userLocated');
                }
            })
            .error(function(data, status) {
            });
    };

    $scope.getLocation();

    // Dev Tools
    // Baseline toggle

    $scope.toggleBaseline = function() {
        if (!$scope.baselineShowing) {
            $scope.baselineShowing = true;
            $('.baseline').addClass('show-grid');
        } else {
            $scope.baselineShowing = false;
            $('.baseline').removeClass('show-grid');
        }
    };


   // window.setTimeout(function() {
   //      //$(".thumbnail").each(function)
   //      $(".thumbnail-sheet-4").each(function() {
   //          var rowHeight = 0;

   //          $(this).find('.thumbnail').each(function(index, obj) {
   //              if ( $(obj).innerHeight() > rowHeight) {
   //                  rowHeight
   //              }
   //              console.log( $(obj).innerHeight() + " " + index );

   //          });

   //      });
   // }, 4000);
})

// Unique filter taken from AngularUI
.filter('unique', function () {

  return function (items, filterOn) {

    if (filterOn === false) {
      return items;
    }

    if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
      var hashCheck = {}, newItems = [];

      var extractValueToCompare = function (item) {
        if (angular.isObject(item) && angular.isString(filterOn)) {
          return item[filterOn];
        } else {
          return item;
        }
      };

      angular.forEach(items, function (item) {
        var valueToCheck, isDuplicate = false;

        for (var i = 0; i < newItems.length; i++) {
          if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
            isDuplicate = true;
            break;
          }
        }
        if (!isDuplicate) {
          newItems.push(item);
        }

      });
      items = newItems;
    }
    return items;
  };
})

;


// Convenience JS Functions

// Here is a VERY basic generic trigger method
function triggerEvent(el, type)
{
    if ((el[type] || false) && typeof el[type] == 'function')
    {
        el[type](el);
    }
}

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) {
                func.apply(context, args);
            }
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) {
            func.apply(context, args);
        }
	};
}