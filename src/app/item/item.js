/**
 * @fileOverview Route and Controller for Assets view page.
 * @author Cody & Rehan
 * @namespace View
 * @version 1.0.1
 */
angular.module( 'alcove.item', [
  'titleService',
  'ngStorage'
])

.config(function config( $routeProvider ) {
  $routeProvider
    .when( '/item/:assetId', {
      controller: 'ItemCtrl',
      templateUrl: 'item/item.tpl.html'
    });
})

.controller( 'ItemCtrl', function ItemCtrl(
      $scope,
      $localStorage,
      titleService,
      $routeParams,
      account,
      assets,
      records,
      $timeout,
      $http,
      $log,
      $rootScope,
      $sce,
      schema,
      findAssetField,
      $location,
      searchQuery,
      hotkeys
    ) {

      // Services to scope
      $scope.findAssetField = findAssetField;

      $scope.currentGenre = "ART";
      titleService.setTitle( 'View Item' );

      $scope.assetId = $routeParams.assetId;
      $scope.compareAssets = [];
      $scope.compareAssetsIds = [];
      $scope.relatedAssets = [];

      var sourceObjectLabel = "SOURCE-RECORD";

      var tileSource = "";

      // Keyboard Shortcuts
      hotkeys
          .bindTo($scope)
          .add({ 
              combo: 'left',
              description: 'Open / View the previous asset',
              callback: function(event) {
                  event.preventDefault();
                  $scope.$$childHead.prevAsset();
              }
          })
          .add({ 
              combo: 'right',
              description: 'Open / View the next asset',
              callback: function(event) {
                  event.preventDefault();
                  $scope.$$childHead.nextAsset();
              }
          })
          .add({ 
              combo: 'mod+s',
              description: 'Save asset',
              callback: function(event) {
                event.preventDefault();
                $timeout(function() { 
                    document.querySelector('#downloadLink').click();
                }, 0);
              }
          })
          .add({ 
              combo: 'mod+h',
              description: 'Share asset',
              callback: function(event) {
                event.preventDefault();
                $scope.showShareOptions();
              }
          })
          .add({ 
              combo: 'p',
              description: 'Toggle presentation mode',
              callback: function(event) {
                  event.preventDefault();
                  $scope.togglePresentationMode();
              }
          })
          .add({ 
              combo: 'esc',
              description: 'Exit full screen mode',
              callback: function(event) {

              }
          })
          .add({ 
              combo: 'c',
              description: 'Toggle comparison mode',
              callback: function(event) {
                event.preventDefault();
                if($scope.fullscreen){
                  $scope.toggleSelectionPane();
                }
                if ($scope.selectingItems !== 0) { // In comparison mode
                  $timeout(function() { 
                    document.querySelector('#selColumnContent thumbnail:nth-of-type(1)').parentNode.focus();
                  }, 250);
                }
              }
          })
          .add({ 
              combo: 'down',
              description: 'Browse (down) between the assets in comparison mode',
              callback: function(event) {
                event.preventDefault();
                if ($scope.selectingItems !== 0) { // In comparison mode
                  var activeElement = document.activeElement;
                  var nextActiveElement = activeElement ? activeElement.nextElementSibling : null;
                  if(activeElement && nextActiveElement){
                      $timeout(function() { 
                          nextActiveElement.focus();
                      }, 250);
                  }
                }
              }
          })
          .add({ 
              combo: 'up',
              description: 'Browse (up) between the assets in comparison mode',
              callback: function(event) {
                event.preventDefault();
                if ($scope.selectingItems !== 0) { // In comparison mode
                  var activeElement = document.activeElement;
                  var prevActiveElement = activeElement ? activeElement.previousElementSibling : null;
                  if(activeElement && prevActiveElement){
                      $timeout(function() { 
                          prevActiveElement.focus();
                      }, 250);
                  }
                }
              }
          })
          .add({ 
              combo: 'enter',
              description: 'Select asset in comparison mode',
              callback: function(event) {
                event.preventDefault();
                if ($scope.selectingItems !== 0) { // In comparison mode
                  var activeElement = document.activeElement;
                  if(activeElement && activeElement.children && activeElement.children[0] && (activeElement.children[0].tagName === 'THUMBNAIL')){
                    $timeout(function() { 
                        activeElement.children[0].click();
                    }, 0);
                  }
                }
              }
          });

      assets.openById($scope.assetId).success(function(response) {

    // Sending View Item Data to google Analytics
  /*  ga('send', {
      hitType: 'event',
      eventCategory: 'asset_view_by_institution',
      eventAction: 'Harvard',
      eventLabel: $scope.assetId
    });

    ga('send', {
      hitType: 'event',
      eventCategory: 'asset_view_by_collection',
      eventAction: 'SSC',
      eventLabel: $scope.assetId
    }); */

    ga('send', {
      hitType: 'event',
      eventCategory: 'Access',
      eventAction: 'View item',
      eventLabel: $scope.assetId
    });

    var search_results_ids = [];
    if ($localStorage.search_results && $localStorage.search_results.ids_array) {
      search_results_ids = $localStorage.search_results.ids_array;
    }
    var asset_index  = search_results_ids.indexOf($scope.assetId);

    if(asset_index > -1){
      $scope.assetindex = asset_index + 1;
      $scope.totalassets = $localStorage.search_results.totalResults;
      var assetProgress = ($scope.assetindex / $scope.totalassets * 100);
      $rootScope.progressbar.set(assetProgress);
    }
    else{
      $scope.assetindex = '';
      $scope.totalassets = '';
      $rootScope.progressbar.reset();
    }

    // assets.openById( $localStorage.search_results.ids_array[$scope.assetindex + 1] ).success(function(response){
    //   console.log(response);
    //   var asset = response.hits.hits[0]._source;
    //   $scope.compareAssets.push(asset);
    // });

    if (response.hits && response.hits.hits[0]) {
      $scope.asset = response.hits.hits[0]._source;
      $scope.asset["source"] = response.hits.hits[0]._source[sourceObjectLabel];
    } else {
      $scope.errorMsg = "Object could not be located";
      return;
    }
    $scope.compareAssets.push( $scope.asset );
    $scope.compareAssetsIds.push( $scope.asset['Meta-Id'] );

    if ($scope.asset.File.format == "pdf") {
      //$scope.pdfUrl = $scope.asset.File.url;
      $scope.mediaLoadingFailed = true;
    }

    if($scope.asset["Meta-WorkId"]){
      $scope.getRelatedItems($scope.asset["Meta-WorkId"]);
    }

    if($scope.asset["Meta-InstitutionId"]){
      assets.getInstituteNameById($scope.asset["Meta-InstitutionId"]).success(function(response) {
        $scope.asset["instituteName"] = response.name;
      });
    }

    if ($scope.asset["Collections"]) {
      $scope.collectionsArray = [];
      $scope.collectionsRights = [];
      assets.getCollectionById($scope.asset["Collections"])
          .success(function(response) {
            // console.log(response);
            // Attach names to results
            angular.forEach(response.hits.hits, function(col) {
              $scope.collectionsArray.push({ "id" : col._id, "name" : col._source.name, "rights" : col._source.rights });
              $scope.collectionsRights.push(col._source.rights);
            });
          })
          .error(function(response) {
            console.log("Unable to access Collections index");
          });
    }

    if ( $scope.asset["Title"] ) {
      titleService.setTitle( $scope.asset["Title"] );
    }


    // Update Meta tags for exporting bibliographic citation data to Zotero & for OGP.
    document.querySelector('meta[property="og:url"]').setAttribute('content', $location.absUrl() );
    document.querySelector('meta[property="og:image"]').setAttribute('content', findAssetField.thumbUrl($scope.asset,1));

    if($scope.asset[schema.file] && $scope.asset[schema.file].type && ($scope.asset[schema.file].type == 'image')){
      document.querySelector('meta[name="DC.format"]').setAttribute('content', 'image/' + $scope.asset[schema.file].data.storData.format);
      document.querySelector('meta[name="DC.type"]').setAttribute('content', 'Still Image');
      document.querySelector('meta[property="og:type"]').setAttribute('content', 'artstor:image');
    }
    else if($scope.asset[schema.file] && $scope.asset[schema.file].type && ($scope.asset[schema.file].type == 'video')){
      document.querySelector('meta[name="DC.format"]').setAttribute('content', 'video/' + $scope.asset[schema.file].data.storData.format);
      document.querySelector('meta[name="DC.type"]').setAttribute('content', 'Moving Image');
      document.querySelector('meta[property="og:type"]').setAttribute('content', 'artstor:video');
    }
    else if($scope.asset[schema.file] && $scope.asset[schema.file].type && ($scope.asset[schema.file].type == 'audio')){
      document.querySelector('meta[name="DC.format"]').setAttribute('content', 'audio/' + $scope.asset[schema.file].data.storData.format);
      document.querySelector('meta[name="DC.type"]').setAttribute('content', 'Sound');
      document.querySelector('meta[property="og:type"]').setAttribute('content', 'artstor:audio');
    }
    else if($scope.asset[schema.file] && $scope.asset[schema.file].type && ($scope.asset[schema.file].type == 'qtvr')){
      document.querySelector('meta[name="DC.format"]').setAttribute('content', 'video/' + $scope.asset[schema.file].data.storData.format);
      document.querySelector('meta[name="DC.type"]').setAttribute('content', 'Interactive Resource');
      document.querySelector('meta[property="og:type"]').setAttribute('content', 'artstor:3D');
    }
    else if($scope.asset[schema.file] && $scope.asset[schema.file].type && (($scope.asset[schema.file].type == 'pdf') || ($scope.asset[schema.file].type == 'file'))){
      document.querySelector('meta[name="DC.format"]').setAttribute('content', 'text/' + $scope.asset[schema.file].data.storData.format);
      document.querySelector('meta[name="DC.type"]').setAttribute('content', 'Text');
      document.querySelector('meta[property="og:type"]').setAttribute('content', 'artstor:document');
    }

    if(findAssetField.title($scope.asset)){
      document.querySelector('meta[name="DC.title"]').setAttribute('content', findAssetField.title($scope.asset));
      document.querySelector('meta[property="og:title"]').setAttribute('content', findAssetField.title($scope.asset));
    }

    if($scope.asset[schema.record]['Description']){
      document.querySelector('meta[property="og:description"]').setAttribute('content', findAssetField.description($scope.asset));
    }

    if($scope.asset[schema.record] && $scope.asset[schema.record].Creator && $scope.asset[schema.record].Creator.display_value){
      document.querySelector('meta[name="DC.creator"]').setAttribute('content', $scope.asset[schema.record].Creator.display_value);
      // document.querySelector('meta[property="og:creator"]').setAttribute('content', $scope.asset[schema.record].Creator.display_value);
    }
    if($scope.asset[schema.genre]){
      document.querySelector('meta[name="DCTERMS.subject"]').setAttribute('content', $scope.asset["Genre"]);
    }

    if($scope.asset["Date"]){
      var metaDate =  new Date($scope.asset["Date"]);
      document.querySelector('meta[name="DCTERMS.issued"]').setAttribute('content', metaDate.getFullYear());
    }

    //tileSource=  $scope.asset.file["pyramidal_server_path"] + $scope.asset.file["pyramidal_image_path"];

    /** @type {string}
      * @default
      * @description Base URL for the IIIF viewer.
      * @memberof View
      */
    // if ( $scope.asset.File.data.storData["pyramidal_server_path"] || $scope.asset.File.data.storData["pyramidal_image_path"] ) {
    //   tileSource = $scope.asset.File.data.storData["pyramidal_server_path"] + $scope.asset.File.data.storData["pyramidal_image_path"] + "/info.json";
    //   console.log(tileSource);
    //   tileSource = "http://tsprod.artstor.org/rosa-iiif-endpoint-1.0-SNAPSHOT/fpx%2Fsslps%2Fc7729596%2F10118719.fpx/info.json";
    

    records.getWorkRecord($scope.asset.Work, function(response) {
      $log.debug(response);
    });
  })
  .error(function(res) {
    $scope.errorMsg = "Object could not be located";
  })
  ;
  
  $scope.fullscreen = false;

  var changeHandler = function(){                      
    if (document.webkitIsFullScreen || document.mozFullScreen || document.fullscreen)
    {
      $scope.fullscreen = true;
    } else {
      
      $scope.fullscreen = false;
      $scope.selectingItems = 0;

      $scope.compareAssets.splice(1);
      $scope.compareAssetsIds.splice(1);

      // Pause compared audio / video assets
      for(var i = 1; i < $scope.compareAssets.length; i++){
        var comparedAsset = $scope.compareAssets[i];
        if( (comparedAsset.Media === 'vid') || (comparedAsset.Media === 'aud') ){
          document.getElementById('video-' + comparedAsset['Meta-Id'] + '-' + (i + 1) ).sendNotification('doPause');
        }
      }
    }  
  };

  document.addEventListener('fullscreenchange', function () {
        changeHandler();
    }, false);

    document.addEventListener('mozfullscreenchange', function () {
        changeHandler();
    }, false);

    document.addEventListener('webkitfullscreenchange', function () {
        changeHandler();
    }, false);

  function requestFullScreen(el) {
      // Supports most browsers and their versions.
      var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullScreen;

      if (requestMethod) { // Native full screen.
        requestMethod.call(el);
      } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
      }
    }

    function exitFullScreen() {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }

  $scope.togglePresentationMode = function(){
      var elem = document.body; // Make the body go full screen.

      if (!$scope.fullscreen) {
        ga('send', {
          hitType: 'event',
          eventCategory: 'Access',
          eventAction: 'Enter Full Screen Mode',
          eventLabel: $scope.assetId
        });
        requestFullScreen(elem);
        $scope.fullscreen = true;
      } else {

        $scope.compareAssets.splice(1);
        $scope.compareAssetsIds.splice(1);

        // Pause compared audio / video assets
        for(var i = 1; i < $scope.compareAssets.length; i++){
          var comparedAsset = $scope.compareAssets[i];
          if( (comparedAsset.Media === 'vid') || (comparedAsset.Media === 'aud') ){
            document.getElementById('video-' + comparedAsset['Meta-Id'] + '-' + (i + 1) ).sendNotification('doPause');
          }
        }
        exitFullScreen();
        $scope.fullscreen = false;
      }
  };

  /**
   * @function
   * @name formatMetadata
   * @description For formatting asset metadata values.
   * @param {string} value - Asset metadata value.
   * @returns {string} Formatted asset metadata values.
   * @memberof View
   */
  $scope.formatMetadata = function(value, key) {
    var jsVal;
    // var excludeVocabLinks = ['Style/Period:AAT', 'Organizer'];
    var excludeVocabLinks = ['Organizer'];

    if ( !value || value.length < 1 ) {
      return "";
    } else if ( value.constructor === Array ) {
      return value[0];
    } else if ( typeof value === "object" ) {
      //jsVal = JSON.parse(value);
      //console.log( jsVal );
      if ( value["display_value"] ) {

        if(value["links"].length > 0){
          var resultHTML = '<p>' + value["display_value"] + '</p>';
          for(var i = 0; i < value["links"].length; i++){
            var termObj = value["links"][i];
            if((termObj.source === 'TGN') && (excludeVocabLinks.indexOf(key) === -1)){
              resultHTML += '<div><a ng-click="clearSearchTerm(\'ART-Location : ' + termObj.data.preferredTerm.toLowerCase() + '\')" href="' + window.location.pathname + '#/?f=ART-Location:' +  encodeURIComponent(termObj.data.preferredTerm.toLowerCase()) + '&v=curate">' + termObj.data.preferredTerm + '</a></div>';
            }
            else if((termObj.source === 'AAT') && (excludeVocabLinks.indexOf(key) === -1)){
              if((key === 'Style/Period') || (key === 'Style/Period:AAT')){
                resultHTML += '<div><a ng-click="clearSearchTerm(\'ART-Style-Period : ' + termObj.data.preferredTerm.toLowerCase() + '\')" href="' + window.location.pathname + '#/?f=ART-Style-Period:' +  encodeURIComponent(termObj.data.preferredTerm.toLowerCase()) + '&v=curate">' + termObj.data.preferredTerm + '</a></div>';
              }
              else if((key === 'Materials/Techniques') || (key === 'Materials/Techniques:AAT')){
                resultHTML += '<div><a ng-click="clearSearchTerm(\'ART-Material : ' + termObj.data.preferredTerm.toLowerCase() + '\')" href="' + window.location.pathname + '#/?f=ART-Material:' +  encodeURIComponent(termObj.data.preferredTerm.toLowerCase()) + '&v=curate">' + termObj.data.preferredTerm + '</a></div>';  
              }
            }
            else if((termObj.source === 'SSN') && (excludeVocabLinks.indexOf(key) === -1)){
              resultHTML += '<div><a ng-click="clearSearchTerm(\'ART-Creator : ' + termObj.data.preferredTerm.toLowerCase() + '\')" href="' + window.location.pathname + '#/?f=ART-Creator:' +  encodeURIComponent(termObj.data.preferredTerm.toLowerCase()) + '&v=curate">' + termObj.data.preferredTerm + '</a></div>';
            }
          }
          return resultHTML;
        }
        else{
          return value["display_value"];
        }
      }
    } else {
      return value;
    }
  };

  $scope.clearSearchTerm = function(vocabGroup){
    if(vocabGroup){  
      ga('send', {
        hitType: 'event',
        eventCategory: 'Search',
        eventAction: 'Hyperlinked Vocabulary Term Clicked',
        eventLabel: vocabGroup
      });
    }

    searchQuery.clearSearchTerm();
  };

  $scope.viewAssetinSS = function(asset_id){
    if(asset_id){
      window.open("/?ssid=" + asset_id, '_blank');
    }
  };

  $scope.getRelatedItems = function(work_id){
    $scope.loadingRelatedAssets = true;
    assets.getRelatedDRs(work_id, function(response) {
      var idsArray = [];
      angular.forEach(response.dr, function(dr) {
        if(dr.id && (dr.id !== $scope.assetId)){
          idsArray.push(dr.id);
        }
      });
      console.log(idsArray);
      if(idsArray.length > 0){
        assets.getBulkDRsInfo(idsArray).then(function(data){
          angular.forEach(data, function(dataObj) {
            if(dataObj.data && dataObj.data.hits && dataObj.data.hits.hits && dataObj.data.hits.hits[0]){
              $scope.relatedAssets.push(dataObj.data.hits.hits[0]);
            }
          });
          console.log($scope.relatedAssets);
          $scope.loadingRelatedAssets = false;
        });
      } else {
        // No Related Assets
        $scope.loadingRelatedAssets = false;
      }
    });
  };

  $scope.getDateRltdDR = function(asset) {
    var date = "";
    if ( asset._source["ARTSTORX-Date"] ) {
        var dateObj = new Date(  asset._source["ARTSTORX-Date"] );
        date = dateObj.getFullYear();
    }
    return date;
  };

  $scope.getCreatorRltdDR = function(asset) {
    if ( asset._source["ARTSTORX-" + $scope.currentGenre + "-Creator"] ) {
        return asset._source["ARTSTORX-" + $scope.currentGenre + "-Creator"][0];
    } else if (asset._source["ARTSTORX-Agent"]) {
       return asset._source["ARTSTORX-Agent"][0];
    } else {
        return "";
    }
  };

  $scope.showShareOptions = function(){
    ga('send', {
      hitType: 'event',
      eventCategory: 'Share',
      eventAction: 'Show Share Options',
      eventLabel: 'Via Item Page'
    });

    angular.element( document.getElementById('shareAssetModal') ).addClass('fade-in');
  };

  $scope.trackAssetDownloads = function(){
    // Sending Download Item Data to google Analytics
  /*  ga('send', {
      hitType: 'event',
      eventCategory: 'asset_download_by_institution',
      eventAction: 'Harvard',
      eventLabel: $scope.assetId
    });

    ga('send', {
      hitType: 'event',
      eventCategory: 'asset_download_by_collection',
      eventAction: 'SSC',
      eventLabel: $scope.assetId
    }); */

    ga('send', {
      hitType: 'event',
      eventCategory: 'Access',
      eventAction: 'Download Media',
      eventLabel: $scope.assetId
    });

  };

  $scope.getAssetUrl = function(assetId) {
    return window.location.origin + window.location.pathname + '#/item/' + assetId;
  };
  
  // Convenience functions on scope
  $scope.isEmpty = function (obj) {
      for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
          return false;
        }
      }
      return true;
  };

  $scope.selectingItems = 0;
  $scope.toggleSelectionPane = function() {
    // Handle selectingItems as a number, since it is used in column calculations
    if ($scope.selectingItems === 0) {
      $scope.selectingItems = 1;
    } else {
      $scope.selectingItems = 0;
    }
  };

  // For implementing drag and drop in Presentation mode
  $scope.droppedItem = function(dragEl, dropEl) {
    console.log("Dropped", dragEl);
  };

  if ($localStorage.search_results && $localStorage.search_results.assets) {
    $scope.resultItems = $localStorage.search_results.assets;
  } else {
    $scope.resultItems = [$scope.asset];
  }

  $scope.addToCompare = function(asset) {
    var addId = parseFloat(asset._id);
    if ($scope.compareAssetsIds.indexOf(addId) >= 0) {
      // Remove asset if already showing
      $rootScope.$broadcast('removeComparedAsset', addId);
    } else {
      // Pull and add asset object
      assets.openById( addId ).success(function(response){
        var fullasset = response.hits.hits[0]._source;
        $scope.compareAssets.push(fullasset);
        $scope.compareAssetsIds.push(addId);
      });
    }
  };

  $rootScope.$on('removeComparedAsset', function(event, id) {
    console.log(id);
    // Loop through compare assets
    for (var index=0; index < $scope.compareAssets.length; index++) {
      if ( $scope.compareAssets[index]['Meta-Id'] === id) {
        $scope.compareAssets.splice(index,1);
      }
    }
    $scope.compareAssetsIds.splice($scope.compareAssetsIds.indexOf(id),1);
  });

})

;
