/**
 * @fileOverview Custom PDF Viewer module
 * @author Cody & Rehan
 * @namespace View
 * @version 1.0.1
 */
angular.module( 'alcove.pdfViewer', [])
  .controller( 'pdfViewerCtrl', function pdfViewerCtrl (
        $scope
      ) {

        // Jquery Kinetic allows drag-control panning
        $('#pdf-wrap').kinetic();

        $scope.getPageCount = function() {
          if ($scope.pageCount) {
            return $scope.pageCount;
          } else {
            return "1";
          }
        };
    }
  );
