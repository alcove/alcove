/**
 * @fileOverview Handles the implementation of various custom Angular JS directives.
 * @author Cody & Rehan
 * @namespace Directives
 * @version 1.0.1
 */
angular.module('alcove.simpleDirectives', [])
    /**
     * @name ngEnter
     * @description For catching an event whenever an "Enter Key" is pressed.
     * @memberof Directives
     */
	.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    })

    /**
     * @name ngTap
     * @description For catching an event whenever a "tap" event fires.
     * @memberof Directives
     */
    .directive('ngTap', function() {
        return function(scope, element, attrs) {
        var tapping;
        tapping = false;
        element.bind('touchstart', function(e) {
            element.addClass('active');
            tapping = true;
        });
        element.bind('touchmove', function(e) {
            element.removeClass('active');
            tapping = false;
        });
        element.bind('touchend', function(e) {
            element.removeClass('active');
            if (tapping) {
            scope.$apply(attrs['ngTap'], element);
            }
        });
        };
    })

  /**
     * @name compile
     * @description Compiles the sting html, to be loaded into the DOM - (Alternative for ng-bind-html to avoid string html parsing issues)
     * @memberof Directives
     */ 
  .directive('compile', ['$compile', function ($compile) {
      return function(scope, element, attrs) {
          scope.$watch(
              function(scope) {
                  return scope.$eval(attrs.compile);
              },
              function(value) {
                  element.html(value);
                  $compile(element.contents())(scope);
              }
          );
      };
  }])

    /**
     * @name lazyImage
     * @description Lazy loads the asset images. Delays the loading of images in long web pages. Images outside of viewport are not loaded until user scrolls to them. This is opposite to image preloading and improves the page load speed.
     * @memberof Directives
     */ 
    .directive('lazyImage', function() {
      return {
        restrict: 'A',
        scope: { lazyImage : '@' },
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                element.attr('src', scope.lazyImage);
                element.unbind('load');
            });
            element.bind('error', function() {
                scope.$apply(function() {
                    var elementObj = element[0];
                    if((elementObj.dataset.externalApi === 'lacma') || (elementObj.dataset.externalApi === 'brooklyn')){
                        if(elementObj.dataset.externalApi === 'lacma'){
                            scope.$parent.$parent.$parent.lacmaAssets.splice(elementObj.dataset.externalAssetIndex, 1);
                        }
                        else if(elementObj.dataset.externalApi === 'brooklyn'){
                            scope.$parent.$parent.$parent.brooklynAssets.splice(elementObj.dataset.externalAssetIndex, 1);
                        }
                    }
                    
                });
            });
        }
      };
    })
    ;
