// Convenience factories that do not access external services
// -- Used to maintain objects, write URLs
angular.module('alcove.handlers',[])
    .factory('searchQuery', function ($location, $rootScope, $routeParams, $timeout) {
        'use strict';
        
        // Factory has to return objects
        var activeFilters = {};
        var activeFiltersExclude = {};
        var filterArray = []; 
        var filterArrayExclude = [];
        var filterString = "";
        var filterStringExclude = "";
        var dateFilter =   {
            min: 0,
            max: new Date().getFullYear(),
            minEra: 'BCE',
            maxEra: 'CE'
        };
        var dateRangeQuery = false;
        var searchTerm = "";
        var sortField = "";
        var sortDirection = "asc";
        var currentLoadedLocation = "";
        var view = "";

        function readUrl() {
            // Reset values first
            searchTerm = "";
            clearFilters(true, true);

            // Detect Date Range in URLs
            if ($routeParams['drb']) {
                dateFilter.min  = parseInt($routeParams['drb']);
                dateFilter.max = parseInt($routeParams['dre']);
                if (parseInt($routeParams['drb']) < 0) {
                    dateFilter.min  = -1*parseInt($routeParams['drb']);
                    dateFilter.minEra = 'BCE';
                } else {
                    dateFilter.minEra  = 'CE';
                }
                if (parseInt($routeParams['dre']) < 0) {
                    dateFilter.max = -1*parseInt($routeParams['dre']);
                    dateFilter.maxEra = 'BCE';
                } else {
                    dateFilter.maxEra = 'CE';
                }
                dateRangeQuery = {
                    "DateRangeBegin" : {
                        "gte" : parseInt($routeParams['drb']),
                        "lte" : parseInt($routeParams['dre'])
                    },
                    "DateRangeEnd" : {
                        "gte" : parseInt($routeParams['drb']),
                        "lte" : parseInt($routeParams['dre'])
                    }
                };
            } else {
                dateRangeQuery = false;
            }

            // Detect search term in URL
            if ($routeParams['term']) {
                if (searchTerm != decodeURIComponent($routeParams['term'])) {
                    updateSearchTerm( decodeURIComponent($routeParams['term']) );
                }
            } else {
                searchTerm = "";
            }

            // Detect filters in URL
            if ( $routeParams['f'] ) {
                readUrlFilters(decodeURIComponent( $routeParams['f']));
            }

            if ( $routeParams['xf'] ) {
                readUrlXFilters(decodeURIComponent( $routeParams['xf'] ));
            }
            
            if ($routeParams['s']) {
                readUrlSort();
            }
            currentLoadedLocation = $location.url();
            $rootScope.$broadcast('searchQueryUpdate');
        }

        function setView(viewName) {
            view = viewName;
            $location.search('v', viewName);
            currentLoadedLocation = $location.url();
        }

        function isEmptyObject(obj) {
            for(var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    return false;
                }
            }
            return true;
        }

        function formatFilterString(urlObj) {
            var filterString = "";
            angular.forEach(urlObj, function(value, fieldGroup) {
                var encodedArray = [];
                for (var n = 0; n < urlObj[fieldGroup].length; n++) {
                    encodedArray.push( encodeURIComponent( urlObj[fieldGroup][n] ) );
                }
                if (encodedArray.length > 0 && encodedArray[0].length > 0) {
                    filterString = filterString + encodeURIComponent(fieldGroup) + ":" + encodedArray.join(':') + ";";
                } else {
                    filterString = filterString + encodeURIComponent(fieldGroup) + ";";
                }
            });
            return filterString;
        }

        function parseFilterParams(filterParams, excludeFilter) {
            // 'i' used to ensure infinite loop doesn't occur from a bad string
            var i = 0;
            while ( (filterParams.indexOf(':') >= 0 || filterParams.indexOf(';') >= 0) && i < 50) {
                var group = "";
                var filter = "";
                var hasFields = true;
                
                if (filterParams.indexOf(';') > 0 && filterParams.indexOf(';') < filterParams.indexOf(':') ) {
                    // If group doesn't have a field set (an existing filter), but other filters remain
                    hasFields = false;
                    group = filterParams.slice(0,filterParams.indexOf(';') );
                    filterParams = filterParams.slice(filterParams.indexOf(';') + 1, filterParams.length );
                } else if (filterParams.indexOf(':') > 0) {
                    // Associated Group and fields are separted by colons
                    group = filterParams.slice(0,filterParams.indexOf(':') );
                    filterParams = filterParams.slice(filterParams.indexOf(':') + 1, filterParams.length );
                } else if (filterParams.indexOf(';') > 0) {
                    // If group doesn't have a field set (an existing filter),
                    hasFields = false;
                    group = filterParams.slice(0,filterParams.indexOf(';') );
                    filterParams = filterParams.slice(filterParams.indexOf(';') + 1, filterParams.length );
                }
                
                if (hasFields) {
                    if (filterParams.indexOf(';') >= 0 ) {
                        filter = filterParams.slice( 0, filterParams.indexOf(';') );
                        filterParams = filterParams.slice(filterParams.indexOf(';') + 1, filterParams.length);
                    } else {
                        filter = filterParams;
                        filterParams = "";
                    }
                }
                
                // If there are more than one terms for a field
                if ( filter.indexOf(':') > 0 ) {
                    var filterTerm = "";
                    while ( filter.indexOf(':') >= 0 ) {
                        filterTerm = filter.slice(0, filter.indexOf(':'));
                        filter = filter.slice(filter.indexOf(':') + 1, (filter.length));
                        toggleFilter(group, filterTerm, true, excludeFilter);
                    }
                    // Toggle filter without loading until on last parameter
                    toggleFilter(group, filter, true, excludeFilter);
                } else {
                    toggleFilter(group, filter, true, excludeFilter);
                }
                i++;
            }
        }

        var toggleFilter = function(group, filter, quiet, exclude) {
            filter = filter.toString().toLowerCase();
            
            // console.log("Group " + group + "\nFilter " + filter + "\nQuiet " + quiet + "\nExclude " + exclude);

            if (typeof(group) == 'undefined' || typeof(filter) == 'undefined'){
                console.error("Filter was undefined.");
                return;
            }

            if(exclude){
                if (!activeFiltersExclude[group]) {
                    activeFiltersExclude[group] = {};
                }
            }
            else{
                if (!activeFilters[group]) {
                    activeFilters[group] = {};
                }
            }

            if(exclude){
                if ( activeFiltersExclude[group][filter] ) {
                    delete activeFiltersExclude[group][filter];

                    if(isEmptyObject(activeFiltersExclude[group])){
                        delete activeFiltersExclude[group];
                    }

                } else {
                    activeFiltersExclude[group][filter] = true;
                }
            }
            else{
                if ( activeFilters[group][filter] ) {
                    // Remove Filter
                    delete activeFilters[group][filter];

                    // Remove Filter Group from the active filters array, if it doesn't have any selected filters
                    if(isEmptyObject(activeFilters[group])){
                        delete activeFilters[group];
                    }

                } else {
                    // Add Filter
                    activeFilters[group][filter] = true;
                }
            }

            // Build Array for search query
            var filterString = "";
            var tempString = "";
            var urlObj = {};
            filterArray = [];

            angular.forEach(activeFilters, function(value, key) {
                var filterObjB = {};

                angular.forEach(activeFilters[key], function(valueB, keyB) {
                    filterObjB = { 'term' : {} };
                    filterObjB["term"][key] = '';
                    if (activeFilters[key][keyB] === true && keyB.length > -1) {
                        filterObjB["term"][key] = keyB;
                        filterArray.push( filterObjB );
                        // Save for URL construction
                        if (!urlObj[key]) {
                            urlObj[key] = [];
                        }
                        urlObj[key].push(keyB);
                    }
                });
            });

            /**********************************
             *  Encoding filters for the URL
            ***********************************/
            filterString = formatFilterString(urlObj);
            var xUrlObj = {};
            var filterStringExclude = "";
            filterArrayExclude = [];

            angular.forEach(activeFiltersExclude, function(value, key) {
                var filterObjC = {};

                angular.forEach(activeFiltersExclude[key], function(valueB, keyB) {
                    filterObjC = { 'term' : {} };
                    filterObjC["term"][key] = '';
                    if (activeFiltersExclude[key][keyB] === true && keyB.length > -1) {
                        filterObjC["term"][key] = keyB;
                        filterArrayExclude.push( filterObjC );
                        // Save for URL construction
                        if (!xUrlObj[key]) {
                            xUrlObj[key] = [];
                        }
                        xUrlObj[key].push(keyB);
                    }
                });
            });

            /**********************************
             * Encoding excluded filters for the URL
            **********************************/
            filterStringExclude = formatFilterString(xUrlObj);

            if(filterString){
                $location.search('f', filterString);
            }
            else{
                $location.search('f', null);
            }

            if (filterStringExclude) {
                $location.search('xf', filterStringExclude);
            } else {
                $location.search('xf', null);
            }

            if(!quiet) {
                // Sends Search
                $rootScope.$broadcast('searchQueryUpdate');
            }

            currentLoadedLocation = $location.url();
        };

        var applyDateFilter = function(newDateFilter, quiet) {
            dateFilter = dateFilter;
            var dateMin = dateFilter.min = Math.abs(newDateFilter.min);
            var dateMax = dateFilter.max = Math.abs(newDateFilter.max);

            dateFilter.minEra = newDateFilter.minEra;
            dateFilter.maxEra = newDateFilter.maxEra;

            if (dateFilter.minEra === 'BCE') {
                dateMin = dateFilter.min * -1;
            }
            if (dateFilter.maxEra === 'BCE') {
                dateMax = dateFilter.max * -1;
            }
            dateRangeQuery = {
                "DateRangeBegin" : {
                    "gte" : dateMin,
                    "lte" : dateMax
                },
                "DateRangeEnd" : {
                    "gte" : dateMin,
                    "lte" : dateMax
                }
            };
            $location.search('drb', dateMin);
            $location.search('dre', dateMax);
            currentLoadedLocation = $location.url();
            if(!quiet) {
                // Sends Search
                $rootScope.$broadcast('searchQueryUpdate');
            }
        };

        var clearDateFilter = function(quiet) {
            dateFilter = {
                min: 0,
                max: new Date().getFullYear(),
                minEra: 'BCE',
                maxEra: 'CE'
            };
            dateRangeQuery = false;
            $location.search('drb', null);
            $location.search('dre', null);
            currentLoadedLocation = $location.url();

            if(!quiet) {
                // Sends Search
                $rootScope.$broadcast('searchQueryUpdate');
            }
        };

        var clearFilters = function(quiet, noUpdateURL) {
            activeFilters = {};
            activeFiltersExclude = {};

            filterArray = [];
            filterArrayExclude = [];

            filterString = "";
            filterStringExclude = "";

            dateFilter = {
                min: 0,
                max: new Date().getFullYear(),
                minEra: 'BCE',
                maxEra: 'CE'
            };
            dateRangeQuery = false;

            if(!noUpdateURL){
                $location.search('drb', null);
                $location.search('dre', null);
                $location.search('f', null);
                $location.search('xf', null);
            }

            currentLoadedLocation = $location.url();

            if(!quiet) {
                // Sends Search
                $rootScope.$broadcast('searchQueryUpdate');
            }
        };

        var readUrlFilters = function(filterParams) {
            parseFilterParams(filterParams);
        };

        var readUrlXFilters = function(filterParams) {
            parseFilterParams(filterParams, true);
        };
        
        var readUrlSort = function() {
            if ($routeParams['s']) {
               var s = decodeURIComponent( $routeParams['s'] );
               if (s.indexOf(':') > -1) {
                   sortField = s.slice(0, s.indexOf(':'));
                   sortDirection = s.slice(s.indexOf(':') + 1);
               } else if (s.length > 0) {
                   sortField = s;
               } else {
                   $location.search('s', null);  
               }               
            }

            currentLoadedLocation = $location.url();
        };
        
        var setSort = function(field, direction, quiet) {
            if (field && direction) {
                sortField = field;
                sortDirection = direction;
                $location.search('s', sortField + ":" + sortDirection);  
            } else if (field) {
                sortField = field;
                $location.search('s', sortField);
            } else {
                $location.search('s', null);  
            }

            currentLoadedLocation = $location.url();
            if(!quiet) {
                // Sends Search
                $rootScope.$broadcast('searchQueryUpdate');
            }
        };

        var clearSearchTerm = function(){
            searchTerm = '';
            $location.search('term', null);

            currentLoadedLocation = $location.url();
        };

        var updateSearchTerm = function(newSearchTerm) {
            searchTerm = newSearchTerm;

            // Sends Search
            $rootScope.$broadcast('searchQueryUpdate');

            var param = encodeURIComponent(newSearchTerm);
            if (newSearchTerm.length > 0) {
                $location.search('term', param);
            } else {
                // Keep the URL tidy, remove the param
                $location.search('term', null);
            }

            currentLoadedLocation = $location.url();
        };

        var newSearch = function(newSearchTerm) {
            // Sending Search Event Data to google Analytics
            ga('send', {
              hitType: 'event',
              eventCategory: 'Search',
              eventAction: 'Keyword Search',
              eventLabel: newSearchTerm
            });
            
            clearFilters(true);
            searchTerm = newSearchTerm;
            // Sends Search
            $rootScope.$broadcast('searchQueryUpdate');

            var param = encodeURIComponent(newSearchTerm);
            if (newSearchTerm.length > 0) {
                $location.search('term', param);
            } else {
                // Keep the URL tidy, remove the param
                $location.search('term', null);
            }

            currentLoadedLocation = $location.url();
        };

        var searchWithFilter = function(newSearchTerm, group, filter) {
            // update search term
            clearFilters(true);
            searchTerm = newSearchTerm;
            var param = encodeURIComponent(newSearchTerm);
            $location.search('term', param);

            // update filter
            toggleFilter(group, filter);

            currentLoadedLocation = $location.url();
        };

        return {

            toggleFilter: toggleFilter,
            clearFilters: clearFilters,
            applyDateFilter: applyDateFilter,
            clearDateFilter: clearDateFilter,
            setView: setView,
            setSort: setSort,
            readUrl: readUrl,
            readUrlSort: readUrlSort,
            readUrlFilters: readUrlFilters,
            readUrlXFilters: readUrlXFilters,
            getActiveFilters: function() {
                return activeFilters;
            },
            getActiveFiltersExclude: function() {
                return activeFiltersExclude;
            },
            getFilterArray: function() {
                return filterArray;
            },
            getFilterArrayExclude: function() {
                return filterArrayExclude;
            },
            getDateFilter: function() {
                return dateFilter;
            },
            getDateRangeQuery: function() {
                return dateRangeQuery;
            },
            getSearchTerm: function() {
                return searchTerm;
            },
            getSortField: function() {
                return sortField;
            },
            getSortDirection: function() {
                return sortDirection;
            },
            getCurrentLoadedLocation: function() {
                return currentLoadedLocation;
            },
            updateSearchTerm: updateSearchTerm,
            clearSearchTerm: clearSearchTerm,
            newSearch: newSearch,
            searchWithFilter: searchWithFilter
        };
      })

    ;
