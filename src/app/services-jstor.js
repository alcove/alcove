angular.module('jstor.services', [])

  .provider("mode", function ($windowProvider) {
        this.hostname = $windowProvider.$get().location.hostname;
        this.$get = function () {
            return {
                hostname: this.hostname,
                local: this.hostname == 'labs.jstor.org.local' || this.hostname == 'localhost' || this.hostname == '127.0.0.1',
                github: this.hostname == 'jstor-labs.github.io',
                prod: !(this.hostname == 'localhost' || this.hostname == '127.0.0.1' || this.hostname == 'jstor-labs.github.io')
                };
            };
    })


.run(function ($rootScope, $http, mode) {

        $rootScope.baseUrl = mode.local ? '/#/' : mode.github ? '/images/#/' : '/images/';

        // API host and credentials
        $rootScope.apiHost =  'labs.jstor.org';
        $rootScope.apiProtocol = mode.local ? 'https://' : 'https://';
        //$rootScope.apiHost = 'labs.jstor.org';
        //$rootScope.apiProtocol ='https://';

        $rootScope.apiBaseURL = $rootScope.apiProtocol + $rootScope.apiHost + '/api';

        $rootScope.authHost = 'https://labs.jstor.org';
        $rootScope.authProtocol = mode.local ? 'https://' : 'https://';
        $rootScope.authBaseURL = $rootScope.authProtocol + $rootScope.authHost + '/accounts';

        // user credentials
        $rootScope.user = "demo"; //localStorage.getItem("user");
        $rootScope.password = "demo"; //localStorage.getItem("password");
        $rootScope.authToken = null;

        // console.log('apiProtocol='+$rootScope.apiProtocol+" apiHost="+$rootScope.apiHost + " apiBaseURL="+$rootScope.apiBaseURL + " authBaseURL="+$rootScope.authBaseURL);
    })

.service('JWTToken', function ($http, $q, $rootScope, $httpParamSerializerJQLike) {
        //var cachedToken;
        var refreshInterval = 590 * 1000;  // 590 seconds, 10 less than Labs JWT expiration
        var lastRefresh;
        var p;
        return {
            getToken: function(refresh) {
                refresh = refresh || !lastRefresh || Date.now() > lastRefresh + refreshInterval;
                // console.log('getToken',refresh, $rootScope.authToken);
                //return $q.when(refresh ? helper() : cachedToken || p || helper());
                return $q.when(refresh ? helper() : $rootScope.authToken || p || helper());
            }
        };
        function helper () {
            var deferred = $q.defer();
            p = deferred.promise;
            var data = {username: "demo", password: "demo"};
            $http({
                method: 'POST', url: $rootScope.apiBaseURL + '/token-auth/',
                headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
                data: $httpParamSerializerJQLike( data ) //angular.toJson( data ) // angular.toJson
            }).success(function (data) {
                //cachedToken = data.token;
                lastRefresh = Date.now();
                $rootScope.authToken = data.token;
                deferred.resolve(data.token);
            }).error(function (data) {
                console.log('JWT error',data);
                deferred.reject('Error');
            });
            return deferred.promise;
        }

    })

    .service("SearchSOLR", function ($rootScope, $http, JWTToken, $q) {
        return {
            select: function (query, start, rows, filters, sort) {
                return JWTToken.getToken().then(function(token) {
                    return helper(query, start, rows, filters, sort, token);
                });
            }
        };
        function helper(query, start, rows, filters, sort, token, d) {
          // console.log('SearchSOLR',token);
            var deferred = d || $q.defer();
            var url = $rootScope.apiBaseURL + '/solr/labs/select/?wt=json';
            url += '&q='+(query ? query : '*:*');
            url += '&start='+(start ? start : 0);
            url += '&rows='+(rows ? rows : 20);
            if (filters) {
              for (var i = 0; i < filters.length; i++) {
                url += '&fq='+filters[i];
              }
            }
            if (sort) {
               url += '&sort='+sort; 
            } 
            //console.log(url);
            $http.get(url, {headers: {'Authorization': 'JWT ' + token}}).then(
                function (response) {
                    deferred.resolve({docs: response.data.response.docs, numFound:response.data.response.numFound});
                },
                function (err) {
                    if (!d) {
                        JWTToken.getToken(true).then(
                            function (token) {
                                return helper(query, start, rows, filters, sort, token, deferred);
                            },
                            function (err) {
                                deferred.reject(err);
                            });
                    } else {
                        deferred.reject(err);
                    }
                });
            return deferred.promise;
        }
    });