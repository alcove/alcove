/**
 * @fileOverview Route and Controller for Home page.
 * @author Cody & Rehan
 * @namespace Home
 * @version 1.0.1
 */

angular.module( 'alcove.home', [
    'titleService',
    'ngStorage'
])

.config( function config( $routeProvider ) {
  $routeProvider.when( '/?', {
    controller: 'HomeCtrl',
    templateUrl: 'home/home.jade',
    reloadOnSearch: false
  });
})

.controller( 'HomeCtrl', function HomeCtrl(
        $scope,
        $localStorage,
        titleService,
        account,
        assets,
        records,
        $route,
        $location,
        $routeParams,
        $timeout,
        $interval,
        external,
        $rootScope,
        $log,
        $window,
        searchQuery,
        schema,
        findAssetField,
        $q,
        SearchSOLR,
        hotkeys
    ) {

    $rootScope.$on('$routeUpdate', function(scope, next, current) {
        // Check for value updates
        $timeout(function() {
            console.log("Route update");
            if (searchQuery.getCurrentLoadedLocation() != $location.url()) {
                searchQuery.readUrl();
            }
        });
    });

    document.querySelector('meta[property="og:url"]').content = $location.absUrl();

    // Services to scope
    $scope.findAssetField = findAssetField;
    $scope.schema = schema;

    // Variables
    $scope.assets = [];
    $scope.brooklynAssets = [];
    $scope.RijksmuseumAssets = [];
    $scope.lacmaAssets = [];
    $scope.newReleaseAssets = [];
    $scope.wikiTerms = [];
    $scope.facets = {};
    $scope.contextInfo = {};
    $scope.assetCount = 0;
    $scope.message = {};

    $scope.activeFilters = searchQuery.getActiveFilters();
    $scope.filterArray = searchQuery.getFilterArray();

    $scope.activeFiltersExclude = searchQuery.getActiveFiltersExclude();
    $scope.filterArrayExclude = searchQuery.getFilterArrayExclude();

    $scope.inResults = false;

    // Keyboard Shortcuts
    hotkeys
        .bindTo($scope)
        .add({ 
            combo: 'mod+s',
            description: 'Focus on search field',
            callback: function(event) {
                event.preventDefault();

                var activeFilterGroup = document.querySelector('filter-group[data-state=active]');
                if(activeFilterGroup){
                    activeFilterGroup.setAttribute('data-state', 'inactive'); // Remove focus from the filter panel
                }

                setTimeout(function(){ 
                    var inputElement = document.getElementById('autocomplete');
                    inputElement.focus();
                }, 250);
            }
        })
        .add({ 
            combo: 'mod+f',
            description: 'Focus on filter panel (first filter group)',
            callback: function(event) {
                event.preventDefault();
                document.activeElement.blur();
                document.querySelectorAll('filter-group[data-state=inactive]')[0].setAttribute('data-state', 'active');
            }
        })
        .add({ 
            combo: 'w',
            description: 'Switch between catalog & exhibition views',
            callback: function(event) {
                event.preventDefault();

                if($scope.showCatalogView){ // Go to exhibition view
                    $scope.loadExhibitionView();
                }
                else{// Go to catalog view
                    $scope.loadDisplayView();
                }
            }
        })
        .add({ 
            combo: 's',
            description: 'Switch between sort options',
            callback: function(event) {
                event.preventDefault();

                if($scope.showCatalogView){
                    var topResults;
                    if($scope.searchTerm && ($scope.searchTerm !== '*')){
                        topResults = true;
                    }

                    if($scope.sortByNewest){
                        if(topResults){
                            $scope.selectSortByRelevance();
                        }
                        else{
                            $scope.toggleSortByTitle();
                        }
                    }   
                    else if($scope.sortByRelevance){
                        $scope.toggleSortByTitle();
                    }
                    else if($scope.sortByTitle){
                        $scope.toggleSortByDate();
                    }
                    else if($scope.sortByDate){
                        $scope.toggleSortByNewest();
                    }
                }
            }
        })
        .add({ 
            combo: 'mod+r',
            description: 'Focus on results',
            callback: function(event) {
                event.preventDefault();

                var activeFilterGroup = document.querySelector('filter-group[data-state=active]');
                if(activeFilterGroup){
                    activeFilterGroup.setAttribute('data-state', 'inactive'); // Remove focus from the filter panel
                }

                setTimeout(function(){ 
                    if($scope.showCatalogView){ // Go to exhibition view
                        document.querySelector('#resultsContent thumbnail:nth-of-type(1)').focus();
                    }
                    else{
                        document.querySelector('#right thumbnail:nth-of-type(1)').focus();
                    }
                }, 250);
            }
        })
        .add({ 
            combo: 'up',
            description: 'Navigate up within the filter panel',
            callback: function(event) {
                event.preventDefault();
                var activeElement = document.querySelector('filter-group[data-state=active]');
                var prevActiveElement = activeElement ? activeElement.previousElementSibling : null;
                if(activeElement && prevActiveElement && (activeElement.tagName === 'FILTER-GROUP') && (prevActiveElement.tagName === 'FILTER-GROUP') && (activeElement.getAttribute('data-state') === 'active') && (prevActiveElement.getAttribute('data-state') === 'inactive')){
                    activeElement.setAttribute('data-state', 'inactive');
                    prevActiveElement.setAttribute('data-state', 'active');
                }
            }
        })
        .add({ 
            combo: 'down',
            description: 'Navigate down within the filter panel',
            callback: function(event) {
                event.preventDefault();
                var activeElement = document.querySelector('filter-group[data-state=active]');
                var nextActiveElement = activeElement ? activeElement.nextElementSibling : null;
                if(activeElement && nextActiveElement && (activeElement.tagName === 'FILTER-GROUP') && (nextActiveElement.tagName === 'FILTER-GROUP') && (activeElement.getAttribute('data-state') === 'active') && (nextActiveElement.getAttribute('data-state') === 'inactive')){
                    activeElement.setAttribute('data-state', 'inactive');
                    nextActiveElement.setAttribute('data-state', 'active');
                }
            }
        })
        .add({ 
            combo: 'right',
            description: 'Navigate right within the container',
            callback: function(event) {
                event.preventDefault();
                var activeElement = document.activeElement;
                var nextActiveElement = activeElement ? activeElement.nextElementSibling : null;
                if(activeElement && nextActiveElement && (activeElement.tagName === 'THUMBNAIL') && (nextActiveElement.tagName === 'THUMBNAIL')){
                    setTimeout(function(){ 
                        nextActiveElement.focus();
                    }, 250);
                }
            }
        })
        .add({ 
            combo: 'left',
            description: 'Navigate left within the container',
            callback: function(event) {
                event.preventDefault();
                var activeElement = document.activeElement;
                var prevActiveElement = activeElement ? activeElement.previousElementSibling : null;
                if(activeElement && prevActiveElement && (activeElement.tagName === 'THUMBNAIL') && (prevActiveElement.tagName === 'THUMBNAIL')){
                    setTimeout(function(){ 
                        prevActiveElement.focus();
                    }, 250);
                }
            }
        })
        .add({ 
            combo: 'enter',
            description: 'Open / View a selected asset / filter group',
            callback: function(event) {
                event.preventDefault();

                var activeElement = document.activeElement;
                var activeFilterGroup = document.querySelector('filter-group[data-state=active]');

                if(activeElement && (activeElement.tagName === 'THUMBNAIL')){
                    $timeout(function() { 
                        activeElement.children[0].children[0].click();
                    }, 0);
                }
                else if(activeFilterGroup){
                    $timeout(function() { 
                        activeFilterGroup.children[0].click();
                    }, 0);
                }
            }
        });

    // Current applied Date Filter value
    $scope.appliedDateFilter = {
        min: 0,
        max: new Date().getFullYear(),
        minEra: 'BCE',
        maxEra: 'CE'
    };

    // Form date filter value
    $scope.dateFilter = {
        min: 0,
        max: new Date().getFullYear(),
        minEra: 'BCE',
        maxEra: 'CE'
    };

    $scope.dateFilterApplied = false;
    
    if ($localStorage['earliestDate']) {
        $scope.dateFilter.min = $scope.appliedDateFilter.min = $localStorage['earliestDate'].year;
        $scope.dateFilter.minEra = $scope.appliedDateFilter.minEra = $localStorage['earliestDate'].era;
    }

    if ($localStorage['latestDate']) {
        $scope.dateFilter.max = $scope.appliedDateFilter.max = $localStorage['latestDate'].year;
        $scope.dateFilter.maxEra = $scope.appliedDateFilter.maxEra = $localStorage['latestDate'].era;
    }
    
    $scope.toggleEra = function(eraVar) {
      if (eraVar === 'BCE') {
        return 'CE';
      } else if (eraVar === 'BCE') {
        return 'BCE';
      } else {
        return 'BCE';
      }
    };

    $scope.page = 1;
    $scope.showCatalogView = false;
    $scope.nameContextExactMatch = false;

    $scope.loadingMoreResults = false;

    $scope.searchingAssets = false;
    $scope.noResults = false;
    $scope.loadingExternalMuseumResults = false;
    $scope.loadingLacmaResults = false;

    var rangeQuery = {};
    var appliedFilters = [];
    var pageSize = 48;
    var pageCount = 1;

    $scope.sortByTitle = false;
    $scope.sortByDate = false;
    $scope.sortByNewest = false;
    $scope.sortByRelevance = false;

    $scope.clearSorting = function(preserveSort) {
        if (preserveSort !== 'title'){
        	$scope.sortByTitle = false;
        }
        if (preserveSort !== 'date'){
        	$scope.sortByDate = false; 
        }
        if (preserveSort !== 'new'){
	        $scope.sortByNewest = false; 
        }
        $scope.sortByRelevance = false;
    };

    $scope.clearFilters = function() {
        searchQuery.clearFilters();
    };

    $scope.contextInfo.aatArray = [];
    $scope.contextInfo.namesArray = [];
    $scope.contextInfo.tgnArray = [];
    $scope.contextInfo.wikiArray = [];
    $scope.contextInfo.wikiTerms = '';

    $scope.nameRecordFieldOrder = [
        "birthText",
        "deathText",
        "nationality",
        "gender"
    ];

    $scope.currentFeaturedColIndex = 0;
    $scope.featuredCollections = [
            { image: 'assets/images/featured_collections/feature-trinity.jpg',
                title: '',
                subtitle: 'Trinity College Art Collection',
                description: 'From Japanese woodblock prints to Haitian paintings', 
                collectionId: '87730855' 
            },
            { image: 'assets/images/featured_collections/feature-gma.jpg',
                title: '',
                subtitle: 'Gettysburg College Map Collection',
                description: 'Seventeenth and eighteenth century maps of Europe, Asia, and the Americas', 
                collectionId: '87730795' 
            },
            { image: 'assets/images/featured_collections/feature-pjm.jpg',
                title: 'Cornell: Persuasive Cartography',
                subtitle: 'The PJ Mode Collection',
                description: 'Maps that send a message', 
                collectionId: '87730456' 
            },
            { image: 'assets/images/featured_collections/feature-prc-wellesley.jpg',
                title: '&ldquo;Serve the People!&rdquo;<br>',
                subtitle: 'Images of Daily Life in China during the Cultural Revolution',
                description: 'Photographs of the People’s Republic of China by William A. Joseph of Wellesley College' , 
                collectionId: '87730856' 
            }
        ];

    // $scope.newCollections = [
    //     {image1: 'assets/images/new_collections/newCollections_1a.jpg', image2: 'assets/images/new_collections/newCollections_1b.jpg', image3: 'assets/images/new_collections/newCollections_1c.jpg', title: 'The Museum of Modern Art: Painting and Sculpture', creator: 'ARTstor Collections', count: '1469'},
    //     {image1: 'assets/images/new_collections/newCollections_2a.jpg', image2: 'assets/images/new_collections/newCollections_2b.jpg', image3: 'assets/images/new_collections/newCollections_2c.jpg', title: 'The Metropolitan Museum of Art: Exhibition Installation Photographs', creator: 'ARTstor Collections', count: '8253'},
    //     {image1: 'assets/images/new_collections/newCollections_3a.jpg', image2: 'assets/images/new_collections/newCollections_3b.jpg', image3: 'assets/images/new_collections/newCollections_3c.jpg', title: "Ghiberti's Gates of Paradise Collection", creator: 'ARTstor Collection', count: '855'},
    // ];
    $scope.newTopCollectionsArray = {};
    $scope.newTopCollectionsHeading = 'New Collections';

    $scope.gaze = {};
    $scope.gaze.imageUrl = 'assets/images/gaze-2.jpg';
    $scope.gaze.assetId = '158345';
    $scope.gaze.description = '"Butler, Pennsylvania 2 / Company Town 2 / Steel Town" by Kingsbury, Alison Mason. 1949. Cornell: Alison Mason Kingsbury: Life and Art.';

    // For Asset Share Options
    $scope.shareUrl = '';
    $scope.shareMediaUrl = '';
    $scope.shareDesc = '';

    $scope.$storage = $localStorage;
    $scope.encodeURIComponent = window.encodeURIComponent;

    var resetTimeout = false;
    $scope.changeFeaturedCol =  function(index){
        console.log("CHANGE");
      resetTimeout = true; // interupts interval
      $scope.currentFeaturedColIndex = index;
    };

    $interval(function() {
        if (!resetTimeout) {
            if($scope.currentFeaturedColIndex < 2){
               $scope.currentFeaturedColIndex ++;
            }
            else{
                $scope.currentFeaturedColIndex = 0;
            }
        } else {
            resetTimeout = false;
        }
    }, 8000);

    // Check if a string is a part / substring of an array element
    Array.prototype.findReg = function(match) {
        var reg = new RegExp(match);

        return this.filter(function(item){
            return typeof item == 'string' && item.match(reg);
        });
    };

    function resetScroll() {
        if ($scope.showCatalogView) {
            if (document.getElementById('results')) {
                document.getElementById('results').scrollTop = 0;
            }
            document.getElementById('bodyContent').scrollTop = 0;
            $rootScope.progressbar.reset();
        }
    }

    $scope.$watch( $scope.showCatalogView, function() {
        resetScroll();
    });

    $scope.openUrl = function(url) {
        $state.go(url);
    };

    function exitFullScreen() {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }

    // Default site to not using fullscreen right now
    // Resolves issues selecting links from data in presentation mode
    exitFullScreen();

    function loadHomeView() {
        document.getElementById('bodyContent').scrollTop = 0;

        searchQuery.setView(null);
        $rootScope.progressbar.reset();
        $scope.showCatalogView = false;
        $scope.showHomeView = true;
        $rootScope.$broadcast('refreshNav');

        isCurated = true;


        $timeout(function() {
            if ( document.getElementById('bodyContent').scrollTop < 100) {
                // Temporary solution to getting Nav bar to be instantly transparent on home view
                angular.element( document.querySelector('#nav .nav-bg') ).addClass('no-fade');
                angular.element( document.querySelector('#nav') ).addClass('no-bg');
                
                setTimeout(function() {
                    angular.element( document.querySelector('#nav .nav-bg') ).removeClass('no-fade');
                }, 4000);
            }
        });
    }

    $scope.isEmpty = function(object){
        for(var property in object) {
            if(object.hasOwnProperty(property)){
                return false;
            }
        }
        return true;
    };

    $scope.loadExhibitionView = function loadExhibitionView() {
        ga('send', {
          hitType: 'event',
          eventCategory: 'Browse',
          eventAction: 'Display Exhibition View',
          eventLabel: 'Exhibition View'
        });

        document.getElementById('bodyContent').scrollTop = 0;

        searchQuery.setView('curate');

        // Exhibition should return to defaults of 'Newest' without search term and 'Relevance' with search term
        $scope.clearSorting();

        // $rootScope.progressbar bar doesn't show in Exhibition, resetting position prevents it from "sticking"
        $rootScope.progressbar.reset();
        $scope.showCatalogView = false;
        $scope.showHomeView = false;
        $rootScope.$broadcast('fetchBlogEnteriesFromCache', $scope.cacheData);
        isCurated = true;
        
        if($scope.searchAssets){
            $scope.searchAssets();
        }

        $timeout(function() {
            angular.element( document.getElementById('nav') ).removeClass('no-bg');
        });
    };

    $scope.loadDisplayView = function loadDisplayView() {
        ga('send', {
          hitType: 'event',
          eventCategory: 'Browse',
          eventAction: 'Display Catalog View',
          eventLabel: 'Catalog View'
        });

        document.getElementById('bodyContent').scrollTop = 0;
        
        searchQuery.setView('display');
        $scope.showCatalogView = true;
        $scope.showHomeView = false;
        $rootScope.$broadcast('refreshNav');

        isCurated = false;
        $scope.loadNoMore = true;
        
        // Sort is set on searchQueryUpdate, so we need to update here
        if($scope.isSearching() && (!$scope.sortField || $scope.sortField.length < 1)) {
            $scope.sortByRelevance = true;
        } 
        
        if($scope.searchAssets){
            $scope.searchAssets();
        }

        $timeout(function() {
            angular.element( document.getElementById('nav') ).removeClass('no-bg');
        });
    };

    $scope.isSearching = function() {
        var term = $scope.searchTerm ? $scope.searchTerm : "";
        var filters =  $scope.filterArray ? $scope.filterArray : [];
        var dateRangeQuery = searchQuery.getDateRangeQuery();

        if (term.length > 0 || $scope.filterArray.length > 0 || $scope.filterArrayExclude.length > 0 || dateRangeQuery[schema.dateBegin]) {
            return true;
        } else {
            return false;
        }
    };

    $scope.isFiltering = function() {
        if ( $scope.filterArray.length > 0 || $scope.filterArrayExclude.length > 0 ) {
            return true;
        }
        return false;
    };

    function loadView() {
        // Detect view mode in URL
        if ( $routeParams['v'] == "display" ) {
            $scope.loadDisplayView();
        } else if ( $routeParams['v'] === "curate" || $scope.isSearching() === true ) {
            $scope.loadExhibitionView();
        } else {
            loadHomeView();
        }
    }

    // Check view on initial load
    loadView();


    $scope.loadingDateFilter = false;
    $scope.dateFilterMessage = "";
    $scope.applyDateFilter = function() {
        $scope.dateFilterMessage = "";
        $scope.loadingDateFilter = true;
        $scope.dateFilterApplied = true;
        searchQuery.applyDateFilter($scope.dateFilter);
    };
    
    $scope.resetDateFilter = function() {
        $scope.appliedDateFilter = {
            min: 0,
            max: new Date().getFullYear(),
            minEra: 'BCE',
            maxEra: 'CE'
        };

        $scope.dateFilter = {
            min: 0,
            max: new Date().getFullYear(),
            minEra: 'BCE',
            maxEra: 'CE'
        };
        
        if ($localStorage['earliestDate']) {
            $scope.dateFilter.min = $scope.appliedDateFilter.min = $localStorage['earliestDate'].year;
            $scope.dateFilter.minEra = $scope.appliedDateFilter.minEra = $localStorage['earliestDate'].era;
        }

        if ($localStorage['latestDate']) {
            $scope.dateFilter.max = $scope.appliedDateFilter.max = $localStorage['latestDate'].year;
            $scope.dateFilter.maxEra = $scope.appliedDateFilter.maxEra = $localStorage['latestDate'].era;
        }
    };

    $scope.clearDateFilter = function() {
        $scope.dateFilterMessage = "";
        $scope.dateFilterApplied = false;
        $scope.loadingDateFilter = true;
        $scope.resetDateFilter();
        searchQuery.clearDateFilter();
    };

    $scope.showHideFilterGroupLabel =  function(filter_key, filter_value){
        var existsInActiveFilters =  false;
        var existsInActiveFiltersExclude = false;

        angular.forEach($scope.activeFilters, function(value, key) {
            if(filter_key === key){
                existsInActiveFilters = true;
            }
        });

        angular.forEach($scope.activeFiltersExclude, function(value, key) {
            if(filter_key === key){
                existsInActiveFiltersExclude = true;
            }
        });

        if(existsInActiveFilters || existsInActiveFiltersExclude || (filter_value && filter_value.buckets && filter_value.buckets.length > 0)){
            return true;
        }
        else{
            return false;
        }

    };

    $scope.countAppliedFilters = function(key){
        var count = 0;
        var includeFilterCount = 0;
        var excludeFilterCount = 0;

        if($scope.activeFilters[key]){
            includeFilterCount  = Object.keys($scope.activeFilters[key]).length;        
        }
        if($scope.activeFiltersExclude[key]){
            excludeFilterCount = Object.keys($scope.activeFiltersExclude[key]).length;
        }

        return (includeFilterCount + excludeFilterCount);
    };

    $scope.validateDateInput = function(event){
        console.log('key pressed');
        console.log(event);

        if(!((event.keyCode > 95 && event.keyCode < 106) || (event.keyCode > 47 && event.keyCode < 58) || (event.keyCode > 36 && event.keyCode < 41) || (event.keyCode == 65  && (event.ctrlKey || event.metaKey)) || event.keyCode == 8)) {
            event.preventDefault();
        }
    };

    function formatHtmlContent(blurb) {
        if (blurb) {
            var contentString = blurb.toString();
            contentString = contentString.replace('\n','')
                .replace('<html>', '')
                .replace('</html>', '');
            var content = angular.element("<div>" + contentString + "</div>");

            content.find("img").remove()
                .find("a[rel]").remove()
                .find("br").remove();

            var formattedContent = content.html();
            return formattedContent;
        } else {
            return "";
        }
    }

    $scope.minWordsLong = function(string, number) {
      if ( !string ) {
        return false;
      }
      var wordArray = string.split(/\s+/);
      if (wordArray.length > number) {
        return true;
      } else {
        return false;
      }
    };

    $scope.descExpandLink = "Expand";

    $scope.toggleColDesc = function() {
      if ($scope.colDescLimit === 50) {
        $scope.colDescLimit = "all";
        $scope.descExpandLink = "Collapse";
      } else {
        $scope.colDescLimit = 50;
        $scope.descExpandLink = "Expand";
      }
    };

    function getRelatedCollections(instId, selectedCollection) { 
        if (!selectedCollection) {
            selectedCollection = "";
        }
        if ( instId ) {
            assets.getInstitutionCollections(instId)
                .then(function successCallback(response) {
                    var colKeyArray = [];
                    var colCount = {};
                    console.log(response);
                    angular.forEach(response.data.aggregations[schema.collections].buckets, function(obj, key) {
                        console.log(selectedCollection, obj.key);
                        if (obj.key && selectedCollection.toString().indexOf(obj.key.toString()) < 0) {
                            colKeyArray.push(obj.key);
                            colCount[obj.key] = obj.doc_count;
                        }
                    });
                    var collectionsObj = {};
                    // Fetch Collections Metadata
                    console.log(colKeyArray);

                    assets.getCollectionById(colKeyArray)
                        .success(function(response) {
                             console.log(response);
                            if(response && response.hits){
                                var colKeyArray = [];
                                $scope.contextInfo.relCollections = [];
                                angular.forEach(response.hits.hits, function(obj, key) {
                                    var collectionsObj = {};
                                    collectionsObj.name = obj._source.name;
                                    collectionsObj.desc = obj._source.short_description;
                                    collectionsObj.image = obj._source.big_image_url;
                                    collectionsObj.id = obj._source.id;
                                    collectionsObj.count = colCount[collectionsObj.id];
                                    $scope.contextInfo.relCollections.push(collectionsObj);
                                });
                            }
                        })
                        .error(function(response) {
                             console.log(response);
                            console.log("Unable to access Collections index");
                        });

                }, function errorCallback(response) {
                    
                });
        }
    }
    
    // I hold the handle on the current request for data. Since we want to
    // be able to abort the request, mid-stream, we need to hold onto the
    // request which will have the .abort() method on it.
    var requestForAssets = null;
    
    // I abort the current request (if its running).
    $scope.abortRequest = function() {
        return( requestForAssets && requestForAssets.abort() );
    };
    
    // $scope.searchJstorSolr = function(term) {
    //     SearchSOLR.select(term).then(function(res) {
    //         console.log(res.data);
    //     });
    // };
    
    $scope.searchAssets = function(searchArgs) { 
        if (!searchArgs) { searchArgs = {}; }
        //term, filters, range, moreResults, facet_group, facet_filter
        var term = (searchArgs.term ? searchArgs.term : $scope.searchTerm);
        
        var moreResults;
        if(searchArgs.fuzzyFallback){
            moreResults = $scope.loadingMoreResults;
        }
        else{
            moreResults = (searchArgs.moreResults ? searchArgs.moreResults : false);
        }

        if (!moreResults) {
             $scope.facetsLoaded = false;
        }
        
        var filters;
        var filtersExclude;
        var range = {};
        var facet_group;
        var facet_filter;
        var booleanTerm = (searchArgs.booleanTerm ? searchArgs.booleanTerm : "AND");
        var fuzzyFallback = (searchArgs.fuzzyFallback ? searchArgs.fuzzyFallback : false);

        // Status messaging variables
        $scope.searchingAssets = true;
        $scope.noResults = false;
        $scope.searchFailed = false;
        $scope.message = {};

        if (fuzzyFallback) {
            $scope.message.searchExpanded = true;
        }
        // Cancel any existing search in progress
        if ($scope.searchingAssets === true) {
            $scope.abortRequest();
        }

        if (searchArgs.filters) {
            filters = searchArgs.filters;
        } else if (($scope.filterArray.length > 0) || ($scope.filterArrayExclude.length > 0) ) {
            filters = $scope.filterArray;
            filtersExclude = $scope.filterArrayExclude;
        }

        var size = pageSize;

        if (term.length <= 0) {
            term = "*";
            titleService.setTitle("");
        } else if (term.length > 1) {
            titleService.setTitle(term);
        }

        if(!moreResults){
            $scope.page = 1;
            $scope.assets = [];
            resetScroll();
        }

        if ($scope.dateFilterApplied && searchArgs.range) {
            range = searchArgs.range;
        } else if ($scope.dateFilterApplied && rangeQuery[schema.dateBegin]) {
            range = rangeQuery;
        }

        if (searchArgs.facet_group) {
            facet_group = searchArgs.facet_group;
        }
        if (searchArgs.facet_filter) {
            facet_filter = searchArgs.facet_filter;
        }

        var newRealse = false;
        if(term === '*'){
            newRealse = true;
        }
        
        /*
         * Show Collection view
         * - If there is a collection filter applied, load collection data
         * - Triggered by search panel selection or the filter panel
         */
        $scope.contextInfo.relCollections = [];
        if(!searchArgs.moreResults){
            $scope.collection = {};
        }
        if (!moreResults && $scope.activeFilters[schema.collections]){
            ga('send', {
              hitType: 'event',
              eventCategory: 'Browse',
              eventAction: 'Browse Collection',
              eventLabel: 'Collection View'
            });

          var collectionId;
          var multipleCollections = false;
          for(var name in $scope.activeFilters[schema.collections]) {
            if (collectionId) {
                // If we've already found one, we don't want to show any collection info
                multipleCollections = true;
                collectionId = '';
                $scope.collection = {};
            } else {
                collectionId = name;    
            } 
          }
          if ($scope.activeFilters[schema.collections][collectionId]) {
            assets.getCollectionById(collectionId)
              .success(function(response) {
                $scope.colDescLimit = 50;
                if (response.hits.total > 0) {
                    $scope.collection = response.hits.hits[0]._source;
                    $scope.collection.description = formatHtmlContent($scope.collection.blurb);
                    getRelatedCollections($scope.collection.institution_id, collectionId);
                }
              });
          }
        } else if (!moreResults) {
          // If there isn't a collection filter applied, clear the collection view object
          delete $scope.collection;
        }
        if ($scope.activeFilters[schema.institutionId]) {
            for (var inst in $scope.activeFilters[schema.institutionId]) {
                getRelatedCollections( inst );
            } 
        }

        /*
         * Pass off parameters to our massive search call
         * - To-Do: Simplify. Use optional parameters?
         */

        if( (term !== '*') || filters || filtersExclude || range.DateRangeBegin || moreResults || $scope.sortByDate || $scope.sortByTitle || $scope.sortByRelevance || ($scope.sortByNewest && $scope.showCatalogView)){
            $scope.newTopCollectionsArray = {};
            $scope.newTopCollectionsHeading = 'Top Collections';
            
            // Get Blog Posts based on search term
            $timeout(function() {
                // Timeout needed to make sure element is loaded
                $rootScope.$broadcast('fetchBlogEnteries',{searchTerm: term});
            });
            
            // Search call is wrapped so we have access to the 'abort()' attached to the returned promise for canceling in progress searches
            ( requestForAssets = assets.search(term, $scope.page, size, filters, range, $scope.sortByTitle, $scope.sortByDate, $scope.sortByNewest, newRealse, filtersExclude, booleanTerm, fuzzyFallback) )
                .then(
                    function( res ) {
                        // Flag the data as loaded.
                        $scope.searchingAssets = false;
                        if (res.data.hits.total === 0 && !fuzzyFallback && !$scope.searchTerm.match(/["|'](?:[^"'\\]|\\.)*["|']/)) {
                            // If user is not specific a strict match, fallback to fuzzy
                            $log.info('Expanded search to fuzzy.');
                            $scope.searchAssets({ 'fuzzyFallback' : true });
                        } else {
                            setSearchAssetResponse(res.data, term, booleanTerm, moreResults, filters);
                            getCollectionsData(res.data);
                            getOpenWebAssets(term);
                        }
                    },
                    function( res ) {
                        if (res.statusText === "Aborted") {
                            // Do nothing, search canceled on purpose
                        } else {
                            // Error messaging should be triggered
                            $scope.searchFailed = true;
                            $scope.searchingAssets = false;
                            getOpenWebAssets(term);
                        }
                        
                    }
            );
            
            
        }
        else{
            $scope.newTopCollectionsHeading = 'New Collections';
            if($scope.cacheData && $scope.cacheData.cache){
                getCollectionsData($scope.cacheData.cache.recentAssets);
                setSearchAssetResponse($scope.cacheData.cache, term, booleanTerm, moreResults, filters);
                $rootScope.$broadcast('fetchBlogEnteriesFromCache',$scope.cacheData);
                
                $timeout(function() {
                    // Delay loading Open Web assets
                    getOpenWebAssets(term);
                }, 100);
            }
            else {
                assets.getCachedData().success(function(response) {
                    $scope.cacheData = response;
                    getCollectionsData($scope.cacheData.cache.recentAssets);
                    setSearchAssetResponse($scope.cacheData.cache, term, booleanTerm, moreResults, filters);
                    $rootScope.$broadcast('fetchBlogEnteriesFromCache',$scope.cacheData);
                    
                    $timeout(function() {
                        // Delay loading Open Web assets
                        getOpenWebAssets(term);
                    });
                });
            }   
        }

        getEvents();

    };
    
    function getOpenWebAssets(term) {
        if (!term) {
            term = "*";
        }
        // Delay loading Open Web assets
        $scope.loadingExternalMuseumResults = true;
        $scope.loadingLacmaResults = true;
        getBrooklynMuseumData(term);
        getRijksmuseumData(term);
        // getLacmaMuseumData(term);
    }

    function getCollectionsData(response){
        // Get Top Collection Results
        if(response.aggregations[schema.collections] && response.aggregations[schema.collections].buckets && (response.aggregations[schema.collections].buckets.length > 0)){
            var colKeyArray = [];
            var collectionsObj = {};
            angular.forEach(response.aggregations[schema.collections].buckets, function(obj, key) {
                // Limit array to reduce collection query
                if (colKeyArray.length < 5) {
                   colKeyArray.push(obj.key);
                   collectionsObj[obj.key] = {};
                   collectionsObj[obj.key].count = obj.doc_count; 
                }
            });

            // Fetch Collections Metadata
            assets.getCollectionById(colKeyArray)
            .success(function(response) {
                if(response && response.hits && response.hits.hits){
                    // $scope.newTopCollectionsArray = response.hits.hits;
                    angular.forEach(response.hits.hits, function(obj, key) {
                        collectionsObj[obj._source.id].name = obj._source.name;
                        collectionsObj[obj._source.id].desc = obj._source.short_description;
                        collectionsObj[obj._source.id].image = obj._source.big_image_url;
                        collectionsObj[obj._source.id].id = obj._source.id;
                    });

                    // Delete collections without metadata.
                    angular.forEach(collectionsObj, function(obj, key) {
                        if(!obj.name){
                            delete collectionsObj[key];
                        }
                    });

                    $scope.newTopCollectionsArray = collectionsObj;
                }
            })
            .error(function(response) {
                console.log("Unable to access Collections index");
            });
        }
    }

    function processCollectionFilters(buckets) {
        var colKeyArray = [];
        $localStorage['collectionFilter'] = $localStorage['collectionFilter'] ? $localStorage['collectionFilter'] : {};

        angular.forEach(buckets, function(obj, key) {
            colKeyArray.push(obj.key);
            if (!$localStorage['collectionFilter'][obj.key] || typeof($localStorage['collectionFilter'][obj.key]) != "object") {
                $localStorage['collectionFilter'][obj.key] = {};
            }
            $localStorage['collectionFilter'][obj.key].doc_count = obj.doc_count; 
        });

        /*
            Get Collection data to use in filter
        */
        var processCollResponse = function(responseHits) {
            angular.forEach(responseHits, function(col) {
                colKeyArray.splice(colKeyArray.indexOf(col._source.id), 1);

                if (!$localStorage['collectionFilter'][col._source.id] || typeof($localStorage['collectionFilter'][col._source.id]) != "object") {
                    $localStorage['collectionFilter'][col._source.id] = {};
                }
                $localStorage['collectionFilter'][col._source.id].name = col._source.name;
            });
        };

        assets.getCollectionById(colKeyArray, ["id", "institution_id", "name"])
            .success(function(res1) {
            // Attach names to results
            processCollResponse(res1.hits.hits);
        })
        .error(function(response) {
            console.log("Unable to access Collections index");
        });
    }

    function setSearchAssetResponse(response, term, booleanTerm, moreResults, filters) {
        var search_results_ids = [];
        var cachedInstitutions = {};
        if (response.institutions) {
          cachedInstitutions = response.institutions;
        }
        if (response.recentAssets) {
            var cachedAssets = true;
            response = response.recentAssets;
        }

        /*
            No results for term: Use "OR"
            Switch boolean value to "OR" if no results come back for a search term
        */
        // if (response.hits.total === 0 && term.length > 1 && booleanTerm === "AND") {
        //     $scope.searchAssets({ "booleanTerm" : "OR"});
        //     return;
        // }

        /*
            Load Returned Assets
            Replace or push assets, depending on whether this is a pagination/infinite-scroll query or not
        */
        if(moreResults){
            angular.forEach(response.hits.hits, function(result) {
              $scope.assets.push(result);
            });
            $scope.loadingMoreResults = false;
        }
        else{
            $scope.assets = [];
            var newReleaseAssets = [];
            angular.forEach(response.hits.hits, function(result) {
            	$scope.assets.push(result);
                
          	    if(cachedAssets){
                    newReleaseAssets.push(result);
                }
            });
            if(newReleaseAssets.length > 0){
                $localStorage['newReleaseAssets'] = $scope.newReleaseAssets = newReleaseAssets;
            }
        }

        /*
            Display Asset Count
        */
        $scope.assetCount = response.hits.total;
        if ($scope.assetCount < 1) {
            $scope.noResults = true;
            if (!$localStorage.newReleaseAssets || $localStorage.newReleaseAssets.length < 1) {
                // No New Release Assets to show! Load from cache call
                assets.getCachedData().success(function(response) {
                    if (response.cache.recentAssets) {
                        angular.forEach(response.cache.recentAssets.hits.hits, function(result) {
                            $scope.newReleaseAssets.push(result);
                        });
                        $localStorage['newReleaseAssets'] = $scope.newReleaseAssets;
                    }
                });
            }
        }

        /*
            Date Filter search:
            Check if returned empty
        */
        if ($scope.loadingDateFilter && response.hits.total === 0) {
            $scope.dateFilterMessage = "Unable to find assets within that range";
        }
        $scope.loadingDateFilter = false;
        

        /*
            Get Collection Names
            --------------------
            Loop through objects within buckets for collections
            Take keys for Collections and place in an Array
        */
        processCollectionFilters(response.aggregations[schema.collections].buckets);
        
        /*
            Get Institution data to use in filter
        */
        
        if (!$localStorage['institutionFilter']) {
            $localStorage['institutionFilter'] = {};
        }
        var institutionBuckets = response.aggregations[schema.institutionId].buckets;

        var buildInstitutionObj = function(buckets, instArray) {
            var instKeyArray = [];
            angular.forEach(buckets, function(obj) {
                instKeyArray.push(obj.key);
                if (!$localStorage['institutionFilter'][obj.key]) {
                    $localStorage['institutionFilter'][obj.key] = {};
                }
                $localStorage['institutionFilter'][obj.key].doc_count = obj.doc_count;
            });

            if (typeof(instArray) != "undefined") {
                for(var i = 0; i < instArray.length; i++){
                    if( instKeyArray.indexOf( instArray[i].institutionId ) > -1 ){
                        if (!$localStorage['institutionFilter'][ instArray[i].institutionId ]) {
                            $localStorage['institutionFilter'][ instArray[i].institutionId ] = {};
                        }
                        $localStorage['institutionFilter'][ instArray[i].institutionId ].name = instArray[i].institutionName;
                        continue;
                    }
                }
            }
        };
        
        if (cachedInstitutions) {
            var cachedInstitutionsArray = [];
            angular.forEach(cachedInstitutions["ssInstitutions"], function(inst) {
                cachedInstitutionsArray.push(inst);
            });
            $localStorage['institutions'] = cachedInstitutionsArray;
            buildInstitutionObj(institutionBuckets, $localStorage['institutions']);
        } else if(!$localStorage['institutions']){
            // Pull and save all Institution names and ids
            var institutionsArray = [];
            assets.getAllInstitutions()
                .success(function(response) {
                    institutionsArray = [];
                    angular.forEach(response["ssInstitutions"], function(inst) {
                        institutionsArray.push(inst);
                    });
                    $localStorage['institutions'] = institutionsArray;
                    buildInstitutionObj(institutionBuckets, $localStorage['institutions']);
                })
                .error(function(response) {
                    // Backup data - should only fire on local dev, but useful backup
                    //$localStorage['institutions'] = [ ]
                    assets.getBackupInstitutions().success(function(response){
                        institutionsArray = [];
                        angular.forEach(response["ssInstitutions"], function(inst) {
                            institutionsArray.push(inst);
                        });
                        $localStorage['institutions'] = institutionsArray;
                        buildInstitutionObj(institutionBuckets, $localStorage['institutions']);
                    });
                });
        }
        
        /*
            Pull data from wild card search:
            - Used by Autocomplete in NavBar
            - Used by date range filter
            - Copy facets for wildcard search before modifying
        */
        if ( (term == "*" || term.length <= 0) && !filters) {                         
            // Save for auto complete bar
            $localStorage["allItemFacets"] = JSON.parse(JSON.stringify(response.aggregations));
        }


        /*
            Date Filter:-
            Set Date Filter Ranges
        */
        if (!$scope.dateFilterApplied && response.aggregations && response.aggregations['EarliestDate']) {
            
            var iso_earliest = response.aggregations['EarliestDate'].value;
            var earliestDate = new Date(iso_earliest).getFullYear();
            
            if(iso_earliest && earliestDate){
                var earliestEra = 'CE';
                if (earliestDate < 0){
                    earliestDate = Math.abs(earliestDate);
                    earliestEra = 'BCE';
                }
                $localStorage['earliestDate'] = { year: earliestDate, era:earliestEra };
                $scope.appliedDateFilter.min = $scope.dateFilter.min = $localStorage['earliestDate'].year;
                $scope.appliedDateFilter.minEra = $scope.dateFilter.minEra = $localStorage['earliestDate'].era;
            }
        }

        if(!$scope.dateFilterApplied && response.aggregations && response.aggregations['LatestDate']){
            var iso_latest = response.aggregations['LatestDate'].value;
            var latestDate = new Date(iso_latest).getFullYear();
            var today = new Date().getFullYear();
            
            if(iso_latest && latestDate){
                var latestEra = 'CE';
                if (latestDate < 0){
                    latestDate = Math.abs(latestDate);
                    latestEra = 'BCE';
                }
                if((latestDate > today) && (latestEra == 'CE')){
                    latestDate = today;
                }

                $localStorage['latestDate'] = { year: latestDate, era:latestEra };
                $scope.appliedDateFilter.max = $scope.dateFilter.max = $localStorage['latestDate'].year;
                $scope.appliedDateFilter.maxEra = $scope.dateFilter.maxEra = $localStorage['latestDate'].era;
            }
        } 

        /* 
            Used by Autocomplete in NavBar
            Remove title bucket after it has been saved for autocomplete
        */
        if (response.aggregations["Title"]) {
            delete response.aggregations["Title"];
        }


        if(moreResults) {
            search_results_ids = $localStorage.search_results.ids_array;
        } else {
            if (!$localStorage.search_results) {
                $localStorage.search_results = {};
            }
            $localStorage.search_results.assets = [];
        }

        angular.forEach(response.hits.hits, function(asset_result) {
            if(asset_result._id){
                search_results_ids.push(asset_result._id);
            }
        });
        if (!$localStorage.search_results) {
            $localStorage.search_results = {};
        }
        $localStorage.search_results.ids_array = search_results_ids;
        if (!$localStorage.search_results.assets) {
            $localStorage.search_results.assets = [];
        }
        
        $localStorage.search_results.assets = $localStorage.search_results.assets.concat(response.hits.hits);
        $localStorage.search_results.totalResults = $scope.assetCount;
        $localStorage.last_search_url = $location.url();

        $scope.searchingAssets = false;

        if(response.hits.total === 0){
            $scope.assets = [];
        }

        pageCount = Math.ceil( $scope.assetCount / pageSize );
        
        if(response.aggregations && response.aggregations.Collections && response.aggregations.Collections.buckets && (response.aggregations.Collections.buckets.length > 0)){
            for(var i = 0; i < response.aggregations.Collections.buckets.length; i++){
                if($localStorage['collectionFilter'] && $localStorage['collectionFilter'][response.aggregations.Collections.buckets[i].key]){
                    // response.aggregations.Collections.buckets[i].id = response.aggregations.Collections.buckets[i].key;
                    response.aggregations.Collections.buckets[i].label = $localStorage['collectionFilter'][response.aggregations.Collections.buckets[i].key].name;
                }
            }
        }

        if(response.aggregations && response.aggregations['Meta-InstitutionId'] && response.aggregations['Meta-InstitutionId'].buckets && (response.aggregations['Meta-InstitutionId'].buckets.length > 0)){
            for(var j = 0; j < response.aggregations['Meta-InstitutionId'].buckets.length; j++){
                if($localStorage['institutionFilter'] && $localStorage['institutionFilter'][response.aggregations['Meta-InstitutionId'].buckets[j].key]){
                    response.aggregations['Meta-InstitutionId'].buckets[j].label = $localStorage['institutionFilter'][response.aggregations['Meta-InstitutionId'].buckets[j].key].name;
                }
            }
        }

        $scope.facets = response.aggregations;
        $scope.facetsLoaded = true;

        $scope.runContextCheck();
    }

    function setDateRange(dateFilter) {
        $scope.dateFilter = dateFilter;
        angular.extend($scope.appliedDateFilter, dateFilter);
        rangeQuery = searchQuery.getDateRangeQuery();
    }

    $rootScope.$on('searchQueryUpdate', function() {
        $scope.searchTerm = searchQuery.getSearchTerm();
        $scope.activeFilters = searchQuery.getActiveFilters();
        $scope.activeFiltersExclude = searchQuery.getActiveFiltersExclude();
        $scope.filterArray = searchQuery.getFilterArray();
        $scope.filterArrayExclude = searchQuery.getFilterArrayExclude();
        
        $scope.sortField = searchQuery.getSortField();
        $scope.sortDirection = searchQuery.getSortDirection();
        
        $scope.sortByTitle = false;
        $scope.sortByDate = false;
        $scope.sortByNewest = false;
        $scope.sortByRelevance = false;

        if (searchQuery.getDateRangeQuery()){
            $scope.dateFilterApplied = true;
            setDateRange(searchQuery.getDateFilter());
        } else {
            $scope.dateFilterApplied = false;
        }

        // Set Window/Tab title
        if ($scope.searchTerm && $scope.searchTerm.length > 0) {
            titleService.setTitle( $scope.searchTerm );
        } else {
            titleService.setTitle("");
        }
        
        switch ($scope.sortField) {
            case 'title':
                $scope.sortByTitle = $scope.sortDirection;
                break;
            case 'date':
                $scope.sortByDate = $scope.sortDirection;
                break;
            case 'new' :
                $scope.sortByNewest = $scope.sortDirection;
                break;
            case 'rel' :
                $scope.sortByRelevance = true;
                break;
            default:
                if($scope.isSearching()) {
                    $scope.sortByRelevance = true;
                } else{
                    $scope.sortByNewest = 'desc';
                }
        }
        
        loadView();
        $scope.searchAssets();
    });

    $scope.toggleFilter = function(group, filter, exclude) {

        // Sending Selected Facet Data to google Analytics
        ga('send', {
          hitType: 'event',
          eventCategory: 'Search',
          eventAction: 'Select/Clear Facet',
          eventLabel: group + ' : ' + filter
        });

        if (group.length > 0 && filter ){
            searchQuery.toggleFilter(group, filter, false, exclude);
        }

        var term = $scope.searchTerm;
    };

    // Read URL Initially
    searchQuery.readUrl();

    $scope.popularAssets = [];

    if (typeof(L) == 'undefined') {
      console.log("Mapbox is unavailable.");
    } else {
      L.mapbox.accessToken = 'pk.eyJ1IjoiYXJ0c3RvcmRlc2lnbiIsImEiOiJwalR1N3dNIn0.6gT46_kwQ6FoqBMFwJMvcA';
    }

    $scope.currentGenre = "ART";

    var sourceObjectLabel = "Source-Record";

    $scope.events = [];

    // if ( $localStorage['userLongitude'] ) {
    //     external.getNYArtBeat( $localStorage['userLatitude'], $localStorage['userLongitude'] );
    // }

    function getEvents() {
        if ($localStorage['userLocation']) {
            // Get City/Region name
        	$scope.userLocation = $localStorage['userLocation'].split(",")[0]; 
        } 

        if ($localStorage['userLatitude'] && $localStorage['userLongitude']) {
            // if user is near NYC, use:
            // external.getNYArtBeat( $localStorage['userLatitude'], $localStorage['userLongitude'] );

            // else, check Eventful:
            if ($scope.searchTerm.length > 0) {
                external.getEvents( $localStorage['userLatitude'], $localStorage['userLongitude'], false, $scope.searchTerm ).success(function(data) {
                        if (data.events && data.events["event"].length > 2) {
                            $scope.searchEvents = data.events["event"];
                        } else {
                            external.getEvents( $localStorage['userLatitude'], $localStorage['userLongitude'], true, $scope.searchTerm ).success(function(data) {
                                    console.info("Expanded event search");
                                    if (data.events && data.events["event"].length > 2) {
                                        $scope.searchEvents = data.events["event"];
                                    }
                                });
                        }
                    })
                    .error(function(data, status) {
                        // $log.error(status);
                    });
            } else {
                external.getEvents( $localStorage['userLatitude'], $localStorage['userLongitude'] ).success(function(data) {
                        if (data.events && data.events["event"].length > 4) {
                            $scope.events = data.events["event"];
                        } else {
                            external.getEvents( $localStorage['userLatitude'], $localStorage['userLongitude'], true).success(function(data) {
                                    console.info("Expanded event search");
                                    if (data.events && data.events["event"].length > 2) {
                                        $scope.events = data.events["event"];
                                    }
                                });
                        }
                    })
                    .error(function(data, status) {
                        
                    });
            }
        }
    }

    var shortMonthArray = ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'];

    $scope.toShortDate = function(dateString) {
        // Time format from Eventful throws off "Date()" in Firefox
        // Assumes date in format "MM/DD/YYYY" (11 Characters)
        if (dateString != null) {
            dateString = dateString.slice(0,10);
        }
        var date = new Date(dateString);
        var formattedDate =  shortMonthArray[date.getMonth()] + " " + date.getDate();
        if (formattedDate != "undefined") {
            return formattedDate;
          } else{
            return dateString.slice(5,11);
          }
    };

    $scope.currentTag = function(dateString) {
        if (dateString === null) {
           return "Soon";
        }
        var date = new Date(dateString);
        var today = new Date();

        if (date.getTime() <= today.getTime()) {
            return "Now";
        } else {
            return "Soon";
        }
    };

    //$rootScope.$on('userLocated', function() {
        getEvents();
    //});



    function getBrooklynMuseumData(term){
        $scope.brooklynAssets = [];

        external.searchBrooklynData(term).success(function(res) {
            if(res && res.data && (res.data.length > 0)){
                angular.forEach(res.data, function(item) {
                    $scope.brooklynAssets.push(item);
                });
            }
            $scope.loadingExternalMuseumResults = false;

        });
    }

    $scope.loadMoreBrooklynResults = function(){
        $scope.loadingExternalMuseumResults = true;
        var startIndex = $scope.brooklynAssets.length;
        var term = $scope.searchTerm;
        external.searchBrooklynData(term, startIndex).success(function(data) {
            if(data && data.response && data.response.resultset && data.response.resultset.items && (data.response.resultset.items.length > 0)){
                angular.forEach(data.response.resultset.items, function(item) {
                    $scope.brooklynAssets.push(item);
                });
            }
            $scope.loadingExternalMuseumResults = false;
        });
    };

    function getRijksmuseumData(term){
        $scope.RijksmuseumAssets = [];

        external.searchRijksmuseumData(term).success(function(res) {
            if(res && res.artObjects && (res.artObjects.length > 0)){
                angular.forEach(res.artObjects, function(item) {
                    $scope.RijksmuseumAssets.push(item);
                });
            }
            $scope.loadingExternalMuseumResults = false;

        });
    }

    $scope.loadMoreRijksmuseumResults = function(){
        $scope.loadingExternalMuseumResults = true;
        var startIndex = $scope.RijksmuseumAssets.length;
        var term = $scope.searchTerm;
        external.searchRijksmuseumData(term, startIndex).success(function(res) {
            if(res && res.artObjects && (res.artObjects.length > 0)){
                angular.forEach(res.artObjects, function(item) {
                    $scope.RijksmuseumAssets.push(item);
                });
            }
            $scope.loadingExternalMuseumResults = false;
        });
    };

    function getLacmaMuseumData(term){
        $scope.lacmaAssets = [];

        external.searchLacmaData(term).success(function(data) {

            if(data && data.hits && data.hits.hits && (data.hits.hits.length > 0)){
                angular.forEach(data.hits.hits, function(hit) {
                    var asset = hit;
                    asset._source.v_umo = getParameterByName(asset._source.PrimaryImageURL , 'v_umo');
                    $scope.lacmaAssets.push(asset);
                });
            }
            $scope.loadingLacmaResults = false;

        });
    }

    $scope.loadMoreLacmaResults = function(){
        $scope.loadingLacmaResults = true;
        var startIndex = $scope.lacmaAssets.length;
        var term = $scope.searchTerm;
        external.searchLacmaData(term, startIndex).success(function(data) {
            if(data && data.hits && data.hits.hits && (data.hits.hits.length > 0)){
                angular.forEach(data.hits.hits, function(hit) {
                    var asset = hit;
                    asset._source.v_umo = getParameterByName(asset._source.PrimaryImageURL , 'v_umo');
                    $scope.lacmaAssets.push(asset);
                });
            }
            $scope.loadingLacmaResults = false;
        });
    };

    //For extracting url parameter value from a url string
    function getParameterByName(urlString, urlParameterName) {
        if(!urlString){
            return "";
        }
        urlParameterName = urlParameterName.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + urlParameterName + "=([^&#]*)"),
            results = regex.exec(urlString);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    $scope.getEntryContent = function(entry) {
        return $sce.trustAsHtml( entry._source.content );
    };

    $scope.openEventfulUrl = function(url) {
        $window.open(url.replace('library.artstor.org', 'eventful.com'), '_blank');
    };

    $scope.updateSearchTerm = function(term) {
        var param = encodeURIComponent(term);
        $location.path( '/' + param );
    };

    $scope.loadMoreResults = function(){

        // Infinite Scroll should be functional only in the Catalouge View
        if(!$scope.showCatalogView){
            return;
        }

        if($scope.loadingMoreResults){
            return;
        }
        else{
            $scope.loadingMoreResults = true;
        }

        if(($scope.page * pageSize) >= $scope.assetCount){
            $scope.loadNoMore = true;
            $scope.loadingMoreResults = false;
            return;
        }
        $scope.page++;
        $scope.searchAssets( { moreResults : true } );
    };



    // if ($scope.searchTerm.length <= 0 && !noAutoSearch) {
    //     $scope.searchAssets({ term : "*" });
    // }  else if (!noAutoSearch) {
    //     $scope.searchAssets();
    // }

    $scope.checkExactMatchesForContextInfo = function(searchTerm, hits){
        var hit = hits[0];
        var context_nameId = '';
        if(hit && hit._source && hit._source[sourceObjectLabel] && hit._source[sourceObjectLabel]["Creator_links"]){
            angular.forEach( hit._source[sourceObjectLabel]["Creator_links"], function(creator) {
                var searchTerm_index = $.map(creator.allTerms, function(n,i){return n.toLowerCase();}).findReg(searchTerm);
                if((searchTerm) && (searchTerm.length > 0) && (searchTerm_index.length > 0)){
                    context_nameId = creator.source_id;
                    $scope.nameContextExactMatch = true;
                    $scope.setNamesContextInfo(context_nameId);
                }
                else{
                    $scope.nameContextExactMatch = false;
                    $scope.fetchWikipediaInfo();
                }
            });
        }
        else{
            $scope.nameContextExactMatch = false;
            $scope.fetchWikipediaInfo();
        }
    };

    $scope.setNamesContextInfo = function(id){
        records.getNameRecord(id, function(response) {
            var nameObj = {};
            nameObj.preferredTerm = response.conceptDetailInfo.preferredTerm;
            nameObj.recordType = response.conceptDetailInfo.recordType;
            nameObj.id = response.conceptDetailInfo.conceptId;

            var birthText = '';
            var deathText = '';
            if(response.conceptDetailInfo && response.conceptDetailInfo.biographies && response.conceptDetailInfo.biographies.biography && (response.conceptDetailInfo.biographies.biography.length > 0)){
                angular.forEach(response.conceptDetailInfo.biographies.biography, function(biography) {
                    if (biography.preferred){
                        if(biography.birthDate && biography.birthDate.year){
                            birthText += biography.birthDate.year;
                        }
                        if(biography.birthDate && biography.birthDate.epoch){
                            if(birthText !== ''){
                                birthText += ' ';
                            }
                            birthText += biography.birthDate.epoch;
                        }
                        if(biography.birthPlace && biography.birthPlace.name){
                            if(birthText !== ''){
                                birthText += ' - ';
                            }
                            birthText += biography.birthPlace.name;
                        }

                        if(biography.deathDate && biography.deathDate.year){
                            deathText += biography.deathDate.year;
                        }
                        if(biography.deathDate && biography.deathDate.epoch){
                            if(deathText !== ''){
                                deathText += ' ';
                            }
                            deathText += biography.deathDate.epoch;
                        }
                        if(biography.deathPlace && biography.deathPlace.name){
                            if(deathText !== ''){
                                deathText += ' - ';
                            }
                            deathText += biography.deathPlace.name;
                        }
                        nameObj.gender = biography.gender;
                    }
                });
            }

            if(response.conceptDetailInfo && response.conceptDetailInfo.nationalities && response.conceptDetailInfo.nationalities.nationality && (response.conceptDetailInfo.nationalities.nationality.length > 0)){
                angular.forEach(response.conceptDetailInfo.nationalities.nationality, function(nationality) {
                    if (nationality.preferred){
                       nameObj.nationality = nationality.name;
                    }
                });
            }

            if(response.conceptDetailInfo && response.conceptDetailInfo.roles && response.conceptDetailInfo.roles.role && (response.conceptDetailInfo.roles.role.length > 0)){
                angular.forEach(response.conceptDetailInfo.roles.role, function(role) {
                    if (role.preferred){
                       nameObj.prefRole = role.name;
                    }
                });
            }

            if(response.conceptDetailInfo && response.conceptDetailInfo.scopeNotes && response.conceptDetailInfo.scopeNotes.scopeNote && (response.conceptDetailInfo.scopeNotes.scopeNote.length > 0)){
                nameObj.noteText = response.conceptDetailInfo.scopeNotes.scopeNote[0].scopeNoteText;
            }

            nameObj.birthText = birthText;
            nameObj.deathText = deathText;

            if( response.conceptDetailInfo.associatives && response.conceptDetailInfo.associatives.associative && ( response.conceptDetailInfo.associatives.associative.length > 0 ) ){
                nameObj.associativesArray = response.conceptDetailInfo.associatives.associative;
            }

            $scope.contextInfo.namesArray.push(nameObj);

            // records.fetchArtistPortrait(id, function(response) {
            //     if(response && response.HGETALL && response.HGETALL.Filename){
            //         $scope.contextInfo.name.portrait = response.HGETALL.Filename;
            //         $scope.contextInfo.name.portraitTitle = response.HGETALL.title;
            //     }
            //     console.log($scope.contextInfo);
            // });

        });
    };

    $scope.fetchWikipediaInfo = function(reset, filterName){
        /* ***** Show wikipedia results if there are no search results from our system ***** Start ***** */

        $scope.wikiTerms = [];
        var term = '';
        angular.forEach($scope.activeFilters, function(facet_value, facet_key) {
            angular.forEach(facet_value, function(filter_value, filter_key) {
                if(facet_key === 'ART-Agent'){
                    if(term !== ''){
                        term += '|';
                    }
                    term += filter_key;
                }
            });
        });

        if(!$scope.nameContextExactMatch){
            if(term !== ''){
                term += '|';
            }
            term += $scope.searchTerm;
        }

        if (term.length > 1) {
            assets.getWikiInfo(term).success(function(response) {
                angular.forEach(response.query.pages, function(wikiTerm) {
                    if(wikiTerm.extract){
                        var obj = {};
                        obj.title = wikiTerm.title;
                        obj.description = wikiTerm.extract;
                        $scope.wikiTerms.push(obj);
                    }
                });
            });
        }

        /* ***** Show wikipedia results if there are no search results from our system ***** End ***** */
    };

    $scope.runContextCheck = function() {
        $scope.contextInfo.aatArray = [];
        $scope.contextInfo.namesArray = [];
        $scope.contextInfo.tgnArray = [];
        $scope.contextInfo.wikiArray = [];
        $scope.contextInfo.jstorDocs = [];
        $scope.contextInfo.wikiTerms = '';
        
        // We only care about contextual information on the Exhibition/Curated View
        if (isCurated) {
            $scope.lookupAllVocabsForExactMatch($scope.searchTerm);

            angular.forEach( $scope.activeFilters, function(filterGroupObj, filterGroup) {
                angular.forEach( filterGroupObj, function(filterValue, filter) {
                    $scope.fetchContextInfo(filterGroup, filter);
                });
            });

            $scope.getWikipediaContextInfo();

            if ($scope.searchTerm != "*" && $scope.searchTerm.length > 0) {
                // Jstor Search
                SearchSOLR.select($scope.searchTerm).then(function(res) {
                    $scope.contextInfo.jstorDocs = res.docs;
                });
            }
        }
    };

    $scope.fetchContextInfo = function(group, filterName){
        var id = "";
        var vocab = "";
        var searchFallback = false;

        if (group === schema.art.creator) {  
            if($scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] && $scope.assets[0]._source[sourceObjectLabel]["Creator"] && $scope.assets[0]._source[sourceObjectLabel]["Creator"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Creator"]["links"].length > 0)){    
                angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Creator"]["links"], function(creator) {
                    if ( creator.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                        id = creator.source_id;
                    }
                });
                if(id){
                    $scope.setNamesContextInfo(id);
                }
                else{
                    searchFallback = true;
                }
            }
            else{
                searchFallback = true;
            }
        }

        else if (group === schema.art.subject) {
            if($scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] && $scope.assets[0]._source[sourceObjectLabel]["Subject"] && $scope.assets[0]._source[sourceObjectLabel]["Subject"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Subject"]["links"].length > 0)){
                angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Subject"]["links"], function(subject) {
                    if (subject.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                        if ((subject.source === "SSN") || (subject.source === "TGN") || (subject.source === "AAT")) {
                            id = subject.source_id;
                            vocab =  subject.source;
                        }
                    }
                });
                if(vocab == 'SSN'){
                    $scope.setNamesContextInfo(id);
                }
                else if(vocab == 'TGN'){
                    $scope.getTGNContextInfo(id);
                }
                else if(vocab == 'AAT'){
                    $scope.getAATContextInfo(id);
                }
                else{
                    searchFallback = true;
                }
            }
            else{
                searchFallback = true;
            }
        }

        else if (group === schema.art.material) {
            if($scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] ){
                if ( $scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques"] && $scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques"]["links"].length > 0) ) {
                   angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques"]["links"], function(aatRecord) {
                        if ( aatRecord.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                            if (aatRecord.source === "AAT") {
                                id = aatRecord.source_id;
                            }
                        }
                    }); 
                }
                if ( $scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques:AAT"] && $scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques:AAT"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques:AAT"]["links"].length > 0) ) {
                   angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Materials/Techniques:AAT"]["links"], function(aatRecord) {
                        if ( aatRecord.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                            if (aatRecord.source === "AAT") {
                                id = aatRecord.source_id;
                            }
                        }
                    }); 
                }

                if(id){
                    $scope.getAATContextInfo(id);
                }
                else{
                    searchFallback = true;
                }
            }
            else{
                searchFallback = true;
            }
        }

        else if (group === schema.art.stylePeriod) {
            if($scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] ){
                if ( $scope.assets[0]._source[sourceObjectLabel]["Style/Period"] && $scope.assets[0]._source[sourceObjectLabel]["Style/Period"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Style/Period"]["links"].length > 0) ) {
                   angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Style/Period"]["links"], function(aatRecord) {
                        if ( aatRecord.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                            if (aatRecord.source === "AAT") {
                                id = aatRecord.source_id;
                            }
                        }
                    }); 
                }
                if ( $scope.assets[0]._source[sourceObjectLabel]["Style/Period:AAT"] && $scope.assets[0]._source[sourceObjectLabel]["Style/Period:AAT"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Style/Period:AAT"]["links"].length > 0) ) {
                   angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Style/Period:AAT"]["links"], function(aatRecord) {
                        if ( aatRecord.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                            if (aatRecord.source === "AAT") {
                                id = aatRecord.source_id;
                            }
                        }
                    }); 
                }

                if(id){
                    $scope.getAATContextInfo(id);
                }
                else{
                    searchFallback = true;
                }
            }
            else{
                searchFallback = true;
            }
        }

        else if (group === schema.art.workType) {
            if($scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] && $scope.assets[0]._source[sourceObjectLabel]["Work Type"] && $scope.assets[0]._source[sourceObjectLabel]["Work Type"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Work Type"]["links"].length > 0)){
                angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Work Type"]["links"], function(aatRecord) {
                    if ( aatRecord.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                        if (aatRecord.source === "AAT"){
                            id = aatRecord.source_id;
                        }
                    }
                });

                if(id){
                    $scope.getAATContextInfo(id);
                }
                else{
                    searchFallback = true;
                }
            }
            else{
                searchFallback = true;
            }
        }

        else if (group === schema.art.location) { 
            if($scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] && $scope.assets[0]._source[sourceObjectLabel]["Location"] && $scope.assets[0]._source[sourceObjectLabel]["Location"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Location"]["links"].length > 0)){
                angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Location"]["links"], function(subject) {
                    if ( subject.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                        if (subject.source === "TGN") {
                            id = subject.source_id;
                        }
                    }
                });
            } 

            if(!id && $scope.assets[0] && $scope.assets[0]._source && $scope.assets[0]._source[sourceObjectLabel] && $scope.assets[0]._source[sourceObjectLabel]["Place"] && $scope.assets[0]._source[sourceObjectLabel]["Place"]["links"] && ($scope.assets[0]._source[sourceObjectLabel]["Place"]["links"].length > 0)){
                angular.forEach( $scope.assets[0]._source[sourceObjectLabel]["Place"]["links"], function(subject) {
                    if ( subject.data.preferredTerm.toLowerCase().indexOf(filterName.toLowerCase()) > -1){
                        if (subject.source === "TGN") {
                            id = subject.source_id;
                        }
                    }
                });
            }

            if(id){
                $scope.getTGNContextInfo(id);
            }
            else{
                searchFallback = true;
            }

        }
        else if((group === schema.art.culture) || (group === schema.art.title) || (group === schema.art.style)){
            $scope.addWikipediaTerm(filterName);
        } else {
            searchFallback = true;
        }
        
        if (searchFallback) {
            // Try exact match search for vocab (will fallback to wikipedia if no results)
            $scope.lookupAllVocabsForExactMatch(filterName, group);
        }

    };

    $scope.addWikipediaTerm = function(term){
        if($scope.contextInfo.wikiTerms !== ''){
            $scope.contextInfo.wikiTerms += '|';
        }
        $scope.contextInfo.wikiTerms += term;
    };

    $scope.getWikipediaContextInfo = function(term){
        if(term){ // for showing relevant wikipedia entry if no search results are returned by Alcove ES index
            assets.getWikiInfo(term).success(function(response) {
                angular.forEach(response.query.pages, function(wikiTerm) {
                    if(wikiTerm.extract){
                        var obj = {};
                        obj.title = wikiTerm.title;
                        obj.description = wikiTerm.extract;
                        $scope.contextInfo.wikiArray.push(obj);
                    }
                });
            });
        }
        else{
            if($scope.contextInfo.wikiTerms !== ''){
                assets.getWikiInfo($scope.contextInfo.wikiTerms).success(function(response) {
                    angular.forEach(response.query.pages, function(wikiTerm) {
                        if(wikiTerm.extract){
                            var obj = {};
                            obj.title = wikiTerm.title;
                            obj.description = wikiTerm.extract;
                            $scope.contextInfo.wikiArray.push(obj);
                        }
                    });
                });
            }
        }
    };

    $scope.getAATContextInfo = function(id){
        records.getAatRecord(id, function(response) {
            var aatObj = response["subjectDetail"];
            if(response.subjectDetail.hierarchy){
                aatObj.hierarchyArray = $scope.getFlatHierarchyforAAT(response);
            }
            $scope.contextInfo.aatArray.push(aatObj);
        });
    };

    $scope.getTGNContextInfo = function(id){
        records.getTgnRecord(id, function(response) {
            console.log(response);
            var tgnObj = response;
            tgnObj.recordType = $scope.getRecordType(tgnObj.recordType);
            if(response.hierarchy){
                tgnObj.hierarchyArray = $scope.getFlatHierarchyforTGN(response.hierarchy, response.preferredName);
            }

            $scope.contextInfo.tgnArray.push(tgnObj);

            $timeout(function() {
                $scope.setUpTgnObjMap(tgnObj);
            }, 1500);

        });
    };

    $scope.lookupAllVocabsForExactMatch = function(term, filterGroup){
        var vocabTypeArray = ['NAMES', 'TGN', 'AAT'];
        if (filterGroup === schema.art.browseCountry || filterGroup === schema.art.location) {
            vocabTypeArray = ['TGN'];
        }
        var exactMatchVocab = {};
        var breakLoop = false;
        records.lookupAllVocabs(term, function(response) {
            angular.forEach(response.lookupVocab, function(vocab) {
                if(!breakLoop){
                    if(vocabTypeArray.indexOf(vocab.lookupType) > -1 && vocab.lookupTerm.toLowerCase().indexOf('unknown') < 0){
                        exactMatchVocab.type = vocab.lookupType;
                        exactMatchVocab.id = vocab.lookupId;
                        breakLoop = true;
                    }
                }
            });

            if(exactMatchVocab.id){
                if(exactMatchVocab.type === 'NAMES'){
                    $scope.setNamesContextInfo(exactMatchVocab.id);
                }
                else if(exactMatchVocab.type === 'TGN'){
                    $scope.getTGNContextInfo(exactMatchVocab.id);
                }
                else if(exactMatchVocab.type === 'AAT'){
                    $scope.getAATContextInfo(exactMatchVocab.id);
                }
            }
            else{
                $scope.getWikipediaContextInfo(term);
            }
        });


    };

    $scope.setUpTgnObjMap = function(tgnObj){
        if(tgnObj.latitude && tgnObj.longitude && !tgnObj.map){

            if(L){
                tgnObj.map = L.mapbox.map('map-' + tgnObj.subjectId,
                        'artstordesign.o6di1m6f', {
                            center: [tgnObj.latitude.decimal, tgnObj.longitude.decimal],
                            zoom: 10,
                            zoomControl: false,
                            attributionControl: false
                    });

                var latLongObj = new L.LatLng(tgnObj.latitude.decimal, tgnObj.longitude.decimal);

                if((tgnObj.marker === undefined) || (tgnObj.marker === null)){
                    tgnObj.marker = L.marker(latLongObj, {
                        icon: L.mapbox.marker.icon({
                            'marker-color': 'FA5858'
                        })
                    });
                    tgnObj.marker.addTo(tgnObj.map);
                }
                else{
                    tgnObj.marker.setLatLng(latLongObj);
                }
            }
            else {
                console.warn("Mapbox was unable to load");
            }

        }
    };

    $scope.getRecordType = function(type){
        var result = '';
        if(type == 'A'){
            result = "Administrative";
        }
        else if(type == 'B'){
            result = "Both";
        }
        else if(type == 'F'){
            result = "Facet";
        }
        else if(type == 'P'){
            result = "Physical";
        }
        return result;
    };

    $scope.getFlatHierarchyforTGN = function(hierarchy, preferredName){
        var flatHierarchy = [];
        var hierarchalChild;

        if(hierarchy.parent){
            hierarchalChild = hierarchy.parent;
        }

        while(hierarchalChild.child){
            if(hierarchalChild.name != preferredName){
                flatHierarchy.push({"name":hierarchalChild.name, "subjectID":hierarchalChild.subjectId, "type":hierarchalChild.type});
            }
            else{
                flatHierarchy.push({"name":hierarchalChild.name, "type":hierarchalChild.type});
            }

            hierarchalChild = hierarchalChild.child;
        }
        flatHierarchy.push({"name":hierarchalChild.name, "type":hierarchalChild.type});
        return flatHierarchy;
    };

    $scope.getFlatHierarchyforAAT = function(obj) {
        var hierarchalChild;
        var hierarchalParent;
        var flatHierarchy = [];

        if(obj && obj.subjectDetail && obj.subjectDetail.hierarchy && obj.subjectDetail.hierarchy.parent){
            hierarchalParent = obj.subjectDetail.hierarchy.parent;

            if(hierarchalParent.subjectId != obj.subjectDetail.subjectId){
                flatHierarchy.push({'name':hierarchalParent.name, 'subjectID':hierarchalParent.subjectId });
            }
            else {
                flatHierarchy.push({'name':hierarchalParent.name});
            }

            if(hierarchalParent.child){
                for(var i = 0; i < hierarchalParent.child.length; i++ ){
                    hierarchalChild = hierarchalParent.child[i];

                    while(hierarchalChild.child){
                        if(hierarchalChild.subjectId != obj.subjectDetail.subjectId){
                            flatHierarchy.push({'name': hierarchalChild.name, 'subjectID':hierarchalChild.subjectId });
                        }
                        else{
                            flatHierarchy.push({'name': hierarchalChild.name });
                        }
                        hierarchalChild = hierarchalChild.child;
                    }

                    if(hierarchalChild.subjectId != obj.subjectDetail.subjectId){
                        flatHierarchy.push({'name':hierarchalChild.name, 'subjectID':hierarchalChild.subjectId });
                    }
                    else{
                        flatHierarchy.push({'name':hierarchalChild.name });
                    }
                }
            }
        }
        return flatHierarchy;
    };

    $scope.toggleSortByTitle = function(){
        ga('send', {
          hitType: 'event',
          eventCategory: 'Search',
          eventAction: 'Sort',
          eventLabel: 'By Title'
        });
        $scope.clearSorting('title');
        if($scope.sortByTitle == "asc"){
            $scope.sortByTitle = "desc";
        } else{
            $scope.sortByTitle = "asc";
        }
        searchQuery.setSort('title', $scope.sortByTitle);
    };

    $scope.toggleSortByDate = function(){
        ga('send', {
          hitType: 'event',
          eventCategory: 'Search',
          eventAction:  'Sort',
          eventLabel: 'By Date'
        });
        $scope.clearSorting('date');
        if($scope.sortByDate == "asc"){
            $scope.sortByDate = "desc";
        } else{
            $scope.sortByDate = "asc";
        }
        searchQuery.setSort('date', $scope.sortByDate);
    };


    $scope.toggleSortByNewest = function(){
        ga('send', {
          hitType: 'event',
          eventCategory: 'Search',
          eventAction:  'Sort',
          eventLabel: 'By Newest'
        });
        $scope.clearSorting('new');
        if($scope.sortByNewest == "asc"){
            $scope.sortByNewest = "desc";
        } else{
            $scope.sortByNewest = "asc";
        }
        searchQuery.setSort('new', $scope.sortByNewest);
    };


    $scope.sortByCuration = function() {
        $scope.loadExhibitionView();
        $scope.clearSorting();
        $scope.showCatalogView = false;
    };

    $scope.selectSortByRelevance = function() {
        $scope.clearSorting();
        $scope.sortByRelevance = true;
        searchQuery.setSort('rel');
    };

    $scope.showMoreResults = function(){
        
        ga('send', {
          hitType: 'event',
          eventCategory: 'Browse',
          eventAction: 'Show More Results',
          eventLabel: 'More Results'
        });
        
        $scope.loadDisplayView();
    };
    
    $scope.scrollToTop = function(){
        scroll2Top(document.getElementById('bodyContent'), 500);
    };

    $scope.showShareOptions = function(){
        ga('send', {
          hitType: 'event',
          eventCategory: 'Share',
          eventAction: 'Show Share Options',
          eventLabel: 'Via Thumbnails'
        });

        angular.element( document.getElementById('shareAssetModal') ).addClass('fade-in');
    };
    
    $rootScope.$on('showShareOptions', function(event,data) {
        $scope.shareUrl = data.shareUrl;
        $scope.shareMediaUrl = data.shareMediaUrl;
        $scope.shareDesc = data.shareDesc;
        $scope.showShareOptions();
    });


    /*
     * Filter Modal
     * - Shows all available facets for a field
     * - Relies on variables defined with the scope
     * - Template lives at the bottom of 'home.tpl.html'
     */
    $scope.filterModal = {};
    $scope.filterModal.order = '-doc_count';
    $scope.filterModal.page = 0;
    $scope.filterModal.pageSize = 100;
    $scope.filterModal.filters = [];
    $scope.numberOfPages=function(){
        return Math.ceil($scope.filterModal.filters.length/$scope.filterModal.pageSize);                
    };
    $scope.openFilterModal = function(field, filters, returnAll) {
        $scope.filterModalLoading = true;
        $scope.filterModal.page = 0;
        if (!returnAll) {
            $scope.filterModal.filters = filters;
            $scope.filterModalLoading = false;
        } else {
            assets.getAllFilters(field)
                .success(function(res) {
                    console.log(res);
                    if(res.aggregations && res.aggregations.Collections && res.aggregations.Collections.buckets && (res.aggregations.Collections.buckets.length > 0)){
                        for(var i = 0; i < res.aggregations.Collections.buckets.length; i++){
                            if($localStorage['collectionFilter'] && $localStorage['collectionFilter'][res.aggregations.Collections.buckets[i].key]){
                                res.aggregations.Collections.buckets[i].label = $localStorage['collectionFilter'][res.aggregations.Collections.buckets[i].key].name;
                            }
                        }
                        processCollectionFilters(res.aggregations.Collections.buckets);
                    }

                    if(res.aggregations && res.aggregations['Meta-InstitutionId'] && res.aggregations['Meta-InstitutionId'].buckets && (res.aggregations['Meta-InstitutionId'].buckets.length > 0)){
                        for(var j = 0; j < res.aggregations['Meta-InstitutionId'].buckets.length; j++){
                            if($localStorage['institutionFilter'] && $localStorage['institutionFilter'][res.aggregations['Meta-InstitutionId'].buckets[j].key]){
                                res.aggregations['Meta-InstitutionId'].buckets[j].label = $localStorage['institutionFilter'][res.aggregations['Meta-InstitutionId'].buckets[j].key].name;
                            }
                        }
                    }
                    $scope.filterModal.filters = res.aggregations[field]['buckets'];
                    $scope.filterModalLoading = false;
                });
        }
	    $scope.filterModal.field = field;
	    angular.element( document.getElementById('filterModal') ).addClass('fade-in');
    };

    $scope.hideFilterModal = function(){
        angular.element( document.getElementById('filterModal') ).removeClass('fade-in');
    };

    $scope.setFilterModalSort = function(sortOpt){
        $scope.filterModal.order = sortOpt;
    };

    $scope.Math = Math;

    $scope.toggleFilterMenu = function() {
        if ($rootScope.filterMenuShowing) {
            $rootScope.filterMenuShowing = false;
            angular.element(document.getElementById('filters')).removeClass('slideOut');
        } else {
            $rootScope.filterMenuShowing = true;
            angular.element(document.getElementById('filters')).addClass('slideOut');
        }
    };
});
