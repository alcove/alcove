angular.module( 'alcove.catalogColumn', [] )
    .directive( 'catalogColumn', function() {
        return {
            transclude: true,
            replace: true,
            restrict: 'E',
            templateUrl: 'home/components/catalogColumn.jade',
            controller : ['$scope', '$rootScope', function(
                        $scope, 
                        $rootScope
                    ){ 
                // Controller Code

            }],
            link: function($scope, elem, attrs) {  


            } 
        };
    })
 ;