angular.module( 'alcove.subjectGuides', ['ngStorage'] )
    .directive( 'subjectGuides', function() {
        return {
            replace: false,
            restrict: 'E',
            scope: { },
            templateUrl: 'home/components/subjectGuides.tpl.html',
            controller : ['$scope', '$localStorage', '$rootScope', '$window', function(
                        $scope, 
                        $localStorage, 
                        $rootScope, 
                        $window
                    ){ 

               $scope.subjectGuides = [
                    { "title":"African and African American Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_african-american.pdf"},
                    { "title":"American Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_americanstudies.pdf"},
                    { "title":"Anthropology",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_anthropology.pdf"},
                    { "title":"Architecture and the Built Environment ",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_architecture.pdf"},
                    { "title":"Asian Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_asianstudies.pdf"},
                    { "title":"Classical Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_classicalstudies.pdf"},
                    { "title":"Decorative Arts",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_decorativearts.pdf"},
                    { "title":"Design",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_design.pdf"},
                    { "title":"Fashion and Costume",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_fashion_costume.pdf"},
                    { "title":"History of Medicine and Natural Science",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_medicine_science.pdf"},
                    { "title":"Languages and Literature",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_languages_lit.pdf"},
                    { "title":"Latin American Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_latinamerican.pdf"},
                    { "title":"Maps and Geography ",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_maps_geography.pdf"},
                    { "title":"Medieval Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_medievalstudies.pdf"},
                    { "title":"Middle Eastern Studies ",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_middleeastern.pdf"},
                    { "title":"Music History",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_musichistory.pdf"},
                    { "title":"Native American and Indigenous Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_nativeamerican_indigenous.pdf"},
                    { "title":"Photography",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_photography.pdf"},
                    { "title":"Religious Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_religiousstudies.pdf"},
                    { "title":"Renaissance Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_renaissancestudies.pdf"},
                    { "title":"Theater and Dance",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_theater_dance.pdf"},
                    { "title":"Women's Studies",
                        "url" : "http://www.artstor.org/sites/default/files/filepicker/63/artstor_r_sg_womensstudies.pdf"}
                ];
                
            }],
            link: function($scope, elem, attrs) {  


            }
        };
    })
 ;