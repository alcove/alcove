angular.module( 'alcove.blogPosts', ['ngStorage'] )
    .directive( 'blogPosts', function() {
        return {
            replace: false,
            transclude: true,
            restrict: 'E',
            templateUrl: 'home/components/blogPosts.tpl.html',
            controller : ['$scope', '$localStorage', '$rootScope', '$window', 'external', 'assets', function(
                        $scope,
                        $localStorage,
                        $rootScope,
                        $window,
                        external,
                        assets
                    ){

                $scope.blogEntries = [];
                $scope.isSearchResults = false;
                $scope.cacheData = {};

                // Post Categories to hide (compared as LOWER CASE)
                var catsToHide = ['artstor is...', 'behind the scenes', 'collection agreement', 'events', 'friday links', 'in the news', 'on this day', 'organization', 'Shared Shelf', 'staff pick of the week', 'teaching with artstor', 'tips & tools', 'training', 'travel awards', 'k12', 'oiv', 'uncategorized'];

                function formatEntryContent(blogEntry) {
                  var contentString = blogEntry.content.toString(); 
                  // Prevent images from loading while creating element for formatting
                  contentString = contentString.replace(new RegExp('src=', 'g'), 'title=');
                  var content = angular.element("<div>" + contentString + "</div>");

                  // To-Do: We should sort through the images and find an appropriate lead image
                  content.find("img").remove();
                  content.find("a[rel]").remove();
                  content.find("br").remove();
                  content.find(".wp-caption-text").remove();
                  content.find(".tiled-gallery").remove();
                  content.find(".tiled-gallery-caption").remove();
                  blogEntry.formattedContent = content.html();
                  return blogEntry;
                }

                var getBlogEntries = function(searchTerm) {
                    var fridayLinksCat = "Friday links";
                    var collectionsCat = "Collections";
                    var releaseCat = "Release";

                    var fridayLinksPosts = [];
                    var blogPosts = [];

                    function refreshFridayLinks() {
                      external.getBlogEntriesByCat('friday-links', 2)
                        .then(function success(response) {
                          // Get most recent Friday Links post and format content
                          console.log(response);
                          var blog_entry = formatEntryContent(response.data.posts[0]);

                          var listContentArray = [];
                          var content_lists = angular.element("<div>" + blog_entry.content + "</div>").find("li");
                          content_lists.find("img").remove();

                          angular.forEach(content_lists, function(list){
                            listContentArray.push(list.innerHTML);
                          });
                          blog_entry.listContentArray = listContentArray;
                          blog_entry.dateAccessed = new Date();
                          console.log(blog_entry);
                          fridayLinksPosts.push(blog_entry);

                          $localStorage["fridayLinksPosts"] = fridayLinksPosts;
                        }, function error(response) {

                        });
                    }

                    // Check if stored Friday Links are a day old
                    if ($localStorage["fridayLinksPosts"]) {
                      var now = new Date();
                      var fridayLinksAge = new Date($localStorage["fridayLinksPosts"][0].dateAccessed);
                      var diffDays = Math.abs((now.getTime() - fridayLinksAge.getTime())/(24*60*60*1000));
                      if (isNaN(diffDays) || diffDays > 1) {
                        refreshFridayLinks();
                      }
                    } else {
                      refreshFridayLinks();
                    }

                    if(searchTerm){
                      external.getBlogEntries(searchTerm).success(function(response) {
                        setBlogResponse(response, fridayLinksCat, collectionsCat, releaseCat, blogPosts, searchTerm);
                      });
                    }
                    else{
                      if($scope.cacheData && $scope.cacheData.cache){
                        setBlogResponse($scope.cacheData.cache.blogRecent, fridayLinksCat, collectionsCat, releaseCat, blogPosts, searchTerm);
                      }
                    }
                };

                var setBlogResponse = function(response, fridayLinksCat, collectionsCat, releaseCat, blogPosts, searchTerm){
                  angular.forEach(response.posts, function(blog_entry) {

                    blog_entry._source = {};

                    if(blogPosts.length < 5 && blog_entry.categories){

                      var catObj = blog_entry.categories;
                      var catValue = '';
                      var hideCat = false;

                      angular.forEach(catObj, function(value, key){
                        if (catsToHide.indexOf(key.toLowerCase()) > -1){
                          hideCat = true;
                        }
                      });
                      
                      if (!hideCat) {
                        blogPosts.push(formatEntryContent(blog_entry));
                      }
                    }
                  });

                  if(searchTerm && (searchTerm !== '*')){ // Show Just one Blog Record matching the search term
                    $scope.blogEntries = [];
                    $scope.isSearchResults = true;
                    if(blogPosts[0]) {
                      $scope.blogEntries.push(blogPosts[0]);
                    }
                  }
                  else{
                    // only show two on homepage
                    $scope.blogEntries = [];

                    for(var i = 0; i < blogPosts.length; i++){
                      var blog_entry = blogPosts[i];

                      if($scope.blogEntries.length === 0){
                        $scope.blogEntries.push(blog_entry);
                      }
                      else if($scope.blogEntries[0].title !== blog_entry.title){
                        $scope.blogEntries.push(blog_entry);
                        break;
                      }
                    }
                  }
                };

                $scope.$on('fetchBlogEnteries',function(event, data){
                  getBlogEntries(data.searchTerm);
                });

                $rootScope.$on('fetchBlogEnteriesFromCache',function(event, data){
                  if (data) {
                    $scope.cacheData = data;
                  }
                  getBlogEntries();
                });

                $scope.blogSrcClicked = function(entry){
                  ga('send', {
                    hitType: 'event',
                    eventCategory: 'Access',
                    eventAction: 'Link to Blog Post',
                    eventLabel: entry.title
                  });
                };

                // Checks for local searchTerm variable in template
                if ($scope.$parent.$parent.searchTerm) {
                    getBlogEntries($scope.$parent.$parent.searchTerm);
                }

            }],
            link: function(scope, elem, attrs) {

            }
        };
    })
 ;
