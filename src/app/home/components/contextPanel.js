angular.module( 'alcove.contextPanel', [] )
    .directive( 'contextPanel', function() {
        return {
            transclude: true,
            replace: true,
            restrict: 'E',
            scope: { 
                contextInfo: '=',
                filterArray: '=',
                filterArrayExclude: '=',
                searchTerm: '@',
                panelClass: '@'
            },
            templateUrl: 'home/components/contextPanel.jade',
            controller : ['$scope', '$rootScope', 'schema', function(
                        $scope, 
                        $rootScope,
                        schema
                    ){ 

                // Controller Code
                $scope.schema = schema;
                $scope.relCollExpand = false;

            }],
            link: function($scope, elem, attrs) {  
                
                $scope.jstorArtClicked = function(doc){
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Access',
                        eventAction: 'Link to Journal',
                        eventLabel: doc.ta + ' - http://www.jstor.org/stable/' + doc.doi
                    });
                };

                $scope.contextInfoSrcClicked = function(source){
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Access',
                        eventAction: 'Link to Source',
                        eventLabel: source
                    });
                };

            }
        };
    })
 ;