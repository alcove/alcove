angular.module( 'alcove.homeScroll', [] )
  .directive('homeScroll', ['$timeout', '$rootScope', function($timeout, $rootScope) {
    return {
      scope: {
                  loadedAssets: '@',
                  totalAssets: '@',
                  isHomeView: '=',
                  infiniteScrollFunction: '&'
              },
      restrict: 'A',
      link: function($scope, elem, attrs) {

          $timeout(function() {

            var windowHeight = window.outerHeight;
            var bodyContainer = document.getElementById('bodyContent');
            var catalogResultsContainer; //$('#results .nano-content');
            var bodyScrollTop;
            var bottomOffsetTop;

            window.onresize = function() {
              windowHeight = window.outerHeight;
            };

            // ----------------------------------------------
            // Home & Catalog View: Monitor scroll progress
            // ----------------------------------------------

            var scrollMonitor = function(event, target) {
              var bodyScrollTop = document.getElementById('bodyContent').scrollTop;
              
              if(bodyScrollTop >= 680){
                angular.element(document.querySelector('.show-below-content')).addClass('fade-in');
              }
              else{
                angular.element(document.querySelector('.show-below-content')).removeClass('fade-in');
              }

              if (bodyScrollTop > 100) {
                // Show nav background if not near top
                // use JQLite to remove class
                angular.element( document.getElementById('nav') ).removeClass('no-bg');
              } else if ($scope.isHomeView === true) {
                // Hide nav background on homepage if near the top
                // use JQLite to add class 
                angular.element( document.getElementById('nav') ).addClass('no-bg');
              }

              if ( $scope.loadedAssets && $scope.totalAssets) {
                if (document.getElementById('results')) {
                  var scrollProgress = bodyScrollTop / (( document.getElementById('results').scrollHeight -  windowHeight ) + 1);

                  $rootScope.progressbar.set( ($scope.loadedAssets/$scope.totalAssets) * scrollProgress * 100 );
                  
                  // Catalog View: Infinite Scroll
                  if( (document.getElementById('results').scrollHeight > 900) && (bodyScrollTop >= ( document.getElementById('results').scrollHeight - 600)) ){
                    $scope.infiniteScrollFunction()();
                  }
                }

              }
            };

            bodyContainer.onscroll = function(event) {
              debounce(scrollMonitor("bodyContent"),300);
            };

        }); // end of $timeout
      }
    };
  }]);
