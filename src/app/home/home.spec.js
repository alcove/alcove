/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'HomeCtrl', function() {
	var scope;

	beforeEach( module( 'alcove.home' ) ); 

    beforeEach(inject(function ($rootScope, $controller) {
          scope = $rootScope.$new();

          createController = function() {
              return $controller('HomeCtrl', {
                  '$scope': scope
              });
          };
      }));

      // it('should load Curated View by default', function() {
      //     var controller = createController();  
      //     expect(scope.showCatalogView).toBe(false); 
      // });

	// tests start here
    // it('should update search term', function(){
    //     scope.updateSearchTerm('test');
    //     expect(scope.searchTerm).toBe('test');
    // });

});

