exports.config = {
  baseUrl: 'http://127.0.0.1:9001',
  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  framework: 'custom',
  frameworkPath: 'node_modules/protractor-cucumber-framework',
  specs: [
    'features/*.feature'
  ],
  capabilities: {
    browserName: 'phantomjs',
    'phantomjs.binary.path': './node_modules/phantomjs/bin/phantomjs',
    'phantomjs.cli.args': ['--web-security=false', '--ignore-ssl-errors=true'],
    // ['--webdriver', '--debug=true', '--webdriver-loglevel=DEBUG',  '--ignore-ssl-errors=true', '--web-security=false'],
    version: '',
    platform: 'ANY'
  },
  //screenshotPath: 'features/screenshots',
  cucumberOpts: {
    require: 'features/steps/*_steps.js',
    format: 'pretty'
  }
}

   