Feature: Exhibition View should load
  As a user of Alcove
  I should be able to access Exhibition View
  via the URL parameter

  Scenario: Accessing Exhibition View by URL
    Given I go to "?v=curate"
    Then the title should equal "Alcove"
    Then take screenshot called "exhibition-view"
    Then the right context panel should show

  Scenario: When searching for a Place, contextual info should show
    Then search for "Paris"
    Then context panel heading should be "Paris"

  Scenario: Showing contextual info for selected name facets
    Given I go to "?v=curate"
    Then the title should equal "Alcove"
    Then I click the first "Names" facet
    Then take screenshot called "selected-names-facet"
    Then contextual info should appear for the selected name facet

  Scenario: Showing collection data for selected collection facet
    Given I go to "?v=curate"
    Then the title should equal "Alcove"
    Then I click the first "collection" facet
    Then take screenshot called "selected-collection-facet"
    Then collection data should appear

  Scenario: Filter by Collection Name goes to Exhibition View
    Given I go to "/"
	Then the title should equal "Alcove"
	Then I click the first "collection" facet
    Then take screenshot called "selected-collection-facet-2"
    Then Exhibition View is loaded

  Scenario: Filter by Creator goes to Exhibition View
    Given I go to "/"
	Then the title should equal "Alcove"
	Then I click the first "Names" facet
    Then take screenshot called "selected-Names-facet-2"
    Then Exhibition View is loaded

  Scenario: Filter by location; Clear filter
    Given I go to "?v=curate"
	Then the title should equal "Alcove"
	Then I click the first "Location" facet
    Then take screenshot called "selected-Location-facet"
    Then the results should be filtered by selected location facet
    Then clear the selected filter
