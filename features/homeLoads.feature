Feature: Home Page should load
	As a user of Alcove
  	I should be able to load the Home view
  	and see content in each component

  	Scenario: Load the Home Page by URL
    	Given I go to "/"
    	Then the title should equal "Alcove"
		Then take screenshot called "home-view"
    	And thumbnails should appear

	Scenario: Viewing blog posts in the home view
    	Then the blog posts should load

	Scenario: Viewing featured collections in the home view
		Then the featured collections should load

	Scenario: 3 featured collections are loaded in the home view
		Then at least "3" featured collections should load

	Scenario: Recent collections showing up, scroll to top & terms page link working properly
		Given I go to "/"
		Then the title should equal "Alcove"
		Then "3" most recent collections are showing
		#Then scrollTop button is working properly
		#Then terms link is working properly

	Scenario: Switching to exhibition view works
		Given I go to "/"
		Then the title should equal "Alcove"
		Then I click the "exhibition view" icon
		Then take screenshot called "click-exhb-view"
		Then the "exhibition" view loads

	Scenario: Switching to catalog view works
		Given I go to "/"
		Then the title should equal "Alcove"
		Then I click the "catalog view" icon
		Then take screenshot called "click-ctlg-view"
		Then the "catalog" view loads

		
	Scenario: Clicking view more to see recent items in Catalog view
		Given I go to "/"
		Then the title should equal "Alcove"
		Then I click the "view more" icon
		Then take screenshot called "click-view-more"
		Then the "catalog" view loads
		Then recent Items Load


