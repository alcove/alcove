Feature: Item View should be accessible
	As a user of Alcove
  	I should be able to load the Item view
  	by selecting an asset

  	Scenario: Load the Catalog View by URL
    	Given I go to "?v=display"
    	Then the title should equal "Alcove"
		Then take screenshot called "catalog-view"
    	Then thumbnails should appear
	
	Scenario: Open first asset in results
    	Then I click on the first thumbnail
		Then take screenshot called "item-view"
		Then metadata should display value
		Then metadata should display label

	Scenario: View item metadata in Zotero - verify meta tags only
		Given I go to "/item/3955798"
    	Then the "DC.format" meta tag exists
    	Then the "DC.type" meta tag exists
    	Then the "DC.title" meta tag exists
    	Then the "DC.creator" meta tag exists
    	Then the "DCTERMS.subject" meta tag exists
    	Then the "DC.description" meta tag exists
    	Then the "DCTERMS.issued" meta tag exists
    	Then the "DC.identifier" meta tag exists

	Scenario: Verify access to asset
		Then the download link has url
		Then the download link has filename
    
    Scenario: Click on vocab term to start search
        Given I go to "/item/1078897"
        Then the title should equal "Cold Crush Fever is Here! Catch it Now - Alcove"
        Then I click on the "Conzo, Joe, Jr." vocab hyperlink
        Then the clicked vocab should be a selected facet
        Then take screenshot called "Item Page Hyperlink clicked"
    
    Scenario: Return to results is working properly
        Given I go to "/"
        Then the title should equal "Alcove"
        Then search for "painting"
        #Then take screenshot called "painting-searched"
        Then I click on the first thumbnail
        Then take screenshot called "painting-searched-first-asset-clicked"
        Then I click Back to Results link
        Then take screenshot called "back-to-results"
        Then the search term should be "painting"
        