var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

var expect = chai.expect;
// at the top of the test spec:
var fs = require('fs');

// abstract writing screen shot to a file
function writeScreenShot(data, filename) {
    var stream = fs.createWriteStream(filename);

    stream.write(new Buffer(data, 'base64'));
    stream.end();
}

module.exports = function() {
  // Increase timeout to 20s for slow connections
  this.setDefaultTimeout(90 * 1000);

  this.Given(/^I go to "([^"]*)"$/, function (arg1, callback) {
    //browser.ignoresynchronisation = true;
    
    var width = 1000;
    var height = 1600;
    browser.manage().window().setSize(width, height);

    browser.get('http://localhost:9001/#' + arg1).then(function() {
      // browser.waitForAngular();

      // browser.ignoreSynchronization = false;
      // browser.waitForAngular();
      // browser.sleep(500);

      callback();
    });
  });

  this.Then(/^the title should equal "([^"]*)"$/, function (arg1, callback) {
    expect(browser.getTitle()).to.eventually.equal(arg1).and.notify(callback);
  });

  this.Then(/^thumbnails should appear$/, function (callback) {
    expect( element(by.css('.thumbnail')).isPresent() ).to.eventually.equal(true).and.notify(callback);
  });

  this.Then(/^take screenshot called "([^"]*)"$/, function (arg1, callback) {
    browser.takeScreenshot().then(function (png) {
      writeScreenShot(png, 'features/screenshots/' + arg1 + '.png');
    });
    callback();
  });

  this.Then(/^the blog posts should load$/, function (callback) {
    var blogPosts = element.all(by.css('.rightContext .article'));
    expect(blogPosts.count()).to.eventually.equal(parseInt(2)).and.notify(callback);
  });

  this.Then(/^the featured collections should load$/, function (callback) {
    expect( element(by.css('.featuredCollections-cntnr') ).getCssValue('display') ).to.eventually.equal('block').and.notify(callback);
  });

  this.Then(/^at least "([^"]*)" featured collections should load$/, function (arg1, callback) {
    var featuredCols = element.all(by.css('.featuredCollections'));
    expect(featuredCols.count()).to.eventually.be.gte(parseInt(arg1)).and.notify(callback);
  });

  this.Then(/^"([^"]*)" most recent collections are showing$/, function (arg1, callback) {
    var recentCols = element.all(by.css('.newtopCol'));
    expect(recentCols.count()).to.eventually.be.equal(parseInt(arg1)).and.notify(callback);
  });

  this.Then(/^terms link is working properly$/, function (callback) {
    element(by.id('terms-link')).click();
    browser.getAllWindowHandles().then(function (handles) {
        newWindowHandle = handles[1];
        browser.switchTo().window(newWindowHandle).then(function () {
            expect(browser.driver.getCurrentUrl()).to.eventually.be.equal("http://www.artstor.org/terms");
            //to close the current window
            browser.driver.close().then(function () {
                //to switch to the previous window
                browser.switchTo().window(handles[0]);
                callback();
            });
 
        });
    });

  });

  this.Then(/^scrollTop button is working properly$/, function (callback) {
    browser.executeScript('document.querySelector("#bodyContent").scrollTop = 5000;').then(function () {
      // element(by.id('scrollToTop')).click();
      callback();

      // var top = element(by.id('scrollToTop')).getAttribute('scrollTop');
      // var top = "0";
      // expect(element(by.id('scrollToTop')).getAttribute('scrollTop')).to.eventually.be.equal('0');

      // browser.driver.sleep(2000);
      // browser.executeScript('return window.pageYOffset;').then(function(pos) {
      //     expect(pos).to.eventually.be.equal(0).and.notify(callback);
      // });

    })
  });

  this.Then(/^I click the "([^"]*)" icon$/, function (arg1, callback) {
      var elementClass = '';
      if(arg1 === 'exhibition view'){
        elementClass = 'exhbt-view';
      }
      else if(arg1 === 'catalog view'){
        elementClass = 'ctlg-view';
      }

      else if(arg1 === 'view more'){
        elementClass = 'view-more-btn';
      }

      element( by.css('.' + elementClass) ).click().then(function() {
          callback();
      });
  }); 

  this.Then(/^the "([^"]*)" view loads$/, function (arg1, callback) {
      var elementClass = '#context.rightContext';
      var expectedDisplayValue = '';
      
      if(arg1 === 'exhibition'){
        expectedDisplayValue = 'block';
      }
      else if(arg1 === 'catalog'){
        expectedDisplayValue = 'none';
      }

      expect( element(by.css(elementClass) ).getCssValue('display') ).to.eventually.equal(expectedDisplayValue).and.notify(callback);

  });

  this.Then(/^recent Items Load$/, function (callback) {
      var elementClass = '.ctlg-col-res .thumbnail';
      var expectedDisplayValue = 'block';

      expect( element(by.css(elementClass) ).getCssValue('display') ).to.eventually.equal(expectedDisplayValue).and.notify(callback);

  }); 
}