var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;
var fs = require('fs');

module.exports = function() {
    // Increase timeout to 20s for slow connections
    this.setDefaultTimeout(90 * 1000);

    this.Then(/^I click on the first thumbnail$/, function (callback) {
        var width = 1000;
        var height = 1600;
        browser.manage().window().setSize(width, height);

        var firstThumb = element.all(by.css('.thumbnail__image-wrap'))
            .filter(function(elem) {
                return elem.isDisplayed(); 
            })
            .first();

        firstThumb.click().then(function() {
            callback();
        });
    });

    this.Then(/^metadata should display value$/, function (callback) {
      expect( element.all(by.css('#columnTwoContent .data-chunk .data-value') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^metadata should display label$/, function (callback) {
      expect( element.all(by.css('#columnTwoContent .data-chunk .data-label') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^the "([^"]*)" meta tag exists$/, function (arg1, callback) {
      expect(element(by.css('meta[name="' + arg1 + '"]')).getAttribute('content'))
        .to.eventually.have.length.gt(0)
        .and.notify(callback);
    });

    this.Then(/^I click on the "([^"]*)" vocab hyperlink$/, function (arg1, callback) {
        element(by.partialLinkText(arg1)).click().then(function() {
            callback();
        });
    });

    this.Then(/^the clicked vocab should be a selected facet$/, function (callback) {
        expect( element.all( by.css('filter.active') ).get(2).getText() )
        .to.eventually.equal('Conzo, Joe, Jr.')
        .and.notify(callback);
    });

    this.Then(/^I click Back to Results link$/, function (callback) {
        element( by.css('.return-link') ).click().then(function() {
            callback();
        });
    }); 

    this.Then(/^the search term should be "([^"]*)"$/, function (arg1, callback) {
        
        expect( element(by.model('searchParam')).getAttribute('value') )
        .to.eventually.equal(arg1)
        .and.notify(callback);
        
    });

    this.Then(/^the download link has url$/, function(callback) {
        expect(element(by.id('downloadLink')).getAttribute('href')).to.eventually.have.length.gt(1)
            .and.notify(callback);
    });

    this.Then(/^the download link has filename$/, function(callback) {
        expect(element(by.id('downloadLink')).getAttribute('download')).to.eventually.have.length.gt(1)
            .and.notify(callback);
    });
}