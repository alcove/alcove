var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;
var fs = require('fs');

// ... other code

module.exports = function() {
  // Increase timeout to 20s for slow connections
  this.setDefaultTimeout(50 * 1000);

  this.Then(/^the right context panel should show/, function (callback) {
      expect( element(by.css('.rightContext') ).getCssValue('display') ).to.eventually.equal('block').and.notify(callback);
  });
  
  this.Then(/^search for "([^"]*)"$/, function (arg1, callback) {
    var searchInput = element(by.css('#autocomplete'));

    searchInput.sendKeys(arg1, protractor.Key.ENTER)
      .then(function() {
        callback();
      });
  });

  this.Then(/^context panel heading should be "([^"]*)"$/, function (arg1, callback) {
    expect( element.all(by.css('#contextContent .context-heading') ).first().getText() )
        .to.eventually.equal(arg1)
        .and.notify(callback);
  });

  this.When(/^I click the first "([^"]*)" facet$/, function(arg1, callback) {
	var filterClass = '';
	if(arg1 === 'Names'){
		filterClass = 'ART-Creator';
    }
    else if(arg1 === 'Location'){
    	filterClass = 'ART-Location';
    }
    else if(arg1 === 'collection'){
    	filterClass = 'Collections';
    }

    element.all( by.css('.' + filterClass) ).get(0).click().then(function() {
        callback();
    });

  });

  this.Then(/^contextual info should appear for the selected name facet$/, function (callback) {
    expect( element.all( by.css('.section-wrap .context-heading') ).get(0).getText() )
    	.to.eventually.equal('Photographer')
    	.and.notify(callback);
  });

  this.Then(/^collection data should appear$/, function (callback) {
    expect( element.all( by.css('.collection-desc p') ) )
    	.to.eventually.have.length.gt(0)
        .and.notify(callback);
  });

  this.Then(/^Exhibition View is loaded$/, function (callback) {
    expect( element.all( by.css('.exhbt-view.active') ) )
    	.to.eventually.have.length.gt(0)
        .and.notify(callback);
  });

  this.Then(/^the results should be filtered by selected location facet$/, function (callback) {
    expect( element.all( by.css('filter.active') ).get(2).getText() )
    	.to.eventually.equal('Bronx')
    	.and.notify(callback);
  });


  this.Then(/^clear the selected filter$/, function (callback) {
  	element.all( by.css('.removeIcon') ).get(0).click().then(function() {
        expect( element.all( by.css('filter.active') ) )
	    	.to.eventually.have.length.lt(3)
	    	.and.notify(callback);
    });

    
  });

}