var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;
var fs = require('fs');

module.exports = function() {
    // Increase timeout to 20s for slow connections
    this.setDefaultTimeout(50 * 1000);

    this.Then(/^first thumbnail should display name/, function (callback) {
      expect( element.all(by.css('#resultsContent .thumbnail .e2e-name') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^first thumbnail should display title/, function (callback) {
      expect( element.all(by.css('#resultsContent .thumbnail .e2e-title') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^first thumbnail should display date/, function (callback) {
      expect( element.all(by.css('#resultsContent .thumbnail .e2e-date') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^metadata should display name/, function (callback) {
      expect( element.all(by.css('#columnOneContent .e2e-name') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^metadata should display title/, function (callback) {
      expect( element.all(by.css('#columnOneContent .e2e-title') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^metadata should display date/, function (callback) {
      expect( element.all(by.css('#columnOneContent .e2e-date') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^metadata should display institution/, function (callback) {
      expect( element(by.binding('asset.instituteName')).getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });

    this.Then(/^metadata should display collection/, function (callback) {
      expect( element.all( by.binding('collection.name') ).first().getText() )
        .to.eventually.have.length.gt('0')
        .and.notify(callback);
    });
}