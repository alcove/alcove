Feature: Tombstone Data should show
	As a user of Alcove
  	I should be able to see tombstone data
  	by thumbnails and in item view

  	Scenario: Find an asset with all tombstone fields
    	Given I go to "?v=display&f=Meta-InstitutionId;Name;Date;Collections;Title;"
    	Then the title should equal "Alcove"
    	Then thumbnails should appear

    Scenario: Thumbnails should display Title, Date, and Name
        Then first thumbnail should display title
        Then first thumbnail should display date
        Then first thumbnail should display name
    
    Scenario: Item view should display Title, Date, Name, Institution, and Collection
        Then I click on the first thumbnail
        Then metadata should display title
        Then metadata should display date
        Then metadata should display name
        Then metadata should display institution
        Then metadata should display collection